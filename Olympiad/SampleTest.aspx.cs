﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
namespace Olympiad
{
    public partial class SampleTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["starttime"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            if (!IsPostBack)
            {
                //Query strings
                if (Request.QueryString.ToString().Length == 0)
                {
                    Response.Redirect("~/default.aspx");
                }
                foreach (var allkey in Request.QueryString.AllKeys)
                {
                    switch (allkey)
                    {
                        case "T":
                            hn_testid.Value = PasswordEncrypt_Decrypt.Decrypt(HttpUtility.UrlDecode(Request.QueryString["T"]).ToString().Replace(" ", "+"));
                            break;
                        case "C":
                            hn_classid.Value = PasswordEncrypt_Decrypt.Decrypt(HttpUtility.UrlDecode(Request.QueryString["C"]).ToString().Replace(" ", "+"));
                            break;
                        case "Th":
                            hn_thour.Value = PasswordEncrypt_Decrypt.Decrypt(HttpUtility.UrlDecode(Request.QueryString["Th"]).ToString().Replace(" ", "+"));
                            break;
                        case "Tm":
                            hn_tmin.Value = PasswordEncrypt_Decrypt.Decrypt(HttpUtility.UrlDecode(Request.QueryString["Tm"]).ToString().Replace(" ", "+"));
                            break;
                        case "TT":
                            hn_testtype.Value = PasswordEncrypt_Decrypt.Decrypt(HttpUtility.UrlDecode(Request.QueryString["TT"]).ToString().Replace(" ", "+"));
                            break;
                        case "R":
                            hn_regno.Value = PasswordEncrypt_Decrypt.Decrypt(HttpUtility.UrlDecode(Request.QueryString["R"]).ToString().Replace(" ", "+"));
                            break;
                        default:
                            Response.Redirect("~/default.aspx");
                            break;
                    }
                }
                hn_submiturl.Value = "Sampletest.aspx/submitdata";
                hn_getdataurl.Value = "Sampletest.aspx/getsampletestdata";

                hn_starttime.Value = Session["starttime"].ToString();
                hn_currenttime.Value = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                Session["starttime"] = null;
            }
        }
        [WebMethod]
        public static string getsampletestdata(string regno, string testid)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@regNo", regno);
            param[1] = new SqlParameter("@testid", testid);

            DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "Pro_getstutestdata", param);

            return JsonConvert.SerializeObject(ds);
        }
        [WebMethod]
        public static string submitdata(string regno, int tstid, int classid, int t_hr, int t_min, int t_sec, string ansdata)
        {
            try
            {
                DataTable dt = (DataTable)JsonConvert.DeserializeObject(ansdata, typeof(DataTable));

                SqlParameter[] param = new SqlParameter[7];
                param[0] = new SqlParameter("@Regno", regno);
                param[1] = new SqlParameter("@testid", tstid);
                param[2] = new SqlParameter("@classid", classid);
                param[3] = new SqlParameter("@t_hour", t_hr);
                param[4] = new SqlParameter("@t_min", t_min);
                param[5] = new SqlParameter("@t_sec", t_sec);
                param[6] = new SqlParameter("@ansdata", dt);
                SqlHelper.ExecuteNonQuery(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "Pro_SubmittestAnsdetails", param);
                return "Submit test Successfully.";
            }
            catch (Exception ex)
            {

                UtilityModule.LogError(ex);
                return "Error Occured.";
            }
        }
        [WebMethod]
        public static string tempanssave(int testid, int classid, string regno, int quesid, string ans)
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@testid", testid);
            param[1] = new SqlParameter("@classid", classid);
            param[2] = new SqlParameter("@regno", regno);
            param[3] = new SqlParameter("@quesid", quesid);
            param[4] = new SqlParameter("@ans", ans);

            string sql = @"Insert into OnlineTestTempAns(Testid,Classid,Regno,Quesid,Ans)
                        values(" + testid + "," + classid + "," + regno + "," + quesid + ",'" + ans + "')";
            SqlHelper.ExecuteNonQuery(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, sql);
            return "";
        }
    }
}