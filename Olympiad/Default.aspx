﻿<%@ Page Title="RPS Olympiad 2020" Language="C#" MasterPageFile="~/MasterPage.master"
    AutoEventWireup="true" Inherits="_Default" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="Slider">
        <img src="Images/Slides/Slider2.jpg" alt="RPSGOS" />
    </div>
    <div class="div_about">
        <div class="container wow animated fadeInUp">
            <h2 class="text-center wow animated fadeInUp">
                About RPS Olympiad</h2>
            <div class="border_botom_center">
            </div>
            <p class="wow animated fadeInUp">
                Rao Pahlad Singh Education Society, since 22 years, has been in the noble service
                of enlightening the so called backward strata of the State of Haryana and the adjoining
                state Rajasthan through its educational institutions at school and college level.
                It has been established by a group of philanthropists with an objective to make
                education easy accessible even to the person of the last row. RPS Group of Schools
                and Colleges under the aegis of RPS Education Society, have been rooting the records
                of achievements for the last 22 years, since the inception. RPS institutions have
                proved that private enterprise in education can produce the greatest results with
                no strain on the country’s exchequer. In spite of being in rural area they have
                produced students who are self-reliant, confident and versatile with the courage
                of conviction and endowed with a spirit of adventure and creativity. Above all,
                good human beings, who are upright citizens with a strong moral fiber, ready to
                be the torchbearers of a resurgent India of the 21st century which again proves
                that all you need is the right vision, determination, altruism and sound management.
                Every year many hundreds of students solely from RPS achieve their dreams by seeking
                admissions in the IITs, NITs, IIMs, BITS and top Medical Institutes and other professions.
            </p>
        </div>
        <div class="col-sm-12 col-xs-12 text-center animated wow fadeInUp">
            <a class="readmore" href="About.aspx">read more</a></div>
    </div>
    <div class="Div_how_toReach">
        <div class="container wow animated fadeInUp">
            <h2 class="text-center">
                How To Register & Pay
            </h2>
            <div class="border_botom_center">
            </div>
            <div class="HowToFill wow animated fadeInUp">
                <div class="col-sm-2 col-xs-6 wow animated fadeInUp">
                    <a onclick="ShowPopupInstruction()">
                        <img src="Images/Process/GeneralInstruction.png" alt="RPSGOS" />
                    </a>
                    <p>
                        Read the General Instructions Carefully
                    </p>
                </div>
                <div class="col-sm-1 col-xs-6 wow animated fadeInUp">
                    <img src="Images/Process/Arrow.png" alt="RPSGOS" />
                </div>
                <div class="col-sm-2 col-xs-6 wow animated fadeInUp">
                    <a onclick="ShowPopupInstruction()">
                        <img src="Images/Process/Fill-form.png" alt="RPSGOS" />
                    </a>
                    <p>
                        Fill in Your details in the application form.
                    </p>
                </div>
                <div class="col-sm-1 col-xs-6 wow animated fadeInUp">
                    <img src="Images/Process/Arrow.png" alt="RPSGOS" />
                </div>
                <div class="col-sm-2 col-xs-6 wow animated fadeInUp">
                    <a onclick="ShowPopupInstruction()">
                        <img src="Images/Process/pay-fee.png" alt="RPSGOS" />
                    </a>
                    <p>
                        Complete the process by paying the exam fee through online mode.
                    </p>
                </div>
                <div class="col-sm-1 col-xs-6 wow animated fadeInUp">
                    <img src="Images/Process/Arrow.png" alt="RPSGOS" />
                </div>
                <div class="col-sm-2 col-xs-6 wow animated fadeInUp">
                    <a onclick="ShowPopupInstruction()">
                        <img src="Images/Process/Compelete.png" alt="RPSGOS" />
                    </a>
                    <p>
                        Congratulations! You are sucessfully registered for Olympiad.
                    </p>
                </div>
                <%--<div class="col-sm-1 col-xs-6 wow animated fadeInUp">
                    <img src="Images/Process/Arrow-downlaod.png" alt="RPSGOS" />
                </div>--%>
            </div>
            <%--
            <div class="col-sm-12 col-xs-12 text-center wow animated fadeInUp">
                <p class="Offline">
                    Offline: Purchase Registration from, fill up and submit at any campus of RPS
                    Group of Schools.
                </p>
            </div>--%>
        </div>
    </div>
    <div class="Div_Prize">
        <div class="container wow animated fadeInUp">
            <h2 class="text-center">
                Class-wise Prizes for top 40 Students
            </h2>
            <div class="border_botom_center">
            </div>
            <div class="col-sm-12 col-xs-12 wow animated fadeInUp" style="overflow: auto">
                <table border="1" class="tblPrize">
                    <tr>
                        <th>
                            Class
                        </th>
                        <th>
                            1<sup>st</sup> Prize
                        </th>
                        <th>
                            2<sup>nd</sup> Prize
                        </th>
                        <th>
                            3<sup>rd</sup> Prize
                        </th>
                        <th>
                            4<sup>th</sup> to 30<sup>th</sup> Prize
                        </th>
                        <th>
                            31<sup>st</sup> to 40<sup>th</sup> Prize
                        </th>
                    </tr>
                    <tr>
                        <td>
                            X
                        </td>
                        <td>
                            SANTRO CAR
                        </td>
                        <td>
                            LAPTOP
                        </td>
                        <td>
                            TABLET
                        </td>
                        <td>
                            BICYCLE
                        </td>
                        <td>
                            EDUCATIONAL KIT
                        </td>
                    </tr>
                    <tr>
                        <td>
                            IX
                        </td>
                        <td>
                            SCOOTY
                        </td>
                        <td>
                            LAPTOP
                        </td>
                        <td>
                            TABLET
                        </td>
                        <td>
                            BICYCLE
                        </td>
                        <td>
                            EDUCATIONAL KIT
                        </td>
                    </tr>
                    <tr>
                        <td>
                            VIII
                        </td>
                        <td>
                            SCOOTY
                        </td>
                        <td>
                            LAPTOP
                        </td>
                        <td>
                            KINDLE
                        </td>
                        <td>
                            BICYCLE
                        </td>
                        <td>
                            EDUCATIONAL KIT
                        </td>
                    </tr>
                    <tr>
                        <td>
                            VII-VI
                        </td>
                        <td>
                            SCOOTY
                        </td>
                        <td>
                            TABLET
                        </td>
                        <td>
                            KINDLE
                        </td>
                        <td>
                            BICYCLE
                        </td>
                        <td>
                            EDUCATIONAL KIT
                        </td>
                    </tr>
                    <tr>
                        <td>
                            V-IV
                        </td>
                        <td>
                            LAPTOP
                        </td>
                        <td>
                            TABLET
                        </td>
                        <td>
                            KINDLE
                        </td>
                        <td>
                            BICYCLE
                        </td>
                        <td>
                            EDUCATIONAL KIT
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="Div_Tie ">
        <div class="container wow animated fadeInUp">
            <h2 class="text-center">
                TIE-BREAKER RULES
            </h2>
            <div class="border_botom_center">
            </div>
            <div class="col-sm-12 col-xs-12 wow animated fadeInUp" style="overflow: auto">
                <div class="col-sm-12 col-xs-12">
                    <p>
                        In case of tie-breaker in any class and on any position, the older candidate (as
                        per DOB) will be considered first. In case of tie-breaker in DOB too, the priority
                        of the subjects mentioned below shall be taken into consideration for deciding the
                        position:</p>
                    <p>
                        <b>Class-Wise Tie-Breaker Priority:</b></p>
                    <table border="1" class="tblExam">
                        <tr>
                            <th>
                                Class
                            </th>
                            <th>
                                1<sup>st</sup> Priority
                            </th>
                            <th>
                                2<sup>nd</sup> Priority (in case of tie-breaker in DOB)
                            </th>
                        </tr>
                        <tr>
                            <td>
                                IV-V
                            </td>
                            <td>
                                DOB (older in age)
                            </td>
                            <td>
                                Subject Priority- Maths, English
                            </td>
                        </tr>
                        <tr>
                            <td>
                                VI-VIII
                            </td>
                            <td>
                                DOB (older in age)
                            </td>
                            <td>
                                Subject Priority- Maths, Science
                            </td>
                        </tr>
                        <tr>
                            <td>
                                IX-X
                            </td>
                            <td>
                                DOB (older in age)
                            </td>
                            <td>
                                Subject Priority- Maths, Science
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="Div_ExamPattren wow animated fadeInUp">
        <div class="container ">
            <h2 class="text-center ">
                Exam Pattern for both Phase-I & Phase-II Exams
            </h2>
            <div class="border_botom_center">
            </div>
            <div class="col-sm-12 col-xs-12 wow animated fadeInUp" style="overflow: auto">
                <table class="tblExam" border="1">
                    <tr>
                        <th>
                            Classes
                        </th>
                        <th>
                            Subject Wise Marks
                        </th>
                        <th>
                            Total Marks
                        </th>
                        <th>
                            Time
                        </th>
                    </tr>
                    <tr>
                        <td>
                            IV to V
                        </td>
                        <td>
                            English(15)<br />
                            Maths(15)<br />
                            EVS(15)<br />
                            GK(15)
                        </td>
                        <td>
                            60 Marks
                        </td>
                        <td>
                            60 Minutes
                        </td>
                    </tr>
                    <tr>
                        <td>
                            VI to VIII
                        </td>
                        <td>
                            English(15)<br />
                            Maths(15)
                            <br />
                            Science(15) - Phy(5), Chem(5), Bio(5))<br />
                            Aptitude(15)
                        </td>
                        <td>
                            60 Marks
                        </td>
                        <td>
                            60 Minutes
                        </td>
                    </tr>
                    <tr>
                        <td>
                            IX to X
                        </td>
                        <td>
                            English(10)<br />
                            Maths(20)<br />
                            Social Science(10)<br />
                            Aptitude(10)<br />
                            Science(20) - Phy(7), Chem(7), Bio(6)
                        </td>
                        <td>
                            70 Marks
                        </td>
                        <td>
                            70 Minutes
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-12 col-xs-12 wow animated fadeInUp">
                <br />
                <br />
                <p class="Exam_P">
                    For Syllabus and more information &nbsp;<a href="Syllabus.aspx">Click Here</a></p>
            </div>
        </div>
    </div>
    <div class="ResultPopup">
        <a class="SignIn">
            <img src="SampleImages/RPSOlympiad.gif" alt="RPSOLY" /></a>
    </div>
</asp:Content>
