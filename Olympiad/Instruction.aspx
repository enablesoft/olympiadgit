﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Instruction.aspx.cs" Inherits="Olympiad.Instruction" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="UTF-8" />
    <link href="Images/Favicon.png" rel="shortcut icon" type="image/png" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="theme-color" content="#103157">
    <link href="Style/Bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Style/Bootstrap/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="Style/Style.css" rel="stylesheet" type="text/css" />
    <link href="Style/animate.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        input[type="checkbox"]:focus + label
        {
            color: red;
        }
        input[type=checkbox]
        {
            vertical-align: top;
        }
        label
        {
            display: inline-block;
            max-width: 95%;
            margin-bottom: 5px;
            font-weight: normal;
            padding-left: 10px;
            font-size: 15px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <div class="col-sm-12 col-xs-12">
            <h2>
                General Instructions
            </h2>
        </div>
        <div class="col-sm-12 col-xs-12">
            <p class="errorcode">
                <u>Note: - Before applying for registration, applicants must go through the general
                    instructions. </u>
            </p>
            <ul>
                <li>The candidate must choose the class in which he/she is studying during the current academic session 2019-20.</li>
                <li>For the Phase-I of RPS Olympiad, the candidate may opt either of the two modes of examination i.e. online and offline. </li>
                <li>For both the modes, the candidate must register online only. No manual application/registration form shall be accepted. After successful online registration and payment, the candidate will get a registration message on his/her registered mobile number. </li>
                <li>The candidate is required to pay Rs. 200/- (for Classes IV-VIII) and Rs. 300/- (for Classes IX & X) as Registration Fee (Additional transaction charges as applicable shall be paid by the applicant). Registration fee once paid shall not be refunded (full or partial) under any circumstance. After successful registration, the candidate will get user-id and password on registered mobile number.</li>
                <li>Each candidate is allowed to submit only one application form, more than one application form with the same information, is not allowed and such forms are strictly liable to be rejected.
                </li>
                <li>Phase-I exam. of RPS Olympiad (online and offline) shall be held on 02nd February, 2020 (Sunday) at 12:00 Noon And Phase- II exam. (offline only) will be held on 9th Feb. 2020 (Sunday) at 12:00 Noon </li>
                <li>For online RPS Olympiad exam., the candidate has to login by entering his user id and password through www.rpsolympiad.in. The window for online exam. (Phase-I) will open at 12:00 Noon sharp on the exam date. The candidate may attempt online exam from anywhere as per convenience through either mobile/Laptop/Desktop or in any cyber cafe with high speed uninterrupted internet facility. For failure of any internet or any other IT/ electric failure on candidate’s end during the exam, RPS Olympiad team will not be responsible and no extra time will be given to the candidate.</li>
                <li>If the candidate chooses offline mode, he/she can choose only one examination centre out of all RPS School campuses at Mahendergarh, Narnaul, Rewari, Rewari(Elegant City), Behror, Dharuhera, Gurugram Sec. 50 & Sec. 89, Kosli and Hansi. The candidate must fill his/her choice of exam. centre during filling registration form. Once registration is done successfully, request for change of centre for offline mode only shall not be accepted under any circumstance. </li>
                <li>No bus facility will be available for RPS Olympiad 2020 offline exam. (Phase-I & Phase-II).</li>
                <li>For offline exam., in Phase-I & Phase-II, it is  mandatory to bring admit cards in the opted examination centre. However, the candidates who have opted online mode of examination may also download their admit cards. Other important instructions in this regard have also been given on the admit card. </li>
                <li>The result of RPS Olympiad Phase-I (both online and offline examinations) shall be declared within specific period.  The successful candidates who have qualified RPS Olympiad Phase-I (both online and offline) as per the criteria decided by the managing committee shall be called for Phase –II (Offline only). Such candidates have to re-register themselves for Phase- II examination and may opt any of the given choices of the offline exam. centres. No separate registration fee shall be charged for Phase –II examination. </li>
                <li>The candidates called for Phase- II examination must bring their valid school I- card showing the present class of academic session 2019-20.</li>
                <li>The examination pattern and class-wise syllabus shall remain the same for both the phases. Every Question will have 4 answers out of which one answer will be correct. For detailed exam. pattern and syllabus, please refer to RPS Olympiad Website: www.rpsolympiad.in </li>
                <li>There will be no negative marking in Phase-I examination but for Phase-II examination there will be ¼ negative marking for each incorrect answer.  </li>

            </ul>
            <p>
                <asp:CheckBox Text="I Agree" ID="checkConditions" runat="server" /></p>
        </div>
        <div class="col-sm-12 col-xs-12 text-right">
            <div class="col-sm-4 ">
            </div>
            <div class="col-sm-4 ">
                <br />
                <a class="btn btn-success text-uppercase" href="Registration.aspx" onclick="return onClick()"
                    style="width: 100%">Continue Registration</a>
            </div>
        </div>
    </div>
    <div style="height: 50px; width: 100%">
    </div>
    <script src="Style/Bootstrap/jquery/jquery.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        function onClick() {
            if (document.getElementById('checkConditions').checked) {
                return true;
                
            } else {
                document.getElementById('checkConditions').focus();
                return false;
            }
        }
    </script>
    </form>
</body>
</html>
