﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ManageTests" Codebehind="MakeQuestPaper.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<%--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>--%>
    <link href="Style/Bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="Style/Bootstrap/jquery/jquery.js" type="text/javascript"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <title></title>
  
          <script type = "text/javascript">
              function isNumberKey(evt) {
                  var charCode = (evt.which) ? evt.which : evt.keyCode;
                  if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                      return false;

                  return true;
              }
      </script>
</head>
<body>
    <form id="form1" runat="server">
           <div id = "dialogMsg" title = "Hey!" style="display:none">
         <asp:Label runat="server" Id="lblAlertMessage" />
        
         </div>
    <div class=" col-sm-12">
        <div class=" col-sm-2">
            Class</div>
        <div class=" col-sm-3">
            <asp:DropDownList CssClass="form-control" runat="server" ID="ddlClass" OnSelectedIndexChanged="ddlClass_SelectedIndexChanged"
                AutoPostBack="true" />
        </div>
        <div class=" col-sm-2">
            Test</div>
        <div class=" col-sm-5">
            <asp:DropDownList CssClass="form-control" runat="server" ID="ddlOnlineTests" OnSelectedIndexChanged="ddlOnlineTests_SelectedIndexChanged"
                AutoPostBack="true" />
        </div>
    </div>
    <div class=" col-sm-12 panel-primary">
      
        <div class="panel-body">
            <div class=" col-sm-12">
                <div class="col-sm-3">
                    Test
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtName" />
                </div>
                <div class="col-sm-1">
                    Neg. Marking
                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlNegMarking" 
                        onselectedindexchanged="ddlNegMarking_SelectedIndexChanged"  AutoPostBack="true">
                     <asp:ListItem Text="Select" Value="-1" />
                    <asp:ListItem Text="No" Value="0" />
                          <asp:ListItem Text="Yes" Value="1" />
                    </asp:DropDownList>
                </div>
                  <div  id="divDeductMarks" runat="server" class="col-sm-1">
                    Deduct Marks
                    <asp:TextBox CssClass="form-control" onkeypress="return isNumberKey(event)" runat="server" ID="txtDeductMarks" />
                </div>
                <div class="col-sm-2">
                    Total Questions
                    <asp:TextBox CssClass="form-control" onkeypress="return isNumberKey(event)" runat="server" ID="txtTotalNoOfQuests" />
                </div>  <div class="col-sm-2">
                    Max Marks
                    <asp:TextBox CssClass="form-control" onkeypress="return isNumberKey(event)" runat="server" ID="txtMaxMarks" />
                </div>
                <div class="col-sm-2">
                    Conduct Date
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtConductDate" />
                </div>
                 <div class="col-sm-1">
                    Time From
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtStartTime" />
                </div>
                <div class="col-sm-1">
                    Time To
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtEndTime" />
                </div>
                <div class="col-sm-1">
                    Pass Marks
                    <asp:TextBox CssClass="form-control" onkeypress="return isNumberKey(event)" runat="server" ID="txtPassingMarks" />
                </div>
               
                <div class="col-sm-1">
                Status
                    <asp:CheckBox runat="server" CssClass="form-control" ID="chkIsActive" Text="Active?" />
                </div>
                <div class="col-sm-3">
                    <asp:Button ID="Button1" CssClass=" btn btn-default" runat="server" Text="Save" OnClick="Unnamed1_Click" />
                </div>
            </div>

            <div class="col-sm-12">
            
              <div class="col-sm-4">
              <asp:Table ID="tblQuestManifest" CssClass="table table-bordered"  runat="server" />
              </div>
           
            </div>
            <asp:Button runat="server"  CssClass=" btn btn-default" ID="btnPrepareQuestionPaper" 
            Text="Prepare Question Paper" onclick="btnPrepareQuestionPaper_Click" />
           
            </div>
            <div class="col-sm-12">
              <asp:Table ID="tblQuestionPaper" CssClass="table table-bordered" runat="server" />
            
              </div>
            </div>
            
    </form>
</body>
</html>
