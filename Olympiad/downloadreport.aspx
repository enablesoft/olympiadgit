﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="downloadreport.aspx.cs"
    Inherits="Olympiad.downloadreport" MasterPageFile="~/Site1.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function disableBtn(btnID, newText) {

            var btn = document.getElementById(btnID);
            btn.disabled = true;
            btn.value = newText;
        }</script>
    <style>
        .Registration_div
        {
            font-family: 'Roboto';
            display: inline-block;
            padding: 5px;
            display: inline-block;
            background-color: #fff;
            -webkit-box-shadow: 0 1px 6px rgba(57,73,76,0.35);
            box-shadow: 0 1px 6px rgba(57,73,76,0.35);
            min-height: 400px;
            width: 100%;
            margin-top: 50px;
        }
        .InputText
        {
            width: 100%;
            display: inline-block;
            padding: 5px;
            height: 31px;
        }
    </style>
    <main> <h1>
        Download Report</h1>  
       <div class="Registration_div">
       <div class="col-sm-12 col-md-12 col-xs-12">
        <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Mode of Exam</p>
                        <asp:RadioButtonList runat="server" CssClass="rbtbutton" ID="rbtExamMode" RepeatDirection="Horizontal"
                           >
                            <asp:ListItem Text="Online" Value="1"  />
                            <asp:ListItem Text="Offline" Value="2" />
                            <asp:ListItem Text="Both" Value="3" Selected="True"/>
                        </asp:RadioButtonList>
                    </div>
                </div>
                 <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Center of Exam</p>
                        <asp:DropDownList runat="server" ID="ddl_ExamCenter" CssClass="InputText" Width="100%">
                        </asp:DropDownList>
                    </div>
                </div>
                 <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Class
                          </p>
                        <asp:DropDownList ID="ddlClass" runat="server" CssClass="InputText">
                        </asp:DropDownList>
                    </div>
                </div>
                 <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Gender</p>
                        <asp:RadioButtonList ID="rbtGender" runat="server" CssClass="rbtbutton" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Male" Value="Male"  />
                            <asp:ListItem Text="Female" Value="Female" />
                            <asp:ListItem Text="Both" Value="Both" Selected="True" />
                        </asp:RadioButtonList>
                    </div>
                </div>
                 <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            From Date    </p>                       
                        <asp:TextBox ID="txtfromdate" runat="server" CssClass="InputText">
                        </asp:TextBox>
                    </div>
                </div>
                 <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            To Date
                          </p>
                        <asp:TextBox ID="txttodate" runat="server" CssClass="InputText">
                        </asp:TextBox>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 col-md-4">
                       <div class="form-group">
                            <p>
                                Report Type</p>
                            <asp:DropDownList runat="server" ID="ddreporttype" CssClass="InputText" Width="100%">
                            <asp:ListItem Text="Only Final" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Incomplete" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                    </div> 
                </div> 
                 <div class="col-sm-4 col-xs-12 col-md-4">
                   <div class="form-group">   
               <br />
                        <asp:Button Text="Download" CssClass="btn btn-success" 
                            ID="btnRegister" runat="server"  Style="width: 100%" 
                            onclick="btnRegister_Click"   />
                       
                    </div>


       </div>
       </div>
        </main>
</asp:Content>
