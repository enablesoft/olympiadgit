﻿<%@ Page Title="Forgot Password" Language="C#" MasterPageFile="~/MasterPage.master"
    AutoEventWireup="True" Inherits="ForgotPassword" CodeBehind="~/ForgotPasword.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">@import url('https://fonts.googleapis.com/css?family=Nunito+Sans');

body {
  display: flex;
  justify-content: center;
    background: #ededed;
}

.vldform {
    box-sizing: border-box;
    width: 450px;
    display: flex;
    flex-direction: column;
    margin: 20px auto;
    padding: 35px 55px;
    background: #fff;
    font-family: "Nunito Sans";
    animation: a .5s;
    animation-fill-mode: forwards;
    border: #d4d4d4 1px solid;
    border-radius: 10px;
    box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);
}

.vldform a {
    text-decoration: none;
}

.vldform h1 {
    font-size: 40px;
    color: #263d59;
    margin: 0px 0px 26px 0px;
}

.vldform__textbox {
    border: 0;
    outline: 0;
    border-bottom: 2px #263d59 solid;
    font-size: 18px;
    margin-top: 36px;
    padding-bottom: 9px;
    font-family: "Nunito Sans";
}

.vldform__textbox[type="password"]::after {
    content: " ";
    display: block;
    width: 10px;
    height: 10px;
    background-color: rebeccapurple;
    
}

.vldform__recoverypassword{
    align-self: flex-end;
    margin: 10px 0px;
    font-size: 16px;
    color: #2E67A9;
}

.vldform__button {
    margin-top: 42px;
    height: 50px;
    border: 0;
    color: #fff;
    border-radius: 10px;
    background: #263d59;
    font-size: 22px;
    font-weight: 600;
    font-family: "Nunito Sans";
    cursor: pointer;
}

.vldform__signup {
    align-self: center;
    margin-top: 50px;
    margin-bottom: 0px;
}

.vldform__signup a {
    color: #6538B5;
    font-weight: 600;
    margin-left: 4px;
}

.vldreg {
    display: none;
}

.vldrecpass {
    display: none;
}

@keyframes a {
    0% {
        opacity: 0;
        transform: translateY(-5px)
    }

    to {
        opacity: 1;
        transform: translateY(5px)
    }
}

@media (max-width: 400px) {
    .vldform {
        width: 400px;
        padding: 20px 40px;
    }
 

    .vldform__signup {
        text-align: center;
    }

    .vldform__signup a {
        margin-left: 14px;
    }
}</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="vldform vldauth" id="Div_UserId" runat="server">
            <h1>
                Forgot Password</h1>
            <asp:RequiredFieldValidator ErrorMessage="Please Enter UserId" ControlToValidate="txtUserid"
                CssClass="errorcode" ValidationGroup="a" ID="rfvUserID" runat="server" />
            <asp:TextBox runat="server" CssClass="vldform__textbox" ID="txtUserid" placeholder="Username" />
            <br />
            <asp:Button class="vldform__button" Text="Send OTP" ID="btnSendOTP" runat="server"
                OnClick="OnClick_SendOTP" ValidationGroup="a" />
        </div>
        <div class="vldform vldauth" visible="false" id="Div_enterOTP" runat="server">
            <h1>
                Forgot Password</h1>
            <p>
                Otp Sent on your Registred Phone No.</p>
            <asp:RequiredFieldValidator ErrorMessage="Please Enter OTP" ControlToValidate="txtITP"
                CssClass="errorcode" ValidationGroup="b" ID="RequiredFieldValidator1" runat="server" />
            <asp:TextBox runat="server" CssClass="vldform__textbox" ID="txtITP" placeholder="Enterv OTP" />
            <asp:RequiredFieldValidator ErrorMessage="Please Enter Password" ControlToValidate="txtPassword"
                CssClass="errorcode" ValidationGroup="b" ID="RequiredFieldValidator2" runat="server" />
            <asp:TextBox runat="server" CssClass="vldform__textbox" ID="txtPassword" placeholder="New Password" />
            <br />
            <asp:Button class="vldform__button" Text="Change" ID="btnChangePassword" runat="server"
                ValidationGroup="b" />
        </div>
    </div>
    <%--<div class="ModelPop " id="zoomOutIn">
        <div class="LoginBox">
            <a class="Model_closebtn">X</a>
            <div class="heading">
                <h2>
                    heading</h2>
            </div>
            <div class="PopDesc">
                <table border="1" class="tblExam">
                    <tr>
                        <td>
                            <b>ACK No :</b>
                        </td>
                        <td>
                            <p>
                                <asp:Label ID="lblAckNo" Text="1234567898" runat="server" />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>NAME :</b>
                        </td>
                        <td>
                            <p>
                                <asp:Label ID="lblName" Text="Name" runat="server" />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>F NAME :</b>
                        </td>
                        <td>
                            <p>
                                <asp:Label ID="lblFName" Text="F Name" runat="server" />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>M NAME :</b>
                        </td>
                        <td>
                            <p>
                                <asp:Label ID="lblMName" Text="M Name" runat="server" />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Email :</b>
                        </td>
                        <td>
                            <p>
                                <asp:Label ID="lblEmail" Text="Email" runat="server" />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mobile No
                        </td>
                        <td>
                            <asp:Label ID="lblContactNo" Text="1234565" runat="server" />
                        </td>
                    </tr>
                </table>
                <br />
                <asp:Button Text="Proceed to Payment" ID="btnPay" runat="server" CssClass="btn btn-success" />
                <p class="danger">
                    * Please note down ackNo for further refrences.</p>
            </div>
        </div>
    </div>--%>
</asp:Content>
