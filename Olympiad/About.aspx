﻿<%@ Page Title="RPS Olympiad 2020" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="About.aspx.cs" Inherits="Olympiad.About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="col-sm-12 col-xs-12">
            <h2>
                An introduction of All India RPS Olympiad 2020
            </h2>
        </div>
        <div class="col-sm-12 col-xs-12">
            <h4>
                (Under the aegis of RPS Education Society, Mahendergarh –Haryana)</h4>
            <p>
                Rao Pahlad Singh Education Society, since 22 years, has been in the noble service
                of enlightening the so called backward strata of the State of Haryana and the adjoining
                state Rajasthan through its educational institutions at school and college level.
                It has been established by a group of philanthropists with an objective to make
                education easy accessible even to the person of the last row.
            </p>
            <p>
                RPS Group of Schools and Colleges under the aegis of RPS Education Society, have
                been rooting the records of achievements for the last 22 years, since the inception.
                RPS institutions have proved that private enterprise in education can produce the
                greatest results with no strain on the country’s exchequer. In spite of being in
                rural area they have produced students who are self-reliant, confident and versatile
                with the courage of conviction and endowed with a spirit of adventure and creativity.
                Above all, good human beings, who are upright citizens with a strong moral fiber,
                ready to be the torchbearers of a resurgent India of the 21st century which again
                proves that all you need is the right vision, determination, altruism and sound
                management. Every year many hundreds of students solely from RPS achieve their dreams
                by seeking admissions in the IITs, NITs, IIMs, BITS and top Medical Institutes and
                other professions.
            </p>
            <p>
                The RPS Institutions have been associated with the social activities for the benefit
                of the trodden and the backward masses of the southern part of Haryana State. The
                Promoters of RPS Education Society, who are distinguished people in the area of
                administration, education, corporate industrial sector and social services, determine
                the overall direction of the growth of the students of the state. They are always
                seeking new vistas of wisdom and knowledge to stand out distinctly apart from the
                milling crowd of education providers.</p>
            <p>
                To felicitate the young minds, the promoters of RPS Education Society have unanimously
                resolved to organize an All India RPS Olympiad 2020 for the Classes IV to X in two
                Phases (I &II) on 02nd February 2020 (Phase – I) and on 9th February 2020 (Phase
                – II). This open competition shall aim at bringing forth the hidden talent of the
                students and assisting them to recognize their caliber. This is an objective type
                online and offline test based upon the choice of the candidate to be conducted as
                per the aforesaid schedule. The qualifiers of Phase-I shall be called for Phase-II
                exam as per the qualifying criterion decided by the management committee. The winners
                of Phase-II which will be an offline objective type exam., shall only be awarded
                with the declared prizes. The top 40 students in each of the mentioned classes for
                RPS Olympiad 2020 shall only be awarded with prizes in a Grand Prize Distribution
                Ceremony in the presence of renowned personalities of the country in the field of
                cinema, education, politics or sports etc. The date of the Prize Distribution Ceremony
                shall be communicated later. However, for making RPS Olympiad 2020, a transparent
                and impartial mega event, all steps have been taken very carefully yet in case of
                any dispute at any stage, the decision of the managing committee shall stand final
                and firm.
            </p>
        </div>
    </div>
</asp:Content>
