﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="testpagePwd.aspx.cs" Inherits="Olympiad.testpagePwd" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Style/Bootstrap/jquery/jquery.min.js" type="text/javascript"></script>
    <link href="Style/Bootstrap/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="Style/Bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Style/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <div class="col-sm-8 col-md-8 col-xs-12" style="margin-top: 2%">
            <div class="form-row">
                <span>Type Password here</span>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtpwd"
                    ErrorMessage="*" ValidationGroup="A" runat="server"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtpwd" Width="30%" runat="server" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtauthpwd"
                    ErrorMessage="*" ValidationGroup="A" runat="server"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtauthpwd" placeholder="Enter Authenticate Pwd" TextMode="Password"
                    runat="server" Width="50%" />
            </div>
            <div class="form-row">
                <asp:Button ID="btnenc" Text="Encrypt" runat="server" Width="40%" OnClick="btnenc_Click"
                    ValidationGroup="A" />
                <asp:Button ID="btndec" Text="Decrypt" runat="server" Width="40%" OnClick="btndec_Click"
                    ValidationGroup="A" /></div>
            <div class="form-row">
                <span>Result</span>
                <asp:TextBox ID="txtresult" Width="100%" runat="server" />
            </div>
        </div>
        <div class="col-sm-4 col-md-4 col-xs-12">
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="dashboard-summery-one">
                    <div class="row" style="display: flex; flex-wrap: wrap">
                        <div class="col-6">
                            <div class="item-icon bg-light-magenta">
                                <span class="glyphicon glyphicon-pencil" style="margin-top: 30px"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="item-content">
                                <div class="item-title">
                                    Today's Online Mode Registrations(final)
                                </div>
                                <div class="item-number">
                                    <span class="counter" id="lblonreg" data-num="12"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="dashboard-summery-one">
                    <div class="row" style="display: flex; flex-wrap: wrap">
                        <div class="col-6">
                            <div class="item-icon bg-light-magenta">
                                <span class="glyphicon glyphicon-pencil" style="margin-top: 30px"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="item-content">
                                <div class="item-title">
                                    Today's Offline Mode Registrations(final)
                                </div>
                                <div class="item-number">
                                    <span class="counter" id="lbloffreg" data-num="12"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="dashboard-summery-one">
                    <div class="row" style="display: flex; flex-wrap: wrap">
                        <div class="col-6">
                            <div class="item-icon bg-light-magenta">
                                <span class="glyphicon glyphicon-pencil" style="margin-top: 30px"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="item-content">
                                <div class="item-title">
                                    Today's Registrations(Not final)
                                </div>
                                <div class="item-number">
                                    <span class="counter" id="lblregnotfinal" data-num="12"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                getregdetails();
            });
            function getregdetails() {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "testpagepwd.aspx/getregdetails",
                    data: "{}",
                    datatype: "json", success: function (result) {
                        result = JSON.parse(result.d);
                        $("#lblonreg").text(result['Table'][0].onlinereg);
                        $("#lbloffreg").text(result['Table'][0].offlinereg);
                        $("#lblregnotfinal").text(result['Table1'][0].totalreg);
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        if (xhr.responseText.indexOf("Session Expired") > 0) {
                            //Session has Expired,redirect to login page
                            window.location.href = "../Default.aspx?Message=Session expired please login again";
                        } else {
                            //Other Exceptions/Errors
                            alert("Error Occured");
                        }
                    }
                });
            }
        </script>
    </form>
</body>
</html>
