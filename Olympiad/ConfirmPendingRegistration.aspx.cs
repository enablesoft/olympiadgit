﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Threading;

namespace Olympiad
{
    public partial class ConfirmPendingRegistration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["adminuserid"] == null)
            {
                Response.Redirect("~/Rpsoly.aspx");
            }
        }
        protected void btnconfirm_Click(object sender, EventArgs e)
        {
            try
            {
                string mobile, custmail, varHelpNo, regno, pwd, CUSTOMER, msg;

                SqlParameter[] param1 = new SqlParameter[10];
                param1[0] = new SqlParameter("@ackNo", txtackno.Text);
                param1[1] = new SqlParameter("@Amount", SqlDbType.Float);
                param1[1].Direction = ParameterDirection.Output;
                param1[2] = new SqlParameter("@msg", SqlDbType.VarChar, 100);
                param1[2].Direction = ParameterDirection.Output;
                param1[3] = new SqlParameter("@mobileno", SqlDbType.VarChar, 10);
                param1[3].Direction = ParameterDirection.Output;
                param1[4] = new SqlParameter("@email", SqlDbType.VarChar, 100);
                param1[4].Direction = ParameterDirection.Output;
                param1[5] = new SqlParameter("@RegNo", SqlDbType.BigInt);
                param1[5].Direction = ParameterDirection.Output;
                param1[6] = new SqlParameter("@Pwd", SqlDbType.VarChar, 30);
                param1[6].Direction = ParameterDirection.Output;
                param1[7] = new SqlParameter("@Name", SqlDbType.VarChar, 50);
                param1[7].Direction = ParameterDirection.Output;
                param1[8] = new SqlParameter("@helpno", SqlDbType.VarChar, 50);
                param1[8].Direction = ParameterDirection.Output;
                param1[9] = new SqlParameter("@type", rbttype.SelectedValue);

                SqlHelper.ExecuteNonQuery(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "Pro_confirmRegistration", param1);
                if (param1[2].Value.ToString() != "")
                {
                    UtilityModule.showMessage(Page, param1[2].Value.ToString());
                    return;
                }
                else
                {
                    if (rbttype.SelectedValue == "0")  //confirmed and send details
                    {


                        //Update Payment Status
                        SqlParameter[] param = new SqlParameter[8];
                        param[0] = new SqlParameter("@ACKNO", txtackno.Text);
                        param[1] = new SqlParameter("@TRANSACTIONID", txtbankid.Text);
                        param[2] = new SqlParameter("@BankAmt", param1[1].Value);
                        param[3] = new SqlParameter("@REGNO", SqlDbType.BigInt);
                        param[3].Direction = ParameterDirection.Output;
                        param[4] = new SqlParameter("@Pwd", SqlDbType.VarChar, 30);
                        param[4].Direction = ParameterDirection.Output;
                        param[5] = new SqlParameter("@Name", SqlDbType.VarChar, 50);
                        param[5].Direction = ParameterDirection.Output;
                        param[6] = new SqlParameter("@HelpNo", SqlDbType.VarChar, 50);
                        param[6].Direction = ParameterDirection.Output;
                        param[7] = new SqlParameter("@Manualentry", 1);
                        SqlHelper.ExecuteNonQuery(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "PRO_REGISTRATIONFINAL", param);
                        mobile = param1[3].Value.ToString();
                        custmail = param1[4].Value.ToString();
                        regno = param[3].Value.ToString();
                        pwd = param[4].Value.ToString();
                        CUSTOMER = param[5].Value.ToString();
                        varHelpNo = param[6].Value.ToString();
                        msg = "Ack No. confirmed & send confirmation Details Successfully.";
                    }
                    else
                    {
                        mobile = param1[3].Value.ToString();
                        custmail = param1[4].Value.ToString();
                        regno = param1[5].Value.ToString();
                        pwd = param1[6].Value.ToString();
                        CUSTOMER = param1[7].Value.ToString();
                        varHelpNo = param1[8].Value.ToString();
                        msg = "Send confirmation Details Successfully.";
                    }
                    //**********************
                    Thread email = new Thread(delegate()
                    {

                        string MSG = @"Dear " + CUSTOMER + Environment.NewLine + "Congratulations! Registered sucessfully for All India RPS Olympiad 2020 Phase-I. Your Registration No. is " + regno + ", Login id is " + regno + "  and pwd : " + pwd + "" + Environment.NewLine + "For any query please contact:" + varHelpNo;
                        SendMail.FireSms(mobile, MSG, varUnicode: "0");
                        string htmlString = @"<html>
                      <body>
                      <p>Dear " + CUSTOMER + @" ,</p>
                      <p>Thank you for Registering for Rps 2020 Olympiad. Your account has now been activated.</p>
                      <br></br>
                      <b>To Login:</b><br/>
                      UserName : " + regno + @"<br/>
                      Password : " + pwd + @"
                      </body>
                      </html>
                     ";
                        if (custmail != "")
                        {
                            SendMail.sendmail(pTo: custmail, pSubject: "Registration: Rps 2020 Olympiad", pBody: htmlString, pFromSMTP: "smtp_1", pFromDispName: "", pCC: "", pAttchmntName: "", pBCC: "");
                        }
                    });
                    email.IsBackground = true;
                    email.Start();
                    //**********************
                }
                UtilityModule.showMessage(Page, msg);
                txtackno.Text = "";
                txtbankid.Text = "";
            }
            catch (Exception)
            {
                UtilityModule.showMessage(Page, "Erro Occured.Please try again");
            }

        }
    }
}