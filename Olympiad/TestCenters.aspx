﻿<%@ Page Title="RPS Olympiad 2020: Test Centres" Language="C#" MasterPageFile="~/MasterPage.master"
    AutoEventWireup="true" Inherits="TestCenters" CodeBehind="TestCenters.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="col-sm-12 col-xs-12">
            <h2 class="text-center">
                Test Centres</h2>
        </div>
        <div class="col-sm-6 col-md-4 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    RPS Mahendergarh
                </div>
                <div class="panel-body">
                    <p>
                        <i class=" fa fa-map-marker"></i>&nbsp;3 <sup>rd</sup> Mile Stone, Satnali Road,
                        M.garh, Hr.
                    </p>
                    <p>
                        <i class=" fa fa-phone"></i>&nbsp; 01285-222645, 9416150201</p>
                    <p>
                        <i class=" fa fa-envelope"></i>&nbsp; rpskhatod@gmail.com
                    </p>
                    <p>
                        <a href="http://www.rpsmgarh.edu.in" target="_blank"><i class=" fa fa-globe"></i>&nbsp;
                            www.rpsmgarh.edu.in</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Narnaul Campus
                </div>
                <div class="panel-body">
                    <p>
                        <i class=" fa fa-map-marker"></i>&nbsp;4 <sup>th</sup> Mile Stone, Rewari Road,
                        Narnaul, Hr.
                    </p>
                    <p>
                        <i class=" fa fa-phone"></i>&nbsp; 01282-248014, 8198976666
                    </p>
                    <p>
                        <i class=" fa fa-envelope"></i>&nbsp; rpsschoolnarnaul@gmail.com
                    </p>
                    <p>
                        <a href="http://www.rpsnarnaul.edu.in" target="_blank"><i class=" fa fa-globe"></i>&nbsp;
                            www.rpsnarnaul.edu.in</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Gurugram Sec.50
                </div>
                <div class="panel-body">
                    <p>
                        <i class=" fa fa-map-marker"></i>&nbsp; Opp. Arcadia Market, Sec 50 Gurugram
                    </p>
                    <p>
                        <i class=" fa fa-phone"></i>&nbsp; 8409989898
                    </p>
                    <p>
                        <i class=" fa fa-envelope"></i>&nbsp; rpsgurugram50@gmail.com
                    </p>
                    <p>
                        <a href="http://www.rpsgurugram50.edu.in" target="_blank"><i class=" fa fa-globe"></i>
                            &nbsp; www.rpsgurugram50.edu.in</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Dharuhera Campus
                </div>
                <div class="panel-body">
                    <p>
                        <i class=" fa fa-map-marker"></i>&nbsp;Dharuhera-Bhiwadi Expressway, Dharuhera,
                        Hr.
                    </p>
                    <p>
                        <i class=" fa fa-phone"></i>&nbsp; 01274-297457, 9875053504
                    </p>
                    <p>
                        <i class=" fa fa-envelope"></i>&nbsp; rpsdharuhera@gmail.com
                    </p>
                    <p>
                        <a href="http://www.rpsdharuhera.edu.in" target="_blank"><i class=" fa fa-globe"></i>
                            &nbsp; www.rpsdharuhera.edu.in</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Rewari (Elegant City) Campus
                </div>
                <div class="panel-body">
                    <p>
                        <i class=" fa fa-map-marker"></i>&nbsp;BMG Elegant City, Garhi Bolni Road Sec 26,
                        Rewari
                    </p>
                    <p>
                        <i class=" fa fa-phone"></i>&nbsp; 8222008806
                    </p>
                    <p>
                        <i class=" fa fa-envelope"></i>&nbsp; rpsrewari2@gmail.com
                    </p>
                    <p>
                        <a href="http://www.rpsrewari2.edu.in" target="_blank"><i class=" fa fa-globe"></i>&nbsp;
                            www.rpsrewari2.edu.in</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Kosli Campus
                </div>
                <div class="panel-body">
                    <p>
                        <i class=" fa fa-map-marker"></i>&nbsp;Malesiyawas Road, Kosli (Rewari) Hr.
                    </p>
                    <p>
                        <i class=" fa fa-phone"></i>&nbsp; 01259-275946, 9255148141
                    </p>
                    <p>
                        <i class=" fa fa-envelope"></i>&nbsp; rpskosli@gmail.com
                    </p>
                    <p>
                        <a href="http://www.rpskosli.edu.in" target="_blank"><i class=" fa fa-globe"></i>&nbsp;
                            www.rpskosli.edu.in</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Rewari Campus
                </div>
                <div class="panel-body">
                    <p>
                        <i class=" fa fa-map-marker"></i>&nbsp;6<sup>th</sup> Milestone, Delhi Road, Rewari,Hr
                    </p>
                    <p>
                        <i class=" fa fa-phone"></i>&nbsp; 01274-240645, 8222000359
                    </p>
                    <p>
                        <i class=" fa fa-envelope"></i>&nbsp; rpsrewari@gmail.com
                    </p>
                    <p>
                        <a href="http://www.rpsrewari.edu.in" target="_blank"><i class=" fa fa-globe"></i>&nbsp;
                            www.rpsrewari.edu.in</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Gurugram Sec 89
                </div>
                <div class="panel-body">
                    <p>
                        <i class=" fa fa-map-marker"></i>&nbsp;Central Periphery Road, Gurugram Sec-89
                    </p>
                    <p>
                        <i class=" fa fa-phone"></i>&nbsp; 0124-656511, 8222999182
                    </p>
                    <p>
                        <i class=" fa fa-envelope"></i>&nbsp; rpsgurugram89@gmail.com
                    </p>
                    <p>
                        <a href="http://www.rpsgurugram89.edu.in" target="_blank"><i class=" fa fa-globe"></i>
                            &nbsp; www.rpsgurugram89.edu.in</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Hansi Campus
                </div>
                <div class="panel-body">
                    <p>
                        <i class=" fa fa-map-marker"></i>&nbsp;8<sup>th</sup>Mile stone, Delhi Road, Hansi
                        Hissar
                    </p>
                    <p>
                        <i class=" fa fa-phone"></i>&nbsp; 9813196969, 9896100407
                    </p>
                    <p>
                        <i class=" fa fa-envelope"></i>&nbsp; rpshansi@gmail.com
                    </p>
                    <p>
                        <a href="http://www.rpshansi.edu.in" target="_blank"><i class=" fa fa-globe"></i>&nbsp;
                            www.rpshansi.edu.in</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Behror Campus
                </div>
                <div class="panel-body">
                    <p>
                        <i class=" fa fa-map-marker"></i>&nbsp;Sotanala, Behror (Raj.)
                    </p>
                    <p>
                        <i class=" fa fa-phone"></i>&nbsp; 9529678004</p>
                    <p>
                        <i class=" fa fa-envelope"></i>&nbsp; rpsschoolbehror@gmail.com
                    </p>
                    <p>
                        <a href="http://www.rpsbehror.edu.in" target="_blank"><i class=" fa fa-globe"></i>&nbsp;
                            www.rpsbehror.edu.in</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Hisar Campus
                </div>
                <div class="panel-body">
                    <p>
                        <i class=" fa fa-map-marker"></i>&nbsp;Huda Sec-16-17, Opp. Civil
                        Line Police Station, Hisar
                    </p>
                    <p>
                        <i class=" fa fa-phone"></i>&nbsp; 9416150201
                    </p>
                    <p>
                        <i class=" fa fa-envelope"></i>&nbsp; rpskhatod@gmail.com
                    </p>
                    <p>
                        <a href="http://www.rpsmgarh.edu.in" target="_blank"><i class=" fa fa-globe"></i>&nbsp;
                            www.rpsmgarh.edu.in</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
