﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="AdminOnlineTestConfig" Codebehind="AddQuestions.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
   rel = "stylesheet">
       <link href="Style/Bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="Style/Bootstrap/jquery/jquery.js" type="text/javascript"></script>
<%--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>--%>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <title></title>
    <script type = "text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
      </script>
</head>
<body>

    <form id="form1" runat="server">
       <div id = "dialogMsg" title = "Hey!" style="display:none">
         <asp:Label runat="server" Id="lblAlertMessage" />
        
         </div>
    <div class=" col-sm-12">
        <div class=" col-sm-2">
            Class</div>
        <div class=" col-sm-3">
            <asp:DropDownList CssClass="form-control" runat="server" ID="ddlClass" OnSelectedIndexChanged="ddlClass_SelectedIndexChanged"
                AutoPostBack="true" />
        </div>
        </div>
      
   
    <div class="card">
       
            <div class=" col-sm-12">
                <div class=" panel-heading">
                    Add Question
                </div>
                <div class="panel-body">
                    <div class=" col-sm-12">
                     <div class=" col-sm-2">
                            Subject
                            <asp:DropDownList runat="server" CssClass="form-control" ID="ddlQuestSubject" />
                        </div>
                        <div class=" col-sm-2">
                            Difficulty Level
                            <asp:DropDownList runat="server" CssClass="form-control" ID="ddlQuestDiffLevel" />
                        </div>
                        <div class=" col-sm-2">
                            Marks
                            <asp:TextBox runat="server" onkeypress="return isNumberKey(event)" CssClass="form-control" ID="txtQuestMarks" ClientIDMode="Static" placeholder="Marks" />
                        </div>
                         <div class=" col-sm-2">
                            Status<asp:CheckBox ID="chkQuestIsActive" CssClass="form-control" Checked="true" runat="server"
                                Text="Active?" /></div>
                        <div class=" col-sm-7">
                            Question
                            <asp:TextBox runat="server" ID="txtQuestName" CssClass="form-control" placeholder="Question Text" TextMode="MultiLine" MaxLength="400" />
                        </div>
                        <div class="col-sm-3">
                        Image
                            <asp:FileUpload ID="fuQuestImage"  onchange="this.form.submit()" CssClass=" form-control" runat="server" />
                        </div>
                        <div class="col-sm-2"> <asp:Image ID="imgQuestImage" runat="server" Width="50" Height="50" /></div></div>
                       
                       
                        <div class=" col-sm-12">
                            <asp:GridView ID="gridViewOptions" CssClass="table table-condensed table-striped" runat="server"
                                ShowFooter="true" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField DataField="RowNumber" HeaderText="Sr." />
                                    <asp:TemplateField HeaderText="Option Title(Ex A,B,C)">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtOptionTitle" CssClass="" MaxLength="5" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Option Text">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtOptionText" CssClass="form-control" TextMode="MultiLine" runat="server" MaxLength="500"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Image">
                                        <ItemTemplate>
                                            <div class="col-sm-6">
                                                <asp:FileUpload ID="fuOptionImage" onchange="this.form.submit()" CssClass="form-control"
                                                    runat="server" /></div>
                                            <div class="col-sm-6">
                                                <asp:Image ID="imgOptionImage" runat="server" Width="50" Height="50" /></div>
                                        </ItemTemplate>
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Correct?">
                                        <ItemTemplate>
                                            <asp:CheckBox CssClass=" form-control" ID="chkIsCorrectAnswer" runat="server" />
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Right" />
                                        <FooterTemplate>
                                            <asp:Button ID="Button2" CssClass=" btn btn-link" runat="server" Text="Remove Last"
                                                OnClick="RemoveLastRowFromOptionsGrid" />
                                            <asp:Button ID="btnAddNewOption" CssClass=" btn btn-link" runat="server" Text="Add"
                                                OnClick="AddNewRowToOptionsGrid" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class=" col-sm-2">
                            <label>
                            </label>
                            <asp:Button runat="server" CssClass=" btn btn-success" ID="btnSaveQuestion" Text="Save Question"
                                OnClick="btnSaveQuestion_Click" />
                        </div>
                    </div>
                </div>
       
            </div>
            <div class=" col-sm-12 panel-primary">
                <div class=" panel-heading">
                    Questions Of this Test
                </div>
                <div class="panel-body">
                    <div class=" col-sm-12">
                        <asp:GridView DataKeyNames="QuestionId,ClassId,SubjectId,MarksOfQuestion,DifficultyLevelId" CssClass="table table-condensed table-striped" runat="server" ID="gridQuestions"  AutoGenerateColumns="false" >
                        <Columns>
                        				
                         <asp:BoundField DataField="Question" HeaderText="Question" />
                          <asp:BoundField DataField="Class" HeaderText="Class" Visible="false" />
                           <asp:BoundField DataField="Subject" HeaderText="Subject"  />
                            <asp:BoundField DataField="TestName" HeaderText="Test Name" Visible="false"/>
                             <asp:BoundField DataField="DifficultyLevel" HeaderText="Difficulty Level" />
                             <asp:TemplateField HeaderText="Is Correct?">
                                        <ItemTemplate>
                                            <asp:Button CssClass="btn btn-link" Text="Edit" ID="btnEditQuestion" OnClick="btnEditQuestOnClick" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>


    </form>
  
</body>
</html>
