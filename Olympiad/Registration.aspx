﻿<%@ Page Language="C#" Title="RPS Olympiad 2020: Registration" AutoEventWireup="true"
    Inherits="Registration" CodeBehind="Registration.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <script src="../Style/Bootstrap/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="../Style/Bootstrap/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="../Style/Bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../Style/Bootstrap/font-awesome/css/font-awesome.css" rel="stylesheet"
        type="text/css" />
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Roboto);
        body
        {
            background-color: #f9f9f9;
        }
        .Registration_div
        {
            font-family: 'Roboto';
            display: inline-block;
            padding: 5px;
            display: inline-block;
            background-color: #fff;
            -webkit-box-shadow: 0 1px 6px rgba(57,73,76,0.35);
            box-shadow: 0 1px 6px rgba(57,73,76,0.35);
            min-height: 400px;
            width: 100%;
            margin-top: 50px;
        }
        .Registration_div > h2
        {
            background-color: #2c276e;
            width: 100%;
            padding: 10px;
            margin: 0px;
            color: #fff;
            text-align: center;
            margin-bottom: 20px;
        }
        .InputText
        {
            width: 100%;
            display: inline-block;
            padding: 5px;
            height: 31px;
        }
        .rbtbutton
        {
            width: 100%;
            border: 1px solid silver;
        }
        .rbtbutton td
        {
            padding: 3px;
        }
        .rbtbutton td > input[type="radio"]
        {
        }
        .rbtbutton td > label
        {
            margin-left: 10px;
            margin-bottom: 0px;
        }
        .errorCode
        {
            color: Red;
        }
        .Registration_div p
        {
            margin: 0px 0px 5px 0px;
            color: #7e7e7e;
        }
        hr
        {
            display: inline-block;
            margin: 10px 0px;
            background-color: Gray;
            height: 1px;
            width: 100%;
        }
        
        .ModelPop
        {
            position: fixed;
            z-index: 999;
            background: rgba(0, 0, 0, 0.79);
            top: 0px;
            bottom: 0px;
            left: 0px;
            width: 100%;
            display: inline-block;
            overflow: hidden;
            overflow: auto;
        }
        .ModelPop.collapse
        {
            display: none;
        }
        
        .PopDesc
        {
            width: 100%;
            display: inline-block;
            padding: 10px;
            background-color: #fff;
        }
        
        .popuptable
        {
            width: 100%;
            color: #000;
            border-color: #263d59;
        }
        .colorwhite
        {
            color: #fff;
        }
        .popuptable th
        {
            padding: 10px;
            text-align: center;
            background: #263d59;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
            letter-spacing: 1px;
        }
        
        .popuptable td
        {
            font-weight: normal;
            padding: 10px;
            text-align: center;
        }
        
        #zoomOutIn.ModelPop.fade .LoginBox
        {
            -webkit-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
            top: 120px;
            opacity: 0;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            transition: all 0.3s;
        }
        
        #zoomOutIn.ModelPop.fade.in .LoginBox
        {
            -webkit-transform: scale(1);
            -moz-transform: scale(1);
            -ms-transform: scale(1);
            transform: scale(1);
            -webkit-transform: translate3d(0, -120px, 0);
            transform: translate3d(0, -120px, 0);
            opacity: 1;
        }
        .Model_closebtn
        {
            display: inline-block;
            position: absolute;
            right: 0px;
            top: 0px;
            color: #fff;
            z-index: 999;
            padding: 5px;
        }
        .Model_closebtn:hover
        {
            color: #fff;
        }
        .errorcode
        {
            color: Red;
        }
        .LoginBox
        {
            box-sizing: border-box;
            width: 370px;
            position: relative;
            margin: 30px auto;
            display: block;
            flex-direction: column;
            animation-fill-mode: forwards;
            box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);
            border-radius: 5px;
        }
        .LoginBox > .heading
        {
            padding: 10px;
            display: inline-block;
            width: 100%;
            background-color: #3F51B5;
            color: #fff;
        }
        .Login_Desc
        {
            padding: 10px;
            display: inline-block;
            width: 100%;
            background-color: #fff;
        }
        .LoginBox > .Login_Desc > input
        {
            width: 100%;
            display: inline-block;
            margin-bottom: 15px;
            padding: 15px;
            border: 1px solid #8e8f93;
            border-radius: 50px;
            outline: none;
        }
        .recoverypassword
        {
            text-align: right;
            display: inline-block;
            padding: 5px;
            width: 100%;
        }
        .LoginBox > .Login_Desc > input[type="submit"]
        {
            background-color: #F44336;
            color: #fff;
            font-size: 20px;
            padding: 10px;
        }
        .Registration_div > h2
        {
            display: inline-block;
            float: left;
        }
        .backbutton
        {
            display: inline-block;
            padding: 5px 10px;
            border: 2px solid white;
            border-radius: 4px;
            color: #fff;
            text-align: right;
            font-size: 15px;
            float: right;
        }
        .backbutton:hover
        {
            color: #fff;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <div class="alert alert-danger alert-dismissible " id="AlertDiv" runat="server" style="position: relative;"
            visible="false">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <p class="text-center">
                <asp:Label ID="lblMsg" runat="server" CssClass="trTextError2"></asp:Label>
            </p>
        </div>
        <div class="Registration_div">
            <h2>
                Registration Form <a href="Default.aspx" class="backbutton">Back</a>
            </h2>
            <div class="col-sm-12 col-xs-12">
                <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Mode of Exam</p>
                        <asp:RadioButtonList runat="server" CssClass="rbtbutton" ID="rbtExamMode" RepeatDirection="Horizontal"
                            AutoPostBack="true" OnSelectedIndexChanged="rbtExamMode_SelectedIndexChanged">
                            <asp:ListItem Text="Online" Value="1" Selected="True" />
                            <asp:ListItem Text="Offline" Value="2" />
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Center of Exam(If You Choose offline)</p>
                        <asp:DropDownList runat="server" ID="ddl_ExamCenter" CssClass="InputText">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Class Studying in (2019-20)
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                                ControlToValidate="ddlClass" InitialValue="-1" CssClass="errorCode" ValidationGroup="R">
                            </asp:RequiredFieldValidator></p>
                        <asp:DropDownList ID="ddlClass" runat="server" CssClass="InputText">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Gender</p>
                        <asp:RadioButtonList ID="rbtGender" runat="server" CssClass="rbtbutton" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Male" Value="Male" Selected="True" />
                            <asp:ListItem Text="Female" Value="Female" />
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Name of the Candidate
                            <asp:RequiredFieldValidator ErrorMessage="*" ControlToValidate="txtName" ValidationGroup="R"
                                CssClass="errorCode" runat="server" /></p>
                        <asp:TextBox ID="txtName" runat="server" CssClass="InputText" Style="text-transform: uppercase"
                            MaxLength="50">
                        </asp:TextBox>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Name of the Father
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ControlToValidate="txtFatherName"
                                ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                        <asp:TextBox ID="txtFatherName" runat="server" CssClass="InputText" Style="text-transform: uppercase"
                            MaxLength="50">
                        </asp:TextBox>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Name of the Mother
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="*" ControlToValidate="txtMother"
                                ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                        <asp:TextBox ID="txtMother" runat="server" CssClass="InputText" Style="text-transform: uppercase"
                            MaxLength="50">
                        </asp:TextBox>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Date of Birth
                            <asp:RegularExpressionValidator ID="revDate1" ErrorMessage="Format should be dd/MM/YYYY"
                                CssClass="errorCode" ValidationGroup="R" ControlToValidate="txtDOB" runat="server"
                                ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/]\d{4}" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="*" ControlToValidate="txtDOB"
                                ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                        <asp:TextBox ID="txtDOB" runat="server" CssClass="InputText" placeholder="dd/MM/YYYY">
                        </asp:TextBox>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Email Address
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtEmail"
                                ForeColor="Red" ValidationExpression="^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                Display="Dynamic" ErrorMessage="Invalid email address" ValidationGroup="R" />
                        </p>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="InputText" MaxLength="50">
                        </asp:TextBox>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Contact No
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="*" ControlToValidate="txtcontactno"
                                ValidationGroup="R" CssClass="errorCode" runat="server" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtcontactno"
                                ErrorMessage="Invalid Mobile No." ValidationExpression="[0-9]{10}" ValidationGroup="R"
                                CssClass="errorCode"></asp:RegularExpressionValidator></p>
                        <asp:TextBox ID="txtcontactno" runat="server" CssClass="InputText" MaxLength="10">
                        </asp:TextBox>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 col-md-4">
                    <div class="form-group">
                        <p>
                            Alternate Contact No.
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtaltcontactno"
                                ErrorMessage="Invalid" ValidationExpression="[0-9]{10}" ValidationGroup="R" CssClass="errorCode"></asp:RegularExpressionValidator>
                        </p>
                        <asp:TextBox ID="txtaltcontactno" runat="server" CssClass="InputText" MaxLength="10">
                        </asp:TextBox>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="icon-bar"></i><b>Complete Address</b></div>
                        <div class="panel-body">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <div class="form-group">
                                    <p>
                                        Address
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="*" ControlToValidate="txtAddress"
                                            ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                                    <asp:TextBox ID="txtAddress" runat="server" CssClass="InputText" MaxLength="100">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 col-md-6">
                                <div class="form-group">
                                    <p>
                                        City
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="*" ControlToValidate="txtcity"
                                            ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                                    <asp:TextBox ID="txtcity" runat="server" CssClass="InputText" MaxLength="50">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 col-md-6">
                                <div class="form-group">
                                    <p>
                                        Pin Code
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator10" ErrorMessage="*" ControlToValidate="txtpincode"
                                            ValidationGroup="R" CssClass="errorCode" runat="server" />--%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtpincode"
                                            ErrorMessage="Invalid" ValidationExpression="[0-9]{6}" ValidationGroup="R" CssClass="errorCode"></asp:RegularExpressionValidator></p>
                                    <asp:TextBox ID="txtpincode" runat="server" CssClass="InputText" MaxLength="6" placeholder="(Max 6 characters)">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        <p>
                                            State</p>
                                        <asp:DropDownList runat="server" ID="ddlstate_comp" CssClass="InputText">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="icon-bar"></i><b>Present School Detail</b></div>
                        <div class="panel-body">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <div class="form-group">
                                    <p>
                                        School Name
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ErrorMessage="*" ControlToValidate="txtschoolname"
                                            ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                                    <asp:TextBox ID="txtschoolname" runat="server" CssClass="InputText" MaxLength="100">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <div class="form-group">
                                    <p>
                                        Address
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="*" ControlToValidate="txtsch_add"
                                            ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                                    <asp:TextBox ID="txtsch_add" runat="server" CssClass="InputText" MaxLength="100">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 col-md-6">
                                <div class="form-group">
                                    <p>
                                        City
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="*" ControlToValidate="txtsch_city"
                                            ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                                    <asp:TextBox ID="txtsch_city" runat="server" CssClass="InputText" MaxLength="50">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 col-md-6">
                                <div class="form-group">
                                    <p>
                                        Pin Code
                                        <%--                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ErrorMessage="*" ControlToValidate="txtsch_pincode"
                                            ValidationGroup="R" CssClass="errorCode" runat="server" />--%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtsch_pincode"
                                            ErrorMessage="Invalid" ValidationExpression="[0-9]{6}" ValidationGroup="R" CssClass="errorCode"></asp:RegularExpressionValidator></p>
                                    <asp:TextBox ID="txtsch_pincode" runat="server" CssClass="InputText" MaxLength="6"
                                        placeholder="(Max 6 characters)">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        <p>
                                            State</p>
                                        <asp:DropDownList runat="server" ID="ddlstate_schadd" CssClass="InputText">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 col-md-6">
                                <div class="form-group">
                                    <p>
                                        Your School is Affiliated to</p>
                                    <asp:TextBox ID="txtSchoolAffiliatedTo" runat="server" CssClass="InputText" Style="text-transform: uppercase"
                                        MaxLength="20">
                                    </asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12">
                <div class="col-sm-3 col-md-3">
                </div>
                <div class="col-sm-6 col-md-6">
                    <%--   <div class="row" style="text-align: center">
                        <asp:CheckBox ID="chkterm" CssClass="checkbox" Text="I Agree Terms  & Conditions"
                            runat="server" />
                    </div>--%>
                    <div>
                        <asp:Button Text="----REGISTRATION CLOSED----" CssClass="btn btn-success text-uppercase"
                            ValidationGroup="R" ID="btnRegister" runat="server" Style="width: 100%" />
                    </div>
                </div>
                <div class="col-sm-3 col-md-3">
                </div>
            </div>
        </div>
    </div>
    <div class="ModelPop collapse" id="zoomOutIn">
        <div class="LoginBox">
            <a class="Model_closebtn">X</a>
            <div class="heading" style="text-align: center">
                <h2>
                </h2>
            </div>
            <div class="PopDesc">
                <table border="1" class="popuptable">
                    <tr>
                        <td>
                            <b>Ack No :</b>
                        </td>
                        <td>
                            <p>
                                <asp:Label ID="lblAckNo" Text="" runat="server" />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>NAME :</b>
                        </td>
                        <td>
                            <p>
                                <asp:Label ID="lblName" Text="Name" runat="server" />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>F NAME :</b>
                        </td>
                        <td>
                            <p>
                                <asp:Label ID="lblFName" Text="F Name" runat="server" />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>M NAME :</b>
                        </td>
                        <td>
                            <p>
                                <asp:Label ID="lblMName" Text="M Name" runat="server" />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Email :</b>
                        </td>
                        <td>
                            <p>
                                <asp:Label ID="lblEmail" Text="Email" runat="server" />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Contact No :</b>
                        </td>
                        <td>
                            <asp:Label ID="lblmobileno" Text="" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Amount :</b>
                        </td>
                        <td>
                            <asp:Label ID="lblamt" Text="1" runat="server" />
                        </td>
                    </tr>
                </table>
                <br />
                <asp:Button Text="Proceed to Payment" ID="btnPay" runat="server" CssClass="btn btn-success"
                    Style="width: 100%" OnClick="btnPay_Click" />
                <p class="danger">
                    * Please note Ack No for future reference.</p>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            setTimeout(function () {
                $(".trTextError2").val = '';
                $(".alert").hide('blind', {}, 500)
            }, 5000);
        });
        $(".Model_closebtn").click(function () {

            $("#zoomOutIn").hide("slow");
        }); function ShowPopup() {
            $("#zoomOutIn").show("slow");
        }</script>
    </form>
</body>
</html>
