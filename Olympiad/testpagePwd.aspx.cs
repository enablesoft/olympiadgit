﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json;
namespace Olympiad
{

    public partial class testpagePwd : System.Web.UI.Page
    {
        static int pwdtype = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnenc_Click(object sender, EventArgs e)
        {
            pwdtype = 1;
            showpassword();
        }
        protected void showpassword()
        {
            if (txtauthpwd.Text != "mk123")
            {
                UtilityModule.showMessage(Page, "Invalid Authentication Password.");
                return;
            }
            if (pwdtype == 1) //encrypt
            {
                txtresult.Text = PasswordEncrypt_Decrypt.Encrypt(txtpwd.Text);
            }
            else if (pwdtype == 2)
            {
                txtresult.Text = PasswordEncrypt_Decrypt.Decrypt(txtpwd.Text);
            }
        }

        protected void btndec_Click(object sender, EventArgs e)
        {
            pwdtype = 2;
            showpassword();
        }
        [WebMethod]
        public static string getregdetails()
        {
            //if (HttpContext.Current.Session["varuserid"] == null)
            //{
            //    throw new Exception("Session Expired");
            //}
            //else
            //{
            string str = @"select isnull(Sum(case when [Exam Mode]='ONLINE' then 1 else 0 end),0) as onlinereg,
                        isnull(Sum(case when [Exam Mode]='OFFLINE' then 1 else 0 end),0) as offlinereg
                        From v_finalregistration where cast(dateadded as date)='" + System.DateTime.Now.ToString("dd-MMM-yyyy") + "'";
            str = str + @" select isnull(Count(RegNo),0) as totalreg
                        From v_registration where cast(dateadded as date)='" + System.DateTime.Now.ToString("dd-MMM-yyyy") + "'";
            DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, str);
            return JsonConvert.SerializeObject(ds);
            //}
        }
    }
}