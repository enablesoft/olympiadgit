﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
public partial class Registration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["acceptcond"] == null || Session["acceptcond"].ToString() != "1")
        {
            Response.Redirect("default.aspx");
            return;
        }
        if (!IsPostBack)
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "PRO_FILLREGCOMBO", param);
            UtilityModule.ConditionalComboFillWithDs(ref ddl_ExamCenter, ds, 0, true, "--Select--");
            UtilityModule.ConditionalComboFillWithDs(ref ddlstate_comp, ds, 1, true, "--Select--");
            UtilityModule.ConditionalComboFillWithDs(ref ddlstate_schadd, ds, 1, true, "--Select--");
            UtilityModule.ConditionalComboFillWithDs(ref ddlClass, ds, 2, true, "--Select--");

            ddl_ExamCenter.Enabled = false;
        }
    }
    protected void OnclickbtnClear(object sender, EventArgs e)
    {
        ClearForm();
    }
    private void ClearForm()
    {
        rbtExamMode.SelectedValue = "1";
        rbtexammodechanged();
        ddl_ExamCenter.SelectedIndex = 0;
        ddlClass.SelectedIndex = 0;
        rbtGender.SelectedIndex = 0;
        txtName.Text = "";
        txtFatherName.Text = "";
        txtMother.Text = "";
        txtDOB.Text = "";
        txtEmail.Text = "";
        txtcontactno.Text = "";
        txtaltcontactno.Text = "";
        txtAddress.Text = "";
        txtcity.Text = "";
        txtpincode.Text = "";
        ddlstate_comp.SelectedIndex = 0;
        txtsch_add.Text = "";
        txtsch_city.Text = "";
        txtsch_pincode.Text = "";
        ddlstate_schadd.SelectedIndex = 0;
        txtSchoolAffiliatedTo.Text = "";
    }
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        try
        {
            if (rbtExamMode.SelectedValue == "2") //offline
            {
                if (ddl_ExamCenter.SelectedIndex <= 0)
                {
                    AlertDiv.Visible = true;
                    lblMsg.Text = "Please select Exam center.";
                    ddl_ExamCenter.Focus();
                    return;
                }
            }
            SqlParameter[] param = new SqlParameter[30];
            param[0] = new SqlParameter("@AckNo", SqlDbType.BigInt);
            param[0].Direction = ParameterDirection.Output;
            param[1] = new SqlParameter("@Exammode", rbtExamMode.SelectedValue);
            param[2] = new SqlParameter("@Centreid", ddl_ExamCenter.SelectedValue);
            param[3] = new SqlParameter("@classid", ddlClass.SelectedValue);
            param[4] = new SqlParameter("@gender", rbtGender.SelectedValue);
            param[5] = new SqlParameter("@Name", txtName.Text);
            param[6] = new SqlParameter("@FName", txtFatherName.Text);
            param[7] = new SqlParameter("@MName", txtMother.Text);
            param[8] = new SqlParameter("@Dob", txtDOB.Text);
            param[9] = new SqlParameter("@Email", txtEmail.Text);
            param[10] = new SqlParameter("@ContactNo", txtcontactno.Text);
            param[11] = new SqlParameter("@AlternateContNo", txtaltcontactno.Text);
            param[12] = new SqlParameter("@Address", txtAddress.Text);
            param[13] = new SqlParameter("@City", txtcity.Text);
            param[14] = new SqlParameter("@Pincode", txtpincode.Text);
            param[15] = new SqlParameter("@Stateid", ddlstate_comp.SelectedValue);
            param[16] = new SqlParameter("@Sch_address", txtsch_add.Text);
            param[17] = new SqlParameter("@Sch_City", txtsch_city.Text);
            param[18] = new SqlParameter("@Sch_pincode", txtsch_pincode.Text);
            param[19] = new SqlParameter("@sch_stateid", ddlstate_schadd.SelectedValue);
            param[20] = new SqlParameter("@Affiliated", txtSchoolAffiliatedTo.Text);
            param[21] = new SqlParameter("@Msg", SqlDbType.VarChar, 100);
            param[21].Direction = ParameterDirection.Output;
            param[22] = new SqlParameter("@RegAmt", SqlDbType.Float);
            param[22].Direction = ParameterDirection.Output;
            param[23] = new SqlParameter("@schoolname", txtschoolname.Text);

            SqlHelper.ExecuteNonQuery(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "pro_registration", param);
            if (param[21].Value.ToString() == "")
            {
                float amt = (float)Convert.ToDouble(param[22].Value.ToString());
                //for dummy
                // amt = 1;
                string ackno = param[0].Value.ToString();
                lblAckNo.Text = ackno;
                lblamt.Text = amt.ToString();
                lblName.Text = txtName.Text.ToUpper();
                lblFName.Text = txtFatherName.Text.ToUpper();
                lblMName.Text = txtMother.Text.ToUpper();
                lblEmail.Text = txtEmail.Text;
                lblmobileno.Text = txtcontactno.Text;
                ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup();", true);
            }
            else
            {
                lblMsg.Text = param[21].Value.ToString();
                AlertDiv.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = "Error Occured.Please try again.";
            AlertDiv.Visible = true;
        }
    }
    private void InitAirPay(float Amnt, string phone, string EmailId, string Name, string Address, string City, string State, string PinCode, string ackno)
    {
        string sfurl = "http://localhost:53051/Olympiad/paymentresponse.aspx";
        //"http://localhost:53051/Olympiad/paymentresponse.aspx";

        sfurl = "https://rpsolympiad.in/paymentresponse.aspx";

        string TransactionId = ackno;
        string fUrl = sfurl;

        //************Dummy Merchant IDs
        //Merchant Id	24516
        //Username	2953945
        //Password	2YfVuCSV
        //API key	rAa9fvRTuMx5gGMZ

        //MySession.AirPaySecretKey = "rAa9fvRTuMx5gGMZ";
        //MySession.AirPayUserName = "2953945";
        //MySession.AirPayPassWord = "2YfVuCSV";
        //MySession.AirPayMerchantId = "24516";

        //Actual Ids 
        MySession.AirPaySecretKey = "ckMkBPfchJ8BWWdD";
        MySession.AirPayUserName = "9498511";
        MySession.AirPayPassWord = "6CPXXPc8";
        MySession.AirPayMerchantId = "29658";

        areAirPayValuesValid(EmailId, phone, Name, Address, City, State, PinCode, Amnt.ToString(), TransactionId);

        string allParamValue = EmailId + Name + Address + City + State + "India" + Amnt.ToString() + TransactionId;
        DateTime now1 = DateTime.Today; // As DateTime
        string now = now1.ToString("yyyy-MM-dd"); // As String
        string allParamValue1 = allParamValue + now;
        string sTemp = MySession.AirPaySecretKey + "@" + MySession.AirPayUserName + ":|:" + MySession.AirPayPassWord;

        byte[] bytClearString = System.Text.ASCIIEncoding.UTF8.GetBytes(sTemp);
        HashAlgorithm sha = new SHA256Managed();
        byte[] hash = sha.ComputeHash(bytClearString);
        StringBuilder hexString = new StringBuilder(hash.Length);
        for (int i = 0; i < hash.Length; i++)
        {
            hexString.Append(hash[i].ToString("x2"));
        }
        string str256Key = hexString.ToString();


        string allParamValue12 = allParamValue1 + str256Key;

        byte[] bytClearString1 = System.Text.ASCIIEncoding.UTF8.GetBytes(allParamValue12);
        MD5 md5 = new MD5CryptoServiceProvider();
        byte[] hash1 = md5.ComputeHash(bytClearString1);
        byte[] result = md5.Hash;
        StringBuilder strBuilder = new StringBuilder(hash1.Length);
        for (int i = 0; i < hash1.Length; i++)
        { strBuilder.Append(hash1[i].ToString("x2")); }

        string checksum1 = strBuilder.ToString();
        System.Collections.Hashtable data = new System.Collections.Hashtable();
        data.Add("currency", "356");
        data.Add("isocurrency", "INR");
        data.Add("orderid", TransactionId);
        data.Add("buyerEmail", EmailId);
        data.Add("buyerPhone", phone);
        data.Add("buyerFirstName", Name);
        data.Add("buyerAddress", Address);
        data.Add("buyerCity", City);
        data.Add("buyerState", State);
        data.Add("buyerCountry", "India");
        data.Add("buyerPinCode", "122018");
        data.Add("amount", Amnt.ToString());
        data.Add("chmod", "");
        data.Add("checksum", checksum1);
        data.Add("privatekey", str256Key);
        data.Add("mercid", MySession.AirPayMerchantId);

        //Actual Url -- testing Url
        string strForm = PreparePOSTForm("https://payments.airpay.co.in/pay/index.php", data);
        //Insert into Table
        if (true)
        {
            Page.Controls.Add(new LiteralControl(strForm));
        }
        else
        {
            lblMsg.Text = "Sorry for the inconvinience! Our services are down right now.";
            AlertDiv.Visible = true;
        }

    }

    private string PreparePOSTForm(string url, System.Collections.Hashtable data)      // post form
    {
        //Set a name for the form
        string formID = "PostForm";
        //Build the form using the specified data to be posted.
        StringBuilder strForm = new StringBuilder();
        strForm.Append("<form id=\"" + formID + "\" name=\"" +
                       formID + "\" action=\"" + url +
                       "\" method=\"POST\">");

        foreach (System.Collections.DictionaryEntry key in data)
        {

            strForm.Append("<input type=\"hidden\" name=\"" + key.Key +
                           "\" value=\"" + key.Value + "\">");
        }


        strForm.Append("</form>");
        //Build the JavaScript which will do the Posting operation.
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language='javascript'>");
        strScript.Append("var v" + formID + " = document." +
                         formID + ";");
        strScript.Append("v" + formID + ".submit();");
        strScript.Append("</script>");
        //Return the form and the script concatenated.
        //(The order is important, Form then JavaScript)
        return strForm.ToString() + strScript.ToString();
    }



    public bool areAirPayValuesValid(string sEmail, string sPhone, string sFName, string sAddress, string sCity, string sState, string sPincode, string sAmount, string sOrderId)
    {
        //    int sEmail123 = Convert.ToInt32(sEmail);
        if (sEmail == "" && sPhone == "" && sFName == "" && sAmount == "")
        {
            return false;
        }
        else
            return true;

    }



    protected void rbtExamMode_SelectedIndexChanged(object sender, EventArgs e)
    {
        rbtexammodechanged();
    }
    protected void rbtexammodechanged()
    {
        switch (rbtExamMode.SelectedValue)
        {
            case "2":
                ddl_ExamCenter.Enabled = true;
                break;
            default:
                ddl_ExamCenter.Enabled = false;
                ddl_ExamCenter.SelectedIndex = 0;
                break;
        }

    }

    protected void btnPay_Click(object sender, EventArgs e)
    {
        if (lblAckNo.Text != "")
        {

            //*************Check Payment Done for this ackNo
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@AckNo", lblAckNo.Text);
            param[1] = new SqlParameter("@msg", SqlDbType.VarChar, 100);
            param[1].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "pro_checkpaymentstatus", param);
            if (param[1].Value.ToString() != "")
            {
                lblMsg.Text = param[1].Value.ToString();
                AlertDiv.Visible = true;
                return;
            }
            //*************
            btnRegister.Text = "Please Wait...";
            btnRegister.Enabled = false;
            InitAirPay((float)Convert.ToDouble(lblamt.Text), lblmobileno.Text, lblEmail.Text, lblName.Text, txtAddress.Text, txtcity.Text, ddlstate_comp.SelectedItem.Text, txtpincode.Text, lblAckNo.Text);
        }
    }
}