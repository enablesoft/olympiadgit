﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
namespace Olympiad
{
    public partial class DashBoard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["RegsNo"] == null)
            {
                Response.Redirect("~/default.aspx");
            }
            if (!IsPostBack)
            {
                hn_regno.Value = Session["RegsNo"].ToString();

                SqlParameter[] param = new SqlParameter[1];
                DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "PRO_FILLREGCOMBO", param);

                UtilityModule.ConditionalComboFillWithDs(ref ddlstate_comp, ds, 1, true, "--Select--");
                UtilityModule.ConditionalComboFillWithDs(ref ddlstate_schadd, ds, 1, true, "--Select--");
                UtilityModule.ConditionalComboFillWithDs(ref ddlClass, ds, 2, true, "--Select--");
            }
        }
        protected void onClickLogOut(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("~/default.aspx");
        }
        [WebMethod]
        public static string getregisteredstudata()
        {
            if (HttpContext.Current.Session["RegsNo"] == null)
            {
                throw new Exception("Session Expired");
            }
            else
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@RegNo", HttpContext.Current.Session["RegsNo"].ToString());
                DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "pro_getregisteredstudata", param);

                return JsonConvert.SerializeObject(ds);
            }
        }
        protected void btnsamletest_Click(object sender, EventArgs e)
        {
            if (hn_classid.Value == "0" || hn_testid.Value == "")
            {
                UtilityModule.showMessage(Page, "Please check class and Test Id.");
                return;
            }
            try
            {
                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter("@Testid", hn_testid.Value);
                param[1] = new SqlParameter("@Classid", hn_classid.Value);
                param[2] = new SqlParameter("@msg", SqlDbType.VarChar, 100);
                param[2].Direction = ParameterDirection.Output;
                param[3] = new SqlParameter("@RegNo", Session["RegsNo"]);

                DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "pro_getstarttestdetail", param);
                if (param[2].Value.ToString() != "")
                {
                    UtilityModule.showMessage(Page, param[2].Value.ToString());
                }
                else
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string regno = HttpUtility.UrlEncode(PasswordEncrypt_Decrypt.Encrypt(Session["RegsNo"].ToString()));
                        string testid = HttpUtility.UrlEncode(PasswordEncrypt_Decrypt.Encrypt(hn_testid.Value));
                        string classid = HttpUtility.UrlEncode(PasswordEncrypt_Decrypt.Encrypt(hn_classid.Value));
                        string t_hour = HttpUtility.UrlEncode(PasswordEncrypt_Decrypt.Encrypt(ds.Tables[0].Rows[0]["t_hour"].ToString()));
                        string t_min = HttpUtility.UrlEncode(PasswordEncrypt_Decrypt.Encrypt(ds.Tables[0].Rows[0]["t_min"].ToString()));
                        string testtype = HttpUtility.UrlEncode(PasswordEncrypt_Decrypt.Encrypt(ds.Tables[0].Rows[0]["testtype"].ToString()));
                        Session["starttime"] = ds.Tables[0].Rows[0]["Starttime"].ToString();
                        Response.Redirect(string.Format("~/Sampletest.aspx?T={0}&C={1}&Th={2}&Tm={3}&TT={4}&R={5}", testid, classid, t_hour, t_min, testtype, regno));
                    }
                }

            }
            catch (Exception ex)
            {
                UtilityModule.showMessage(Page, "Error Occured.Please try again.");
            }
        }
        [WebMethod]
        public static string getdataforupdate()
        {
            if (HttpContext.Current.Session["RegsNo"] == null)
            {
                throw new Exception("Session Expired");
            }
            else
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@RegNo", HttpContext.Current.Session["RegsNo"].ToString());
                param[1] = new SqlParameter("@mode", "Sel");
                DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "pro_updateregistrationdetail", param);
                return JsonConvert.SerializeObject(ds.Tables[0]);
            }
        }
        [WebMethod]
        public static string updatedetails(string regno, int classid, string gender, string name, string fname, string mname, string dob, string address, string city, string pincode, int stateid, string schoolname, string sch_address, string sch_city, string sch_pincode, int sch_stateid, string affiliated, float regamt)
        {
            if (HttpContext.Current.Session["RegsNo"] == null)
            {
                throw new Exception("Session Expired");
            }
            else
            {
                SqlParameter[] param = new SqlParameter[20];
                param[0] = new SqlParameter("@regno", regno);
                param[1] = new SqlParameter("@classid", classid);
                param[2] = new SqlParameter("@gender", gender);
                param[3] = new SqlParameter("@name", name);
                param[4] = new SqlParameter("@fname", fname);
                param[5] = new SqlParameter("@Mname", mname);
                param[6] = new SqlParameter("@dob", dob);
                param[7] = new SqlParameter("@address", address);
                param[8] = new SqlParameter("@city", city);
                param[9] = new SqlParameter("@pincode", pincode);
                param[10] = new SqlParameter("@stateid", stateid);
                param[11] = new SqlParameter("@schoolname", schoolname);
                param[12] = new SqlParameter("@sch_address", sch_address);
                param[13] = new SqlParameter("@sch_city", sch_city);
                param[14] = new SqlParameter("@sch_pincode", sch_pincode);
                param[15] = new SqlParameter("@sch_stateid", sch_stateid);
                param[16] = new SqlParameter("@affiliated", affiliated);
                param[17] = new SqlParameter("@mode", "update");
                param[18] = new SqlParameter("@msg", SqlDbType.VarChar, 100);
                param[18].Direction = ParameterDirection.Output;
                param[19] = new SqlParameter("@Regamt", regamt);
                SqlHelper.ExecuteNonQuery(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "pro_updateregistrationdetail", param);

                return param[18].Value.ToString();
            }
        }
    }

}