﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Threading;
using System.Text;
using System.Web.Services;
using System.Security.Cryptography;

/// <summary>
/// Summary description for UtilityModule
/// </summary>
/// 

public class UtilityModule
{

    public static void ConditionalComboFill(ref DropDownList comboname, string strsql, bool isSelectText, string selecttext)
    {

        try
        {
            comboname.DataSource = null;
            DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                comboname.DataSource = ds.Tables[0];
                comboname.DataTextField = ds.Tables[0].Columns[1].ToString();
                comboname.DataValueField = ds.Tables[0].Columns[0].ToString();
                comboname.DataBind();
            }
            else { comboname.Items.Clear(); }
            if (isSelectText && selecttext != "")
            {
                comboname.Items.Insert(0, new ListItem(selecttext, "-1"));
            }
        }
        catch (Exception Ex)
        {
            //Logs.WriteErrorLog(Ex);
        }

    }
    public static void ConditionalComboFillWithDs(ref DropDownList comboname, DataSet ds, int i, bool isSelectText, string selecttext)
    {

        try
        {
            comboname.DataSource = null;
            //ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, strsql);
            if (ds.Tables[i].Rows.Count > 0)
            {
                comboname.DataSource = ds.Tables[i];
                comboname.DataTextField = ds.Tables[i].Columns[1].ToString();
                comboname.DataValueField = ds.Tables[i].Columns[0].ToString();
                comboname.DataBind();
            }
            else { comboname.Items.Clear(); }
            if (isSelectText && selecttext != "")
            {
                comboname.Items.Insert(0, new ListItem(selecttext, "-1"));
            }
        }
        catch (Exception Ex)
        {
            //Logs.WriteErrorLog(Ex);
        }

    }

    public static void showMessage(Page page, string Message)
    {
        ScriptManager.RegisterStartupScript(page, page.GetType(), "ScriptRegistration", "alert('" + Message + "');", true);
    }
    public static void LogError(Exception ex)
    {
        string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;
        message += string.Format("Message: {0}", ex.Message);
        message += Environment.NewLine;
        message += string.Format("StackTrace: {0}", ex.StackTrace);
        message += Environment.NewLine;
        string path = HttpContext.Current.Server.MapPath("~/ErrorLog.txt");
        using (StreamWriter writer = new StreamWriter(path, true))
        {
            writer.WriteLine(message);
            writer.Close();
        }
    }
}