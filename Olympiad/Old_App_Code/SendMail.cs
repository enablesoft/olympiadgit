﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Configuration;
using System.Net.Mail;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using System.Net;

public class SendMail
{
    public static bool sendmail(string pTo = "", string pSubject = "", string pBody = "", string pFromSMTP = "", string pFromDispName = "", string pAttachments = "", string pCC = "", string pAttchmntName = "", string pBCC = "", Attachment Attch = null)
    {
        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, "select * From [SCH_MAILSETTING]");
            if (ds.Tables[0].Rows.Count > 0)
            {
                SmtpSection smtpSection = (SmtpSection)ConfigurationManager.GetSection("mailsettings/" + pFromSMTP);
                MailMessage objMail = new MailMessage();
                if (pTo == "")
                {
                    pTo = ds.Tables[0].Rows[0]["Receiveaddress"].ToString();
                }

                objMail.To.Add(pTo);
                if (pCC != "") { objMail.CC.Add(pCC); }
                if (pBCC != "") { objMail.Bcc.Add(pBCC); }
                objMail.Subject = pSubject;
                objMail.Body = pBody;
                objMail.IsBodyHtml = true;
                //foreach (string pAttachment in pAttachments.Split('|'))
                //{
                //    if ((pAttachment != ""))
                //    {
                //        string fileName = pAttchmntName == "" ? Path.GetFileName(pAttachment) : pAttchmntName;
                //        objMail.Attachments.Add(new Attachment(new MemoryStream(File.ReadAllBytes(pAttachment)), fileName));
                //    }
                //}
                //if (Attch != null)
                //{
                //    objMail.Attachments.Add(Attch);
                //}
                //else
                //{
                //    string fileName = pAttchmntName == "" ? Path.GetFileName(pAttachments) : pAttchmntName;
                //    objMail.Attachments.Add(new Attachment(new MemoryStream(File.ReadAllBytes(pAttachments)), fileName));
                //}
                // objMail.From = new MailAddress(smtpSection.From, pFromDispName);
                //SmtpClient smClient = new SmtpClient(smtpSection.Network.Host, smtpSection.Network.Port);
                //smClient.EnableSsl = smtpSection.Network.EnableSsl;
                //smClient.UseDefaultCredentials = smtpSection.Network.DefaultCredentials;
                //smClient.Credentials = new System.Net.NetworkCredential(smtpSection.Network.UserName, smtpSection.Network.Password);              
                objMail.From = new MailAddress(ds.Tables[0].Rows[0]["smtpuser"].ToString(), pFromDispName);
                SmtpClient smClient = new SmtpClient()
                {
                    Host = ds.Tables[0].Rows[0]["smtpserver"].ToString(),
                    UseDefaultCredentials = false,
                    Port = Convert.ToInt32(ds.Tables[0].Rows[0]["smtpPort"]),
                    EnableSsl = Convert.ToBoolean(ds.Tables[0].Rows[0]["Enablessl"]),
                    Credentials = new System.Net.NetworkCredential(ds.Tables[0].Rows[0]["smtpuser"].ToString(), ds.Tables[0].Rows[0]["smtppass"].ToString()),
                    DeliveryMethod=SmtpDeliveryMethod.Network
                };
                //string xyz = "Sending mail async";
                //smClient.SendAsync(objMail, xyz);
                smClient.Send(objMail);
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception)
        {
            return false;
        }
    }
    public static bool sendmail(string pTo = "", string pSubject = "", string pBody = "", string pFromSMTP = "", string pFromDispName = "")
    {
        try
        {
            // Specify the from and to email address
            MailMessage mailMessage = new MailMessage("testsoftggn@gmail.com", "testsoftggn@gmail.com");
            // Specify the email body
            mailMessage.Body = "<p>some text here</p>"; ;
            // Specify the email Subject
            mailMessage.Subject = "Exception";

            // Specify the SMTP server name and post number
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
            // Specify your gmail address and password
            smtpClient.Credentials = new System.Net.NetworkCredential()
            {
                UserName = "testsoftggn@gmail.com",
                Password = "8279992010"
            };
            // Gmail works on SSL, so set this property to true
            smtpClient.EnableSsl = true;
            // Finall send the email message using Send() method
            smtpClient.Send(mailMessage);
            return true;

        }
        catch (Exception ex)
        {
            return false;
        }
    }
    public static Boolean FireSms(string varMobNo, string varMessage, string varUnicode = "0", string ClassGrupMsg = null)
    {
        DataTable dsGetSMSDetail = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, "select ID,SMSUrl,SMSUserId,SMSFrom,SMSPWD,isnull(MaxSmsCountInOneShot,100)MaxSmsCountInOneShot,isnull(SmsToActiveAppUsers,0)SmsToActiveAppUsers from Sch_GatewayMaster where selected = 1").Tables[0];

        if (dsGetSMSDetail.Rows.Count != 1 || dsGetSMSDetail.Rows[0].IsNull("SMSUrl") || varMobNo.Trim() == "")
        {
            return false;
        }
        int varGateWay = Convert.ToInt16(dsGetSMSDetail.Rows[0]["ID"].ToString());
        string varSMSUrl, varSMSUserId, varSMSFrom, varSMSPWD;
        //if (MySession.Current.SMSFooter != "") 
        varMessage = varMessage + Environment.NewLine;// +MySession.Current.SMSFooter;
        varMessage = HttpUtility.UrlEncode(varMessage);
        varSMSUrl = dsGetSMSDetail.Rows[0]["SMSUrl"].ToString();
        varSMSUserId = dsGetSMSDetail.Rows[0]["SMSUserId"].ToString();
        if (!string.IsNullOrEmpty(ClassGrupMsg))
        {
            varSMSFrom = ClassGrupMsg;
        }
        else
        {
            varSMSFrom = dsGetSMSDetail.Rows[0]["SMSFrom"].ToString();
        }
        //varSMSFrom = dsGetSMSDetail.Rows[0]["SMSFrom"].ToString();
        varSMSPWD = dsGetSMSDetail.Rows[0]["SMSPWD"].ToString();
        varSMSUrl = varSMSUrl.Replace("varSMSUserId", varSMSUserId);
        varSMSUrl = varSMSUrl.Replace("varSMSFrom", varSMSFrom);
        varSMSUrl = varSMSUrl.Replace("varSMSPWD", varSMSPWD);
        varSMSUrl = varSMSUrl.Replace("varMobNo", varMobNo);

        varSMSUrl = varSMSUrl.Replace("varSMSText", varMessage);
        varSMSUrl = varSMSUrl.Replace("varunicode", varUnicode);
        varSMSUrl = varSMSUrl.Replace("varUnicode", varUnicode);
        varMessage = varSMSUrl;
        WebClient webClient = new WebClient();
        webClient.Credentials = CredentialCache.DefaultCredentials;
        try
        {
            string gatewayResponse = webClient.DownloadString(varMessage).ToLower().TrimStart(Environment.NewLine.ToCharArray());
            if (varGateWay == 0)
                return gatewayResponse.ToLower().Contains("sent") ? true : false;//sent,success,123456,2007,+918459002500
            else if (varGateWay == 1)
                return gatewayResponse.ToLower().Contains("success") ? true : false;//SUCCESS-919712xxxxxx-a8e701a5ebc448b8a4ba44add2af4887(i.e MessageId)
            else
                return false;//can not determine if sms was sent successfully because this gateway is not implemented yet.

        }
        catch (Exception ex)
        {
            return false;
        }


    }

}
