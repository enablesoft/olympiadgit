﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MySession
/// </summary>
public class MySession
{
    private static System.Web.SessionState.HttpSessionState Session
    { get { return HttpContext.Current.Session; } }

    public static string UserId
    {
        get
        {
            if (Session["oUserID"] == null) { Session["oUserID"] = ""; }
            return (string)Session["oUserID"];
        }
        set { Session["oUserID"] = value; }
    }

    public static string Token
    {
        get
        {
            if (Session["token"] == null) { Session["token"] = ""; }
            return (string)Session["token"];
        }
        set { Session["token"] = value; }
    }
    public static string OrgId
    {
        get
        {
            if (Session["orgID"] == null) { Session["orgID"] = ""; }
            return (string)Session["orgID"];
        }
        set { Session["orgID"] = value; }
    }
    public static string SchoolId
    {
        get
        {
            if (Session["schoolID"] == null) { Session["schoolID"] = ""; }
            return (string)Session["schoolID"];
        }
        set { Session["schoolID"] = value; }
    }
     public static string StuEmpId
    {
        get
        {
            if (Session["studentEmpID"] == null) { Session["studentEmpID"] = ""; }
            return (string)Session["studentEmpID"];
        }
        set { Session["studentEmpID"] = value; }
    }
     
     public static string SessionId
     {
         get
         {
             if (Session["ssessionid"] == null) { Session["ssessionid"] = ""; }
             return (string)Session["ssessionid"];
         }
         set { Session["ssessionid"] = value; }
     }
     public static string LogoUrl
     {
         get
         {
             if (Session["LogoImgPath"] == null) { Session["LogoImgPath"] = ""; }
             return (string)Session["LogoImgPath"];
         }
         set { Session["LogoImgPath"] = value; }
     }

     public static string WebsiteUrl
     {
         get
         {
             if (Session["WebsiteUrl"] == null) { Session["WebsiteUrl"] = ""; }
             return (string)Session["WebsiteUrl"];
         }
         set { Session["WebsiteUrl"] = value; }
     }

     public static string PaymentGateway
     {
         get
         {
             if (Session["PaymentGateway"] == null) { Session["PaymentGateway"] = ""; }
             return (string)Session["PaymentGateway"];
         }
         set { Session["PaymentGateway"] = value; }
     }

      public static bool LetUserEditAmount
     {
         get
         {
             if (Session["editAmount"] == null) { Session["editAmount"] = "false"; }

             if (Session["editAmount"].ToString().ToLower() == "true")
                 return true;
             else
                 return false;
         }
         set { Session["editAmount"] = value; }
     }

      public static string ActualAmount
      {
          get
          {
              if (Session["ActualAmount"] == null) { Session["ActualAmount"] = ""; }
              return (string)Session["ActualAmount"];
          }
          set { Session["ActualAmount"] = value; }
      }

      public static string TechProcessKey
      {
          get
          {
              if (Session["TechProcessKey"] == null) { Session["TechProcessKey"] = ""; }
              return (string)Session["TechProcessKey"];
          }
          set { Session["TechProcessKey"] = value; }
      }

      public static string TechProcessLv
      {
          get
          {
              if (Session["TechProcessLv"] == null) { Session["TechProcessLv"] = ""; }
              return (string)Session["TechProcessLv"];
          }
          set { Session["TechProcessLv"] = value; }
      }

      public static string TechProcessMerchantCode
      {
          get
          {
              if (Session["TechProcessMerchantCode"] == null) { Session["TechProcessMerchantCode"] = ""; }
              return (string)Session["TechProcessMerchantCode"];
          }
          set { Session["TechProcessMerchantCode"] = value; }
      }

      public static string StName
      {
          get
          {
              if (Session["StName"] == null) { Session["StName"] = ""; }
              return (string)Session["StName"];
          }
          set { Session["StName"] = value; }
      }

      public static string TransactionId
      {
          get
          {
              if (Session["TransactionId"] == null) { Session["TransactionId"] = ""; }
              return (string)Session["TransactionId"];
          }
          set { Session["TransactionId"] = value; }
      }
      public static string TillMonthId
      {
          get
          {
              if (Session["TillMonthId"] == null) { Session["TillMonthId"] = ""; }
              return (string)Session["TillMonthId"];
          }
          set { Session["TillMonthId"] = value; }
      }
      public static string CCAvenueWorkingKey
      {
          get
          {
              if (Session["CCAvenueWorkingKey"] == null) { Session["CCAvenueWorkingKey"] = ""; }
              return (string)Session["CCAvenueWorkingKey"];
          }
          set { Session["CCAvenueWorkingKey"] = value; }
      }


      public static string AirPayUserName
      {
          get
          {
              if (Session["AirPayUserName"] == null) { Session["AirPayUserName"] = ""; }
              return (string)Session["AirPayUserName"];
          }
          set { Session["AirPayUserName"] = value; }
      }


      public static string AirPayPassWord
      {
          get
          {
              if (Session["AirPayPassWord"] == null) { Session["AirPayPassWord"] = ""; }
              return (string)Session["AirPayPassWord"];
          }
          set { Session["AirPayPassWord"] = value; }
      }



      public static string AirPaySecretKey
      {
          get
          {
              if (Session["AirPaySecretKey"] == null) { Session["AirPaySecretKey"] = ""; }
              return (string)Session["AirPaySecretKey"];
          }
          set { Session["AirPaySecretKey"] = value; }
      }

      public static string AirPayMerchantId
      {
          get
          {
              if (Session["AirPayMerchantId"] == null) { Session["AirPayMerchantId"] = ""; }
              return (string)Session["AirPayMerchantId"];
          }
          set { Session["AirPayMerchantId"] = value; }
      }

      public static string LetPayOldFeeMonths
      {
          get
          {
              if (Session["LetPayOldFeeMonths"] == null) { Session["LetPayOldFeeMonths"] = ""; }
              return (string)Session["LetPayOldFeeMonths"];
          }
          set { Session["LetPayOldFeeMonths"] = value; }
      }
    
    
   
}