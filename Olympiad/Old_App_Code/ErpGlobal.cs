﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for ErpGlobal
/// </summary>

public  class ErpGlobal
{
    static string _dbConnectionString = string.Empty;
    static string _serverConnectionString = string.Empty;
    static string _dbConnectionStringApp = string.Empty;
    /// <summary>
    /// Mithun Start
    /// </summary>
    static string _withCampusColors = string.Empty;
    static string _CommonDB = string.Empty;
    static string _SMSFooter = string.Empty;
    static string _TrialPeriod = string.Empty;

    static string _LoginAllowedFromTime = "";
    static string _LoginAllowedTillTime = "";

    static DataTable _BookData;
    static DataTable _MemData;
    /// <summary>
    /// End
    /// </summary>  
    static ErpGlobal()
    {
        //
        // TODO: Add constructor logic here
        //
        _dbConnectionString = ConfigurationManager.ConnectionStrings["DbConnection"].ToString();
    }

    public static string DBCONNECTIONSTRING
    {
        get
        {
            //if (HttpContext.Current.Session != null)
            //{
            //    if (MySession.Current.ServerName != "" && MySession.Current.ServerName != null)
            //    {

            //        return "data source=" + MySession.Current.ServerName + ";database=" + MySession.Current.DbName + ";uid=" + MySession.Current.UID + ";password=" + MySession.Current.PWD + ";Connection Timeout=120";
            //    }
            //    else
            //    {
            //        return _dbConnectionString;

            //    }
            //}
            //else
            //{
            //    return _dbConnectionString;
            //}
            return _dbConnectionString;
        }
    }
    public static string DBCONNECTIONSTRING_App
    {
        get { return _dbConnectionStringApp; }
    }

    public static string ServerConnectionString
    {
        get { return _serverConnectionString; }
    }


    /// <summary>
    /// Mithun Start
    /// </summary>



    public static string WithCampusColors
    {
        get { return _withCampusColors; }
        set { _withCampusColors = value; }
    }

    public static string CommonDB
    {
        get { return _CommonDB; }
        set { _CommonDB = value; }
    }

    public static string SMSFooter
    {
        get { return _SMSFooter; }
        set { _SMSFooter = value; }
    }

    public static string TrialPeriod
    {
        get { return _TrialPeriod; }
        set { _TrialPeriod = value; }
    }




    public static string LoginAllowedFromTime
    {
        get { return _LoginAllowedFromTime; }
        set { _LoginAllowedFromTime = value; }
    }
    public static string LoginAllowedTillTime
    {
        get { return _LoginAllowedTillTime; }
        set { _LoginAllowedTillTime = value; }
    }



    public static DataTable BookData
    {
        get { return _BookData; }
        set { _BookData = value; }
    }

    public static DataTable MemData
    {
        get { return _MemData; }
        set { _MemData = value; }
    }
    /// <summary>
    /// End
    /// </summary>
}
