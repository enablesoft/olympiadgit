﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Data;
using Aspose.Cells;
using System.Drawing;
using System.Web.UI;
using System.Data.SqlClient;


namespace DataExportsToExcel
{
    public class ExportToExcelAspose
    {
        public static void UpdateCell(Worksheet ws, string pCellName, string pCellValue, Style pStyle = null, bool pIsValueString = true)
        {
            if (pIsValueString && pCellValue.StartsWith("="))
            { ws.Cells[pCellName].Formula = pCellValue.Replace("=", ""); }
            else { ws.Cells[pCellName].Value = pCellValue; }
            // set style on Excel Column if pStyle is not null
            if (pStyle != null) { ws.Cells[pCellName].SetStyle(pStyle); }
        }

        #region Simple Exports Aspose
        public static string ExportfromWebmethod(DataTable pTable, string pFileNameHead, string pDestination = "TempContents/Exports/", bool pIgnoreIDs = false)
        {
            string reportMain = pTable.TableName;
            string SchoolName = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, "Select SchoolId As ID,    CONCAT(sch_Name ,' ,' ,sch_address) As sch_Name   from Sch_Schoolinfo where SchoolId=" + HttpContext.Current.Session["SchoolId"].ToString(), SqlHelper.QueryType.Data).Rows[0]["Sch_Name"].ToString();

            string filePath = HttpContext.Current.Server.MapPath(pDestination);
            if (!Directory.Exists(filePath)) { Directory.CreateDirectory(filePath); }
            string fileName = pFileNameHead + "_" + DateTime.Now.Ticks + ".xlsx";//.ToString("dd-MMM-yyyy")
            filePath = filePath + fileName;

            if (pIgnoreIDs)
            {
                List<string> idCols = new List<string> { };
                foreach (DataColumn dc in pTable.Columns)
                { if (dc.ColumnName.ToUpper().EndsWith("ID")) { idCols.Add(dc.ColumnName); } }
                foreach (string colName in idCols) { pTable.Columns.Remove(colName); }
            }

            Workbook workbook = new Workbook();
            Worksheet worksheet = workbook.Worksheets.First();
            worksheet.Cells.ImportDataTable(pTable, true, "A5");


            ////// adding Styles to sheet
            Style st1 = workbook.CreateStyle();
            StyleFlag st1Flag = new StyleFlag();
            st1.Font.Name = "Calibri";
            st1.Font.Size = 11;
            st1Flag.FontName = true;
            st1Flag.FontSize = true;
            worksheet.Cells.ApplyStyle(st1, st1Flag);
            ////// adding Column Header row style

            Style st0 = workbook.CreateStyle();
            StyleFlag st0Flag = new StyleFlag();
            st0.Font.IsBold = true;
            st0.HorizontalAlignment = TextAlignmentType.Center;
            st0Flag.FontBold = true;
            workbook.ChangePalette(Color.Aquamarine, 55);
            st0.ForegroundColor = Color.LightGray;
            st0.Pattern = BackgroundType.Solid;
            for (int i = 0; i < pTable.Columns.Count; i++)
            {
                worksheet.Cells[4, i].SetStyle(st0);
            }
            st0Flag.HorizontalAlignment = true;
            worksheet.Cells.Rows[4].ApplyStyle(st0, st0Flag);
            ////// adding Header row style
            Style st00 = workbook.CreateStyle();
            StyleFlag st00Flag = new StyleFlag();
            st00.Font.IsBold = true;
            st00.Font.Size = 14;
            st00Flag.FontSize = true;
            st00.HorizontalAlignment = TextAlignmentType.Center;
            st00Flag.FontBold = true;
            st00Flag.HorizontalAlignment = true;
            worksheet.Cells.Rows[1].ApplyStyle(st00, st00Flag);
            ////// adding Date And Type row style
            Style Sty = workbook.CreateStyle();
            StyleFlag StyFlag = new StyleFlag();
            Sty.Font.IsBold = true;
            //Sty.Font.Size = 18;
            StyFlag.FontSize = true;
            Sty.HorizontalAlignment = TextAlignmentType.Center;
            StyFlag.FontBold = true;
            StyFlag.HorizontalAlignment = true;
            worksheet.Cells.Rows[2].ApplyStyle(Sty, StyFlag);
            ////////////////////
            int toMergeColCount = pTable.Columns.Count < 8 ? 8 : pTable.Columns.Count;
            worksheet.FreezePanes("A2", 1, 0);
            worksheet.Cells.Merge(1, 0, 1, toMergeColCount);
            worksheet.FreezePanes("A3", 1, 0);
            worksheet.Cells.Merge(2, 0, 1, toMergeColCount);
            worksheet.Cells["A2"].Value = SchoolName;
            worksheet.Cells["A3"].Value = "Report Type : " + reportMain + "                " + "Date :" + DateTime.Now.ToString("dd MMMM yyyy");

            //worksheet.Cells["A3"].Value = "Date :" + DateTime.Now.ToString("dd MMMM yyyy") + "       " + "Repotr Type :" + reportMain + "";
            //worksheet.Cells["B3"].Value = DateTime.Now.ToString("dd MMMM yyyy");
            //worksheet.Cells["D3"].Value = "Report Type :";
            //worksheet.Cells["E3"].Value = reportMain;
            //worksheet.Cells.GroupRows(1, 7);
            //worksheet.Outline.SummaryRowBelow = true;

            worksheet.AutoFitColumns();

            workbook.Save(filePath, new XlsSaveOptions(SaveFormat.Xlsx));

            string tobesrached = "campuspro.in";

            int ix = filePath.IndexOf(tobesrached);
            if (ix != -1)
            {
                filePath = "https://" + filePath.Substring(ix + (tobesrached.Length) + 1);

            }
            return filePath;
        }

        public static string Export(DataTable pTable, string pFileNameHead, string pDestination = "TempContents/Exports/", bool pIgnoreIDs = false, string ClassExamName = null, string reporttitle = "")
        {
            string reportMain = reporttitle;
            string SchoolName = "";//= SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, "Select SchoolId As ID,    CONCAT(sch_Name ,' ,' ,sch_address) As sch_Name   from Sch_Schoolinfo where SchoolId=" + HttpContext.Current.Session["SchoolId"].ToString(), SqlHelper.QueryType.Data).Rows[0]["Sch_Name"].ToString();

            string filePath = HttpContext.Current.Server.MapPath(pDestination);
            if (!Directory.Exists(filePath)) { Directory.CreateDirectory(filePath); }
            string fileName = pFileNameHead + "_" + DateTime.Now.Ticks + ".xlsx";//.ToString("dd-MMM-yyyy")
            filePath = filePath + fileName;

            if (pIgnoreIDs)
            {
                List<string> idCols = new List<string> { };
                foreach (DataColumn dc in pTable.Columns)
                { if (dc.ColumnName.ToUpper().EndsWith("ID")) { idCols.Add(dc.ColumnName); } }
                foreach (string colName in idCols) { pTable.Columns.Remove(colName); }
            }

            Workbook workbook = new Workbook();
            Worksheet worksheet = workbook.Worksheets.First();
            worksheet.Cells.ImportDataTable(pTable, true, "A6");


            ////// adding Styles to sheet
            Style st1 = workbook.CreateStyle();
            StyleFlag st1Flag = new StyleFlag();
            st1.Font.Name = "Calibri";
            st1.Font.Size = 11;
            st1Flag.FontName = true;
            st1Flag.FontSize = true;
            worksheet.Cells.ApplyStyle(st1, st1Flag);
            ////// adding Column Header row style

            Style st0 = workbook.CreateStyle();
            StyleFlag st0Flag = new StyleFlag();
            st0.Font.IsBold = true;
            st0.HorizontalAlignment = TextAlignmentType.Center;
            st0Flag.FontBold = true;
            workbook.ChangePalette(Color.Aquamarine, 55);
            st0.ForegroundColor = Color.LightGray;
            st0.Pattern = BackgroundType.Solid;
            for (int i = 0; i < pTable.Columns.Count; i++)
            {
                worksheet.Cells[5, i].SetStyle(st0);
            }
            st0Flag.HorizontalAlignment = true;
            worksheet.Cells.Rows[5].ApplyStyle(st0, st0Flag);
            ////// adding Header row style
            Style st00 = workbook.CreateStyle();
            StyleFlag st00Flag = new StyleFlag();
            st00.Font.IsBold = true;
            st00.Font.Size = 14;
            st00Flag.FontSize = true;
            st00.HorizontalAlignment = TextAlignmentType.Center;
            st00Flag.FontBold = true;
            st00Flag.HorizontalAlignment = true;
            worksheet.Cells.Rows[1].ApplyStyle(st00, st00Flag);
            ////// adding Date And Type row style
            Style Sty = workbook.CreateStyle();
            StyleFlag StyFlag = new StyleFlag();
            Sty.Font.IsBold = true;
            //Sty.Font.Size = 18;
            StyFlag.FontSize = true;
            Sty.HorizontalAlignment = TextAlignmentType.Center;
            StyFlag.FontBold = true;
            StyFlag.HorizontalAlignment = true;
            worksheet.Cells.Rows[2].ApplyStyle(Sty, StyFlag);
            worksheet.Cells.Rows[3].ApplyStyle(Sty, StyFlag);
            ////////////////////
            int toMergeColCount = pTable.Columns.Count < 8 ? 8 : pTable.Columns.Count;
            worksheet.FreezePanes("A2", 1, 0);
            worksheet.Cells.Merge(1, 0, 1, toMergeColCount);
            worksheet.FreezePanes("A3", 1, 0);
            worksheet.Cells.Merge(2, 0, 1, toMergeColCount);
            worksheet.Cells["A2"].Value = SchoolName;
            worksheet.Cells["A3"].Value = "Report Type : " + reportMain + "                " + "Date :" + DateTime.Now.ToString("dd MMMM yyyy");
            if (!string.IsNullOrEmpty(ClassExamName))
            {
                worksheet.Cells.Merge(3, 0, 1, toMergeColCount);
                worksheet.Cells["A4"].Value = ClassExamName;
            }
            //worksheet.FreezePanes("A4", 1, 0);
            //worksheet.Cells.Merge(3, 0, 1, toMergeColCount);
            //worksheet.Cells["A3"].Value = "lkjhgcfxghjkljhvjkljbh";
            //worksheet.Cells["A3"].Value = "Date :" + DateTime.Now.ToString("dd MMMM yyyy") + "       " + "Repotr Type :" + reportMain + "";
            //worksheet.Cells["B3"].Value = DateTime.Now.ToString("dd MMMM yyyy");
            //worksheet.Cells["D3"].Value = "Report Type :";
            //worksheet.Cells["E3"].Value = reportMain;
            //worksheet.Cells.GroupRows(1, 7);
            //worksheet.Outline.SummaryRowBelow = true;
            HttpResponse response = HttpContext.Current.Response;
            //if (pIsAdminExport)
            //{
            //    workbook.Save(filePath, new XlsSaveOptions(SaveFormat.Xlsx));
            //}
            worksheet.AutoFitColumns();

            workbook.Save(response, Path.GetFileName(filePath), ContentDisposition.Attachment, new XlsSaveOptions(SaveFormat.Xlsx));
            response.Flush();

            return filePath;
        }
        #endregion

        private static string getValue(string p)
        {
            if (p.Trim() == "")
                return "null";
            else
                return "'" + p.Trim() + "'";
        }

        public static string ExportStudent(DataTable pTable, string pFileNameHead, string pDestination = "TempContents/Exports/", bool pIgnoreIDs = true)
        {
            string reportMain = pTable.TableName;
            string SchoolName = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, "Select SchoolId As ID,    CONCAT(sch_Name ,' ,' ,sch_address) As sch_Name   from Sch_Schoolinfo where SchoolId=" + HttpContext.Current.Session["SchoolId"].ToString(), SqlHelper.QueryType.Data).Rows[0]["Sch_Name"].ToString();

            string filePath = HttpContext.Current.Server.MapPath(pDestination);
            if (!Directory.Exists(filePath)) { Directory.CreateDirectory(filePath); }
            string fileName = pFileNameHead + "_" + DateTime.Now.Ticks + ".xlsx";//.ToString("dd-MMM-yyyy")
            filePath = filePath + fileName;

            if (pIgnoreIDs)
            {
                List<string> idCols = new List<string> { };
                foreach (DataColumn dc in pTable.Columns)
                { if (dc.ColumnName.ToUpper().EndsWith("ID")) { idCols.Add(dc.ColumnName); } }
                foreach (string colName in idCols) { pTable.Columns.Remove(colName); }
            }

            Workbook workbook = new Workbook();
            Worksheet worksheet = workbook.Worksheets.First();
            worksheet.Cells.ImportDataTable(pTable, true, "A1");
            Column column = worksheet.Cells.Columns[0];
            column.IsHidden = true;

            ////// adding Styles to sheet
            Style st1 = workbook.CreateStyle();
            StyleFlag st1Flag = new StyleFlag();
            st1.Font.Name = "Calibri";
            st1.Font.Size = 11;
            st1.Font.IsBold = true;
            st1Flag.FontName = true;
            st1Flag.FontSize = true;
            worksheet.Cells.ApplyStyle(st1, st1Flag);
            ////// adding Column Header row style

            Style st0 = workbook.CreateStyle();
            StyleFlag st0Flag = new StyleFlag();
            st0.Font.IsBold = true;
            st0.Font.Size = 11;
            st0.Font.Name = "Calibri";
            st0.HorizontalAlignment = TextAlignmentType.Center;
            st0Flag.FontBold = true;
            workbook.ChangePalette(Color.Aquamarine, 55);
            st0.ForegroundColor = Color.LightGray;
            st0.Pattern = BackgroundType.Solid;
            for (int i = 0; i < pTable.Columns.Count; i++)
            {
                worksheet.Cells[0, i].SetStyle(st0);
            }
            st0Flag.HorizontalAlignment = true;
            //worksheet.Cells.Rows[4].ApplyStyle(st0, st0Flag);
            //// adding Header row style
            Style st00 = workbook.CreateStyle();
            StyleFlag st00Flag = new StyleFlag();
            st00.Font.IsBold = true;
            st00.Font.Size = 11;
            st00Flag.FontSize = true;
            st00.HorizontalAlignment = TextAlignmentType.Center;
            st00Flag.FontBold = true;
            st00Flag.HorizontalAlignment = true;

            HttpResponse response = HttpContext.Current.Response;
            //if (pIsAdminExport)
            //{
            //    workbook.Save(filePath, new XlsSaveOptions(SaveFormat.Xlsx));
            //}
            worksheet.AutoFitColumns();
            workbook.Save(response, Path.GetFileName(filePath), ContentDisposition.Attachment, new XlsSaveOptions(SaveFormat.Xlsx));
            response.Flush();
            return filePath;
        }

        public static string ExportSubjectwiseanalysis(DataSet ds, string pFileNameHead, string pDestination = "TempContents/Exports/", bool pIgnoreIDs = true)
        {
            DataTable pTable = ds.Tables[0].Copy();
            string reportMain = pTable.TableName;
            string SchoolName = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, "Select SchoolId As ID,    CONCAT(sch_Name ,' ,' ,sch_address) As sch_Name   from Sch_Schoolinfo where SchoolId=" + HttpContext.Current.Session["SchoolId"].ToString(), SqlHelper.QueryType.Data).Rows[0]["Sch_Name"].ToString();

            string filePath = HttpContext.Current.Server.MapPath(pDestination);
            if (!Directory.Exists(filePath)) { Directory.CreateDirectory(filePath); }
            string fileName = pFileNameHead + "_" + DateTime.Now.Ticks + ".xlsx";//.ToString("dd-MMM-yyyy")
            filePath = filePath + fileName;

            if (pIgnoreIDs)
            {
                List<string> idCols = new List<string> { };
                foreach (DataColumn dc in pTable.Columns)
                { if (dc.ColumnName.ToUpper().EndsWith("ID")) { idCols.Add(dc.ColumnName); } }
                foreach (string colName in idCols) { pTable.Columns.Remove(colName); }
            }

            Workbook workbook = new Workbook();
            Worksheet worksheet = workbook.Worksheets.First();
            worksheet.Cells.ImportDataTable(pTable, true, "A1");
            worksheet.Cells.ImportDataTable(ds.Tables[1], true, 0, pTable.Columns.Count + 1, false);

            Column column = worksheet.Cells.Columns[0];
            column.IsHidden = true;

            ////// adding Styles to sheet
            Style st1 = workbook.CreateStyle();
            StyleFlag st1Flag = new StyleFlag();
            st1.Font.Name = "Calibri";
            st1.Font.Size = 11;
            st1.Font.IsBold = true;
            st1Flag.FontName = true;
            st1Flag.FontSize = true;
            worksheet.Cells.ApplyStyle(st1, st1Flag);
            ////// adding Column Header row style

            Style st0 = workbook.CreateStyle();
            StyleFlag st0Flag = new StyleFlag();
            st0.Font.IsBold = true;
            st0.Font.Size = 11;
            st0.Font.Name = "Calibri";
            st0.HorizontalAlignment = TextAlignmentType.Center;
            st0Flag.FontBold = true;
            workbook.ChangePalette(Color.Aquamarine, 55);
            st0.ForegroundColor = Color.LightGray;
            st0.Pattern = BackgroundType.Solid;
            int cellcnt = pTable.Columns.Count + ds.Tables[1].Columns.Count + 1;
            for (int i = 0; i < cellcnt; i++)
            {
                worksheet.Cells[0, i].SetStyle(st0);
            }
            st0Flag.HorizontalAlignment = true;
            //worksheet.Cells.Rows[4].ApplyStyle(st0, st0Flag);
            //// adding Header row style
            Style st00 = workbook.CreateStyle();
            StyleFlag st00Flag = new StyleFlag();
            st00.Font.IsBold = true;
            st00.Font.Size = 11;
            st00Flag.FontSize = true;
            st00.HorizontalAlignment = TextAlignmentType.Center;
            st00Flag.FontBold = true;
            st00Flag.HorizontalAlignment = true;


            HttpResponse response = HttpContext.Current.Response;
            //if (pIsAdminExport)
            //{
            //    workbook.Save(filePath, new XlsSaveOptions(SaveFormat.Xlsx));
            //}
            worksheet.AutoFitColumns();
            workbook.Save(response, Path.GetFileName(filePath), ContentDisposition.Attachment, new XlsSaveOptions(SaveFormat.Xlsx));
            response.Flush();
            return filePath;
        }



        public static Boolean ImportBooksData(string pPath, Page page, string SchoolId, string OrganizationId, string UserId)
        {

            string sql;
            string FinalAlreadyAvailbleBooks = "";
            string FinalAlreadyAvailbleBookNO = "";
            #region Parameters
            string sColumns = " BookId,BookNo,BookName,BookAuthor1,BookAuthor2,BookAuthor3,BookAuthor4,SubjectId,PubId,BookEdition,pubYear,DateOfPurchase,BookCost,NoOfCopies,NoOfPages,BookISBN,BookType,VendorId,ClassificationNo,AccessionNo,BooksOK,BooksDamage,BooksLost,BooksIssued,BooksAvailable,isActive,SchoolId,OrganizationId,TimeStamp,ByUserId,ClassId";

            string BookNo = "", BookName = "", BookAuthor1 = "", BookAuthor2 = "", BookAuthor3 = "", BookAuthor4 = "", SubjectName = "", PublisherName = "", BookEdition = "", pubYear = "", DateOfPurchase = "", BookCost = "", NoOfCopies = "", NoOfPages = "", BookISBN = "", BookType = "", VendorName = "", ClassificationNo = "", AccessionNo = "", ClassName = "";
            int SubjectId = 0, PublisherId = 0, BookTypeId = 0, VendorId = 0, ClassId = 0;

            #endregion
            StringBuilder sqlBunch = new StringBuilder("");
            //Creating a file stream containing the Excel file to be opened
            FileStream fstream = new FileStream(pPath, FileMode.Open);
            //Instantiating a Workbook object
            //Opening the Excel file through the file stream
            Workbook workbook = new Workbook(fstream);
            //Accessing the first worksheet in the Excel file
            Worksheet worksheet = workbook.Worksheets[0];

            DataTable dtBooksInExcel = new DataTable();
            //DataTable dt = worksheet.Cells.ExportDataTable(3, 0, 28, 19, new ExportTableOptions() { CheckMixedValueType = false, ExportColumnName = true, ExportAsString = true });

            dtBooksInExcel = worksheet.Cells.ExportDataTable(1, 0, worksheet.Cells.MaxDataRow, worksheet.Cells.MaxDataColumn, new ExportTableOptions() { ExportAsString = true });

            fstream.Dispose();
            string str = "Select BookName from Lib_BookMaster";
            DataTable dtBooksInDataBase = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, str).Tables[0];
            DataTable dtBookNOInDataBase = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, "Select BookNo from lib_BookMaster").Tables[0];
            string AlreadyAvailbleBooks = "", AlreadyAvailbleBookNo = "";

            StringBuilder strInsert = new StringBuilder();
            for (uint rNo = 2; rNo < 3000; rNo++)
            {
                if (worksheet.Cells["A" + rNo].StringValue.Trim() == "")
                {
                    continue;
                }

                BookNo = (worksheet.Cells["A" + rNo].StringValue);
                //if (UtilityModule.isNumeric(BookNo) == false)
                //{
                //    ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMassage", "alert('Book no should be Numeric!!!' )", true);
                //    return false;
                //}
                if (dtBooksInExcel.Select("Column1='" + BookNo + "'").CopyToDataTable().Rows.Count > 1)
                {
                    fstream.Dispose();
                    ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMassage", "alert('Book no already Exist  in Row no : " + rNo + "' )", true);
                    return false;
                }
                BookName = (worksheet.Cells["B" + rNo].StringValue);

                if (!dtBookNOInDataBase.Select("BookNo ='" + BookNo + "'").AsEnumerable().Any())
                {
                    if (!dtBooksInDataBase.Select("BookName ='" + BookName.Replace("'", "''") + "'").AsEnumerable().Any())
                    {

                        BookAuthor1 = (worksheet.Cells["C" + rNo].StringValue);
                        BookAuthor2 = (worksheet.Cells["D" + rNo].StringValue);
                        BookAuthor3 = (worksheet.Cells["E" + rNo].StringValue);
                        BookAuthor4 = (worksheet.Cells["F" + rNo].StringValue);


                        //Get SubjectId
                        SubjectName = (worksheet.Cells["G" + rNo].StringValue);
                        string strSubId = "Select subjectId from Lib_Subject  where SubjectName='" + SubjectName.Trim().Replace("'", "''") + "'";
                        DataTable dtSubId = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, strSubId).Tables[0];
                        if (dtSubId.Rows.Count > 0)
                        {
                            SubjectId = int.Parse(dtSubId.Rows[0][0].ToString());
                        }
                        else
                        {
                            //ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMassage", "alert('Please Select  Subject Name  Line no :- " + rNo + "' )", true);
                            //UtilityModule.showMessage(page, "Please Select  Subject Name  Line no :- " + rNo + "");
                            //return false;
                        }
                        //Get PublisherId
                        PublisherName = (worksheet.Cells["H" + rNo].StringValue);
                        string strPubId = "Select PubId from Lib_PublisherMaster Where PubName='" + PublisherName.Trim().Replace("'", "''") + "'";
                        DataTable dtPubId = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, strPubId).Tables[0];
                        if (dtPubId.Rows.Count > 0)
                        {
                            PublisherId = int.Parse(dtPubId.Rows[0][0].ToString());
                        }
                        else
                        {
                            // ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alertMassage", "alert('Please Select  Subject Name  Line no :- " + rNo + "' )", true);
                            //UtilityModule.showMessage(page, "Please Select  Publisher Name  Line no :- " + rNo + "");
                            //return false;
                        }

                        BookEdition = (worksheet.Cells["I" + rNo].StringValue);
                        pubYear = (worksheet.Cells["J" + rNo].StringValue);
                        DateOfPurchase = (worksheet.Cells["K" + rNo].StringValue);
                        BookCost = (worksheet.Cells["L" + rNo].StringValue);
                        NoOfCopies = (worksheet.Cells["M" + rNo].StringValue);
                        NoOfPages = (worksheet.Cells["N" + rNo].StringValue);
                        BookISBN = (worksheet.Cells["O" + rNo].StringValue);
                        BookType = (worksheet.Cells["P" + rNo].StringValue);
                        string strBookId = "Select * from Sch_Parameter where ParamType='BookType' and ParamName='" + BookType.Trim() + "'";
                        DataTable dtBookId = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, strBookId).Tables[0];
                        if (dtBookId.Rows.Count > 0)
                        {
                            BookTypeId = int.Parse(dtBookId.Rows[0][0].ToString());
                        }
                        else
                        {
                            //UtilityModule.showMessage(page, "Please Select  Book Type  Line no :- " + rNo + "");
                            //return false;
                        }
                        //Get VenderId
                        VendorName = (worksheet.Cells["Q" + rNo].StringValue);
                        string strVendorId = "Select VendorId from Lib_Vendormaster where Name='" + VendorName.Trim().Replace("'", "''") + "'";
                        DataTable dtVendorId = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, strVendorId).Tables[0];
                        if (dtVendorId.Rows.Count > 0)
                        {
                            VendorId = int.Parse(dtVendorId.Rows[0][0].ToString());
                        }
                        else
                        {
                            // UtilityModule.showMessage(page, "Please Select  Vendor Name  Line no :- " + rNo + "");
                            // return false;
                        }


                        ClassificationNo = (worksheet.Cells["R" + rNo].StringValue);
                        AccessionNo = (worksheet.Cells["S" + rNo].StringValue);
                        ClassName = (worksheet.Cells["T" + rNo].StringValue);

                        //Get ClassId
                        string strClassId = "select Id from Sch_class where ClassName='" + ClassName.Replace("'", "''") + "'";
                        DataTable dtClassId = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, strClassId).Tables[0];
                        if (dtClassId.Rows.Count > 0)
                        {
                            ClassId = int.Parse(dtClassId.Rows[0][0].ToString());
                        }
                        else
                        {
                            //UtilityModule.showMessage(page, "Please Select  Class Name  Line no :- " + rNo + "");
                            //return false;
                        }
                        string strValues = "(Select ISNULL( max(BookId),0)+1  from Lib_BookMaster),'" + BookNo + "',N'" + BookName.Replace("'", "''") + "',N'" + BookAuthor1.Replace("'", "''") + "',N'" + BookAuthor2.Replace("'", "''") + "',N'" + BookAuthor3.Replace("'", "''") + "',N'" + BookAuthor4.Replace("'", "''") + "','" + SubjectId + "','" + PublisherId + "','" + BookEdition.Replace("'", "''") + "','" + pubYear + "','" + DateOfPurchase + "','" + BookCost + "', '" + NoOfCopies + "','" + NoOfPages + "','" + BookISBN + "','" + BookTypeId + "','" + VendorId + "','" + ClassificationNo + "','" + AccessionNo + "','1','0','0','0','1','Y'," + SchoolId + ", " + OrganizationId + ", GetDate(), " + UserId + ", " + ClassId + "";
                        strInsert.Append("insert into Lib_BookMaster(" + sColumns + ") values( " + strValues + ")" + Environment.NewLine);

                    }
                    else
                    {
                        AlreadyAvailbleBooks += AlreadyAvailbleBooks.Trim().EndsWith(",") ? BookName : "," + BookName;
                        FinalAlreadyAvailbleBooks = AlreadyAvailbleBooks.TrimStart(',');

                    }
                }
                else
                {
                    AlreadyAvailbleBookNo += AlreadyAvailbleBookNo.Trim().EndsWith(",") ? BookNo : "," + BookNo;
                    FinalAlreadyAvailbleBookNO = AlreadyAvailbleBookNo.TrimStart(',');
                }
            }
            if (FinalAlreadyAvailbleBookNO.Trim() != "")
            {
                ScriptManager.RegisterStartupScript(page, page.GetType(), "ScriptRegistration1", "alert('Book No:- " + FinalAlreadyAvailbleBookNO + " allready Exist!!!');", true);
            }
            if (strInsert.ToString().Trim() == "")
            {
                UtilityModule.showMessage(page, "No Books are Imported!!!");
                return false;
            }
            if (strInsert.Length != 0)
            {
                SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, strInsert.ToString(), SqlHelper.QueryType.NoData);
                if (FinalAlreadyAvailbleBooks.Trim() == "")
                {
                    UtilityModule.showMessage(page, "Update Successfull!!!");
                }
                else if (FinalAlreadyAvailbleBooks.Trim() != "")
                {
                    UtilityModule.showMessage(page, "Books  Except:- " + FinalAlreadyAvailbleBooks + "");
                }


            }
            fstream.Close();
            return true;
        }

        public static DataTable ImportStuImgExcel(string pPath, Page page)
        {
            string sql;
            StringBuilder sqlBunch = new StringBuilder("");
            //Creating a file stream containing the Excel file to be opened
            FileStream fstream = new FileStream(pPath, FileMode.Open);
            //Instantiating a Workbook object
            //Opening the Excel file through the file stream
            Workbook workbook = new Workbook(fstream);
            fstream.Close();
            //Accessing the first worksheet in the Excel file
            Worksheet worksheet = workbook.Worksheets[0];
            DataTable dt = new DataTable();
            dt = worksheet.Cells.ExportDataTable(1, 0, worksheet.Cells.MaxDataRow, 2);
            dt.Columns["Column1"].ColumnName = "AdmNo";
            dt.Columns["Column2"].ColumnName = "PicName";

            return dt;
        }
        public static DataTable ReadDataUpdateTransport(string pPath, Page page)
        {

            StringBuilder sqlBunch = new StringBuilder("");
            //Creating a file stream containing the Excel file to be opened
            FileStream fstream = new FileStream(pPath, FileMode.Open);
            //Instantiating a Workbook object
            //Opening the Excel file through the file stream
            Workbook workbook = new Workbook(fstream);
            fstream.Close();
            //Accessing the first worksheet in the Excel file
            Worksheet worksheet = workbook.Worksheets[0];
            DataTable dt = new DataTable();
            dt = worksheet.Cells.ExportDataTableAsString(1, 0, worksheet.Cells.MaxDataRow, 7);
            dt.Columns["Column1"].ColumnName = "AdmNo";
            dt.Columns["Column2"].ColumnName = "Route";
            dt.Columns["Column3"].ColumnName = "Area";
            dt.Columns["Column4"].ColumnName = "Stop";
            dt.Columns["Column5"].ColumnName = "BusNo";
            dt.Columns["Column6"].ColumnName = "Month";
            dt.Columns["Column7"].ColumnName = "Charges";
            return dt;
        }
    }

}

