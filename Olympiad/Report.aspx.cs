﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json;

namespace Olympiad
{
    public partial class Report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["adminuserid"] == null)
            {
                Response.Redirect("~/Rpsoly.aspx");
            }
        }
        [WebMethod]
        public static string getregdetails()
        {
            SqlParameter[] param = new SqlParameter[1];
            DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "Pro_getregistrations", param);
            return JsonConvert.SerializeObject(ds);

        }
    }

}