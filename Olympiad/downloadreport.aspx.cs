﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Aspose.Cells;
using DataExportsToExcel;
namespace Olympiad
{
    public partial class downloadreport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["adminuserid"] == null)
            {
                Response.Redirect("~/Rpsoly.aspx");
            }
            if (!IsPostBack)
            {
                string str = @"select Classid,class From tblclass
                             select id,CenterName from tblexamcentre";
                DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, str);
                UtilityModule.ConditionalComboFillWithDs(ref ddlClass, ds, 0, true, "--All--");
                UtilityModule.ConditionalComboFillWithDs(ref ddl_ExamCenter, ds, 1, true, "--All--");
                txtfromdate.Text = System.DateTime.Now.ToString("dd-MMM-yyyy");
                txttodate.Text = System.DateTime.Now.ToString("dd-MMM-yyyy");
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            try
            {
                if (System.IO.File.Exists(Server.MapPath("~/bin/Aspose.Cells.lic")))
                {
                    Aspose.Cells.License license = new Aspose.Cells.License();
                    license.SetLicense("Aspose.Cells.lic");
                }
                string where = "";
                if (ddl_ExamCenter.SelectedIndex > 0)
                {
                    where = where + " and Centreid=" + ddl_ExamCenter.SelectedValue + "";
                }
                if (ddlClass.SelectedIndex > 0)
                {
                    where = where + " and classid=" + ddlClass.SelectedValue + "";
                }
                where = where + " and cast(dateadded as date)>='" + txtfromdate.Text + "' and  cast(dateadded as date)<='" + txttodate.Text + "'";

                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter("@exammode", rbtExamMode.SelectedItem.Text);
                param[1] = new SqlParameter("@reporttype", ddreporttype.SelectedValue);
                param[2] = new SqlParameter("@gender", rbtGender.SelectedItem.Text);
                param[3] = new SqlParameter("@Where", where);

                DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "pro_downloadregistrationdata", param);
                string reporttitle = "From : " + txtfromdate.Text + ",To : " + txttodate.Text + ",Mode of Exam:" + rbtExamMode.SelectedItem.Text + ",Gender:" + rbtGender.SelectedItem.Text + ",ReportType:" + ddreporttype.SelectedItem.Text + ",class:" + ddlClass.SelectedItem.Text + "";
                ExportToExcelAspose.Export(ds.Tables[0], "Registration Details", reporttitle: reporttitle);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}