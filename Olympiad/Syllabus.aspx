﻿<%@ Page Title="RPS Olympiad 2020: Syllabus" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="Syllabus" CodeBehind="Syllabus.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="Div_syllabus">
            <h2 class="text-center">
                Syllabus
            </h2>
            <div class="col-sm-12 col-xs-12">
                <div class="panel panel-primary ">
                    <div class="panel-heading" data-toggle="collapse" data-target="#Fourth">
                        4<sup>th</sup> Class
                    </div>
                    <div class="panel-body collapse" id="Fourth">
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>English</b></p>
                            <ul>
                                <li>Nouns </li>
                                <li>Pronouns </li>
                                <li>Adjectives </li>
                                <li>Adverbs </li>
                                <li>Prepositions </li>
                                <li>Conjunctions </li>
                                <li>Articles </li>
                                <li>Verbs </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Mathematics</b></p>
                            <ul>
                                <li>Numbers </li>
                                <li>Addition and subtraction </li>
                                <li>Multiplication and Division </li>
                                <li>Time and Calendar</li><li>Measurements </li>
                                <li>Money </li>
                                <li>Basic Geometrical Concepts </li>
                                <li>Fractions </li>
                                <li>Patterns </li>
                                <li>Perimeter and Area </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>G.K</b></p>
                            <ul>
                                <li>Current Affairs </li>
                                <li>Awards and achievements of India in 2018 and 2019 </li>
                                <li>Haryana cabinet ministers </li>
                                <li>First male and Female in India </li>
                                <li>Governor’s of Haryana </li>
                                <li>President and Prime Ministers of India </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>E.V.S:- (Science & Social Science)</b></p>
                            <ul>
                                <li>Animals and Plants Kingdom </li>
                                <li>Work, Force & Energy </li>
                                <li>Food </li>
                                <li>Water and Air </li>
                                <li>Organ system (Skeletal System, Digestive, Respiratory, Nervous) </li>
                                <li>Solar System</li>
                                <li>India (Soil, Mineral resources climate) (Physical division, Rivers, Location, Size)
                                </li>
                                <li>Social & religious reform movements in India during 19th Century </li>
                                <li>Freedom struggle</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12">
                <div class="panel panel-primary ">
                    <div class="panel-heading" data-toggle="collapse" data-target="#Fifth">
                        5<sup>th</sup> Class
                    </div>
                    <div class="panel-body collapse" id="Fifth">
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>English</b></p>
                            <ul>
                                <li>Nouns </li>
                                <li>Pronouns </li>
                                <li>Adjectives </li>
                                <li>Adverbs </li>
                                <li>Prepositions </li>
                                <li>Conjunctions </li>
                                <li>Articles </li>
                                <li>Interjections </li>
                                <li>Verbs </li>
                                <li>Tense </li>
                                <li>Vocabulary </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Mathematics</b></p>
                            <ul>
                                <li>Numbers </li>
                                <li>Operation on large numbers </li>
                                <li>Fraction</li><li>Decimals </li>
                                <li>Perimeter/Area </li>
                                <li>Measurement </li>
                                <li>Simplification </li>
                                <li>Multiples and Factors </li>
                                <li>Basic Geometry Concept </li>
                                <li>Pattern and symmetry </li>
                                <li>Data Handling </li>
                                <li>Roman numbers </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>G.K</b></p>
                            <ul>
                                <li>Current Affairs </li>
                                <li>Awards and achievements of India in 2018 and 2019 </li>
                                <li>Haryana cabinet ministers </li>
                                <li>First male and Female in India </li>
                                <li>Governor’s of Haryana </li>
                                <li>President and Prime Ministers of India </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>E.V.S:- (Science & Social Science)</b></p>
                            <ul>
                                <li>Animals and Plants Kingdom </li>
                                <li>Work, Force & Energy </li>
                                <li>Food </li>
                                <li>Water and Air </li>
                                <li>Organ system (Skeletal System, Digestive, Respiratory, Nervous) </li>
                                <li>Solar System</li>
                                <li>India (Soil, Mineral resources climate) (Physical division, Rivers, Location, Size)
                                </li>
                                <li>Social & religious reform movements in India during 19th Century </li>
                                <li>Freedom struggle</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12">
                <div class="panel panel-primary ">
                    <div class="panel-heading" data-toggle="collapse" data-target="#sixth">
                        6<sup>th</sup> Class
                    </div>
                    <div class="panel-body collapse" id="sixth">
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Mathematics </b>
                            </p>
                            <ul>
                                <li>Whole Number </li>
                                <li>Integers </li>
                                <li>Playing with numbers </li>
                                <li>Ratio and proportion </li>
                                <li>Symmetry </li>
                                <li>Mensuration </li>
                                <li>Fraction </li>
                                <li>Understanding elementry shapes </li>
                                <li>Data Handling </li>
                                <li>Basic Geometrical ideas. </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Aptitude </b>
                            </p>
                            <ul>
                                <li>Verbal and Non-Verbal Series </li>
                                <li>Verbal and Non-Verbal Analogy </li>
                                <li>Classification </li>
                                <li>Coding-Decoding </li>
                                <li>Blood-Relation </li>
                                <li>Direction-Sense Test </li>
                                <li>Clock and Calendar </li>
                                <li>VENN Diagram </li>
                                <li>Mathematical operation </li>
                                <li>Puzzle Test </li>
                                <li>Missing Character </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Physics</b>
                            </p>
                            <ul>
                                <li>Fun with Magnet </li>
                                <li>Light, Shadow and Reflection </li>
                                <li>Motion and Measurment of Distances </li>
                                <li>Electricity and Circuits </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Chemistry</b>
                            </p>
                            <ul>
                                <li>Fibre to Fabric </li>
                                <li>Separation of Substances </li>
                                <li>Changes Around Us </li>
                                <li>Air Around </li>
                                <li>Sorting Materials and Groups </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Biology</b>
                            </p>
                            <ul>
                                <li>Body Movements </li>
                                <li>Components of Food </li>
                                <li>Getting to know plants </li>
                                <li>The living organisms and their surroundings. </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>English</b>
                            </p>
                            <ul>
                                <li>Noun </li>
                                <li>Pronoun </li>
                                <li>Adjective </li>
                                <li>Tense </li>
                                <li>Preposition </li>
                                <li>Narration</li>
                                <li>Verb.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12">
                <div class="panel panel-primary ">
                    <div class="panel-heading" data-toggle="collapse" data-target="#Seventh">
                        7<sup>th</sup> Class
                    </div>
                    <div class="panel-body collapse" id="Seventh">
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Mathematics </b>
                            </p>
                            <ul>
                                <li>Lines and angles </li>
                                <li>Triangle and its properties </li>
                                <li>Congruence of triangle </li>
                                <li>Comparing quantities </li>
                                <li>Perimeter and area </li>
                                <li>Algebraic Expression </li>
                                <li>Exponents and powers </li>
                                <li>Simple Equations </li>
                                <li>Data Handing </li>
                                <li>Rational number </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Aptitude</b>
                            </p>
                            <ul>
                                <li>Verbal and Non-Verbal Series</li><li>Verbal and Non-Verbal Analogy </li>
                                <li>Classification </li>
                                <li>Coding-Decoding </li>
                                <li>Blood-Relation </li>
                                <li>Direction-Sense Test </li>
                                <li>Clock and Calendar </li>
                                <li>VENN Diagram </li>
                                <li>Mathematical operation </li>
                                <li>Puzzle Test </li>
                                <li>Missing Character </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Physics</b>
                            </p>
                            <ul>
                                <li>Motion and Time </li>
                                <li>Light </li>
                                <li>Electric current and its effect </li>
                                <li>Heat</li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Chemistry</b>
                            </p>
                            <ul>
                                <li>Acid, Base and Salt </li>
                                <li>Chemical and Physical Change </li>
                                <li>Fibre to Fabric </li>
                                <li>Water </li>
                                <li>Forest </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Biology</b>
                            </p>
                            <ul>
                                <li>Nutrition in plants and animals </li>
                                <li>Respiration in Organisms </li>
                                <li>Transportation in Animals and plants </li>
                                <li>Reproduction in plants. </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>English</b>
                            </p>
                            <ul>
                                <li>Voice </li>
                                <li>Pronoun </li>
                                <li>Adjective </li>
                                <li>Tense </li>
                                <li>Preposition </li>
                                <li>Narration</li>
                                <li>Interjection </li>
                                <li>Subject verb agreement </li>
                                <li>Question tag. </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12">
                <div class="panel panel-primary ">
                    <div class="panel-heading" data-toggle="collapse" data-target="#Eigth">
                        8<sup>th</sup> Class
                    </div>
                    <div class="panel-body collapse" id="Eigth">
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Mathematics </b>
                            </p>
                            <ul>
                                <li>Linear equations in one variable </li>
                                <li>Understanding quadrilateral </li>
                                <li>Comparing quantities</li><li>Direct and Inverse variation</li><li>Mensuration</li><li>
                                    Algebraic expression and identities </li>
                                <li>Exponent and power </li>
                                <li>Square and square root </li>
                                <li>Factorisation</li><li>Visualising solid shapes. </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Aptitude </b>
                            </p>
                            <ul>
                                <li>Verbal and Non-Verbal Series </li>
                                <li>Verbal and Non-Verbal Analogy </li>
                                <li>Classification </li>
                                <li>Coding-Decoding </li>
                                <li>Blood-Relation </li>
                                <li>Direction-Sense Test </li>
                                <li>Number and Ranking (only 8th Class) </li>
                                <li>Cube and Dice (only for 8th Class) </li>
                                <li>Clock and Calendar </li>
                                <li>VENN Diagram </li>
                                <li>Syllogisms (only for 8th Class) </li>
                                <li>Mathematical operation </li>
                                <li>Puzzle Test </li>
                                <li>Missing Character</li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Physics </b>
                            </p>
                            <ul>
                                <li>Force and Pressure </li>
                                <li>Friction </li>
                                <li>Sound </li>
                                <li>Light </li>
                                <li>Some Natural Phenomena</li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Chemistry</b>
                            </p>
                            <ul>
                                <li>Synthetic Fibres and Plastic </li>
                                <li>Metals and Non-Metals</li>
                                <li>Chemical Effects of Electric Current </li>
                                <li>Coal and Petroleum </li>
                                <li>Combustion and Flame </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Biology</b>
                            </p>
                            <ul>
                                <li>Micro-organisms </li>
                                <li>Cell-Structure and Function </li>
                                <li>Reproduction in Animals </li>
                                <li>Reaching the age of adolescence. </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>English</b>
                            </p>
                            <ul>
                                <li>Narration </li>
                                <li>Voice </li>
                                <li>Adjective </li>
                                <li>Tense </li>
                                <li>Preposition </li>
                                <li>Modals </li>
                                <li>Subject verb agreement </li>
                                <li>Idioms and Phrases </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12">
                <div class="panel panel-primary ">
                    <div class="panel-heading" data-toggle="collapse" data-target="#Nineth">
                        9<sup>th</sup> Class
                    </div>
                    <div class="panel-body collapse" id="Nineth">
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Aptitude</b></p>
                            <ul>
                                <li>Series Completion (Verbal & Non Verbal) </li>
                                <li>Coding – Decoding </li>
                                <li>Blood Relation </li>
                                <li>Puzzle Test </li>
                                <li>Clock </li>
                                <li>Calendar </li>
                                <li>Arithmetical Reasoning </li>
                                <li>Inserting the Missing Character </li>
                                <li>Cube and Dice </li>
                                <li>Direction Test </li>
                                <li>Input and Output </li>
                                <li>Figure Counting </li>
                                <li>Sitting Arrangement </li>
                                <li>Analog (Verbal and Non Verbal)</li></ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>English</b></p>
                            <ul>
                                <li>Tenses </li>
                                <li>Modals </li>
                                <li>Non Finites </li>
                                <li>Clauses </li>
                                <li>Preposition </li>
                                <li>Conjunctions </li>
                                <li>Determiners </li>
                                <li>Reported Speech </li>
                                <li>Voice </li>
                                <li>Idioms and Phrases </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Social Studies</b></p>
                            <ul>
                                <li>The French Revolution </li>
                                <li>Russian Revolution </li>
                                <li>Drainage</li><li>Natural Vegetation </li>
                                <li>Climate </li>
                                <li>People as Resource </li>
                                <li>Food Security </li>
                                <li>Working of Institutions </li>
                                <li>Democratic Rights </li>
                                <li>Constitutional Design</li>
                            </ul>
                        </div>
                        <div class="clearfix">
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Mathematics</b></p>
                            <ul>
                                <li>Number System </li>
                                <li>Algebra (Polynomials, Linear Equations)</li><li>Geometry (Triangle , Quadrilaterals
                                    and Circles) </li>
                                <li>Co-ordinate Geometry</li><li>Menstruation (Area of Plane Figures, Surface Area
                                    and Volume) </li>
                                <li>Statistics and Probability </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Biology</b></p>
                            <ul>
                                <li>Cell </li>
                                <li>Plant and animal tissue </li>
                                <li>Plant and animal diversity </li>
                                <li>Improvement in food Resources </li>
                                <li>Natural Resource </li>
                                <li>Why do we fall ill</li></ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Physics</b></p>
                            <ul>
                                <li>Motion </li>
                                <li>Force and Laws of Motion </li>
                                <li>Work and Energy </li>
                                <li>Floatation </li>
                                <li>Gravitation </li>
                                <li>Sound </li>
                            </ul>
                        </div>
                        <div class="clearfix">
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Chemistry</b></p>
                            <ul>
                                <li>Matter </li>
                                <li>Solutions, Colloids and suspensions, Purification Techniques-. </li>
                                <li>Atoms and molecules </li>
                                <li>Mole concept </li>
                                <li>Structure of atom</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12">
                <div class="panel panel-primary ">
                    <div class="panel-heading" data-toggle="collapse" data-target="#Tenth">
                        10<sup>th</sup> Class
                    </div>
                    <div class="panel-body collapse" id="Tenth">
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Aptitude</b></p>
                            <ul>
                                <li>Series Completion (Verbal & Non Verbal) </li>
                                <li>Coding – Decoding </li>
                                <li>Blood Relation </li>
                                <li>Puzzle Test </li>
                                <li>Clock </li>
                                <li>Calendar </li>
                                <li>Arithmetical Reasoning </li>
                                <li>Inserting the Missing Character </li>
                                <li>Cube and Dice </li>
                                <li>Direction Test </li>
                                <li>Input and Output </li>
                                <li>Figure Counting </li>
                                <li>Sitting Arrangement </li>
                                <li>Analog (Verbal and Non Verbal)</li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Mathematics</b></p>
                            <ul>
                                <li>Real Numbers </li>
                                <li>Algebra (Polynomials, Linear Equations, Quadratic Equations, A.P) </li>
                                <li>Geometry (Triangle & Circles) </li>
                                <li>Co-ordinate Geometry </li>
                                <li>Mensuration (Area of Plane Figures, Surface Area and Volume) </li>
                                <li>Trigonometry (Identities & Applications of Trigonometry) </li>
                                <li>Statistics and Probability </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Social Studies</b></p>
                            <ul>
                                <li>Nationalism in India </li>
                                <li>Nationalist Movement in Europe </li>
                                <li>Minerals and Energy Resources </li>
                                <li>Manufacturing Industries </li>
                                <li>Resource and Development </li>
                                <li>Sectors of Indian Economy </li>
                                <li>Money and Credit </li>
                                <li>Political Parties </li>
                                <li>Gender, Religion and Caste </li>
                                <li>Power Sharing </li>
                            </ul>
                        </div>
                        <div class="clearfix">
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>English</b></p>
                            <ul>
                                <li>Tenses </li>
                                <li>Modals </li>
                                <li>Non Finites </li>
                                <li>Clauses </li>
                                <li>Preposition </li>
                                <li>Conjunctions </li>
                                <li>Determiners </li>
                                <li>Reported Speech </li>
                                <li>Voice </li>
                                <li>Idioms and Phrases </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Biology</b></p>
                            <ul>
                                <li>Life Process (Nutrition in plant, Respiration, Transportation in plant & human being,
                                    Excretion in Human being) </li>
                                <li>Control and Co-ordinations </li>
                                <li>How do Organism Reproduce (A sexual Reproduction, Human Reproduction, Reproductive
                                    Health) </li>
                                <li>Heredity and Evolution </li>
                                <li>Management of Natural Resource </li>
                                <li>Our Environment </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Physics</b></p>
                            <ul>
                                <li>Electricity </li>
                                <li>Magnetic Effects of Electric Current </li>
                                <li>Light Reflection & Refraction </li>
                                <li>The Human Eye & the Colourful World </li>
                                <li>Sources of Energy </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <p>
                                <b>Chemistry</b></p>
                            <ul>
                                <li>Chemical reactions and equations </li>
                                <li>Acid, Base and Salts </li>
                                <li>Metals and Non Metals </li>
                                <li>Carbon and its compounds </li>
                                <li>Periodic classification of elements and their properties. </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
