﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="ConfirmPendingRegistration.aspx.cs" Inherits="Olympiad.ConfirmPendingRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <main>
<h1>Confirm Registration</h1>
<div class="row>
<div class="col-sm-12 col-md-12 col-xs-12">
<div class="col-lg-2">

</div>
<div class="col-lg-8">
    
        <div class="dashboard-summery-one">
          <div class="row" style="display: flex; flex-wrap: wrap">
           <div class="col-12">
                 <div class="form-group">
                                        <asp:RadioButtonList ID="rbttype" runat="server" CssClass="rbtbutton" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Confirm Registration & Send Details" Value="0" Selected="True" />
                                            <asp:ListItem Text="Send Only Confirmed Detail" Value="1" />
                                        </asp:RadioButtonList>
                                    </div>
           </div>
          </div>
        <div class="row" style="display: flex; flex-wrap: wrap;margin-bottom: 6px;">
               <div class="col-6">
              Type Ack No. :
               </div>
                <div class="col-6">
              
                 <asp:TextBox ID="txtackno" runat="server" CssClass="InputText" Style="width:100%">
                        </asp:TextBox>

               </div>             
             </div>    
               <div class="row" style="display: flex; flex-wrap: wrap;margin-bottom: 6px;" id="divbankid">
               <div class="col-6">
              Type Bank Transaction ID :
           
               </div>
                <div class="col-6">
                
                 <asp:TextBox ID="txtbankid" runat="server" CssClass="InputText" Style="width:100%">
                        </asp:TextBox>
               </div>             
             </div>             
              <div class="row" style="display: flex; flex-wrap: wrap">
               <div class="col-6">
              
               </div>
                <div class="col-6">
                <asp:Button ID="btnconfirm" Text="Confirm & send Details" CssClass="btn btn-success" 
                        runat="server" Width="100%" OnClientClick="return savevalidate();" onclick="btnconfirm_Click" />
               </div>           
             </div>                      
                   
        </div>
    
</div>
<div class="col-lg-2">

</div>
</div>
</div>
</main>
    <script type="text/javascript">
        $(document).ready(function () {
            rbttypechanges();
        });
        $("#<%=rbttype.ClientID %>").change(function () {
            rbttypechanges();
        });
        function rbttypechanges() {
            var val = $('#<%=rbttype.ClientID %> input:checked').val();
            if (val == 0) {
                $("#<%=btnconfirm.ClientID %>").val('Confirm & send Details');
                $("#divbankid").show();
            }
            else {
                $("#<%=btnconfirm.ClientID %>").val('Send only confirmed Details');
                $("#divbankid").hide();
            }
        }
        function savevalidate() {
            var val = $('#<%=rbttype.ClientID %> input:checked').val();
            var msg = "";
            if (val == 0) { //confirm registration & send details
                if ($("#<%=txtackno.ClientID %>").val() == "") {
                    msg = msg + "Please Enter Ack No. !!!\n";
                }
                if ($("#<%=txtbankid.ClientID %>").val() == "") {
                    msg = msg + "Please Enter Bank Transaction ID !!!\n";
                }
            }
            else {
                if ($("#<%=txtackno.ClientID %>").val() == "") {
                    msg = msg + "Please Enter Ack No. !!!\n";
                }
            }
            if (msg != "") {
                alert(msg);
                return false;
            }
        }
    </script>
</asp:Content>
