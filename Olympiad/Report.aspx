﻿<%@ Page Title="Report" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="Report.aspx.cs" Inherits="Olympiad.Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <main> <h1>
        Dashboard</h1>   
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-sm" style="font-size:15px;color:#777" onclick="getregdetails()">
          <span class="glyphicon glyphicon-refresh"></span> Refresh
        </a>
        </div>
    </div>
<div class="row">
 <div class="dashboard-summery-one" style="margin-bottom:10px;padding: 13px 28px;">
  <div class="item-title" style="color:#edb0b0;font-size:20px">
                                Today's Registration
                            </div>
 </div>
</div>   
 <div class="row">
        <div class="col-lg-4">
            <div class="dashboard-summery-one">
                <div class="row" style="display: flex; flex-wrap: wrap">
                    <div class="col-6">
                        <div class="item-icon bg-light-magenta">
                            <span class="glyphicon glyphicon-pencil" style="margin-top: 30px"></span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="item-content">
                            <div class="item-title">
                                 Online Mode
                            </div>
                            <div class="item-number">
                                <span class="counter" id="lblonreg" data-num="12"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="dashboard-summery-one">
                <div class="row" style="display: flex; flex-wrap: wrap">
                    <div class="col-6">
                        <div class="item-icon bg-light-magenta">
                            <span class="glyphicon glyphicon-pencil" style="margin-top: 30px"></span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="item-content">
                            <div class="item-title">
                                Offline Mode
                            </div>
                            <div class="item-number">
                                <span class="counter" id="lbloffreg" data-num="12"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="dashboard-summery-one">
                <div class="row" style="display: flex; flex-wrap: wrap">
                    <div class="col-6">
                        <div class="item-icon bg-light-magenta">
                            <span class="glyphicon glyphicon-pencil" style="margin-top: 30px"></span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="item-content">
                            <div class="item-title">
                               InComplete
                            </div>
                            <div class="item-number">
                                <span class="counter" id="lblregnotfinal" data-num="12"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
 <div class="dashboard-summery-one" style="margin-bottom:10px;padding: 13px 28px;">
  <div class="item-title" style="color:#edb0b0;font-size:20px">
                                Total Registration
                            </div>
 </div>
</div>   
 <div class="row">
        <div class="col-lg-4">
            <div class="dashboard-summery-one">
                <div class="row" style="display: flex; flex-wrap: wrap">
                    <div class="col-6">
                        <div class="item-icon bg-light-magenta">
                            <span class="glyphicon glyphicon-pencil" style="margin-top: 30px"></span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="item-content">
                            <div class="item-title">
                                Online Mode
                            </div>
                            <div class="item-number">
                                <span class="counter" id="lbltonline" data-num="12"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="dashboard-summery-one">
                <div class="row" style="display: flex; flex-wrap: wrap">
                    <div class="col-6">
                        <div class="item-icon bg-light-magenta">
                            <span class="glyphicon glyphicon-pencil" style="margin-top: 30px"></span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="item-content">
                            <div class="item-title">
                               Offline Mode
                            </div>
                            <div class="item-number">
                                <span class="counter" id="lbltoffline" data-num="12"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="dashboard-summery-one">
                <div class="row" style="display: flex; flex-wrap: wrap">
                    <div class="col-6">
                        <div class="item-icon bg-light-magenta">
                            <span class="glyphicon glyphicon-pencil" style="margin-top: 30px"></span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="item-content">
                            <div class="item-title">
                               InComplete
                            </div>
                            <div class="item-number">
                                <span class="counter" id="lblticm" data-num="12"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </main>
    <script type="text/javascript">
        $(document).ready(function () {
            getregdetails();
        });
        function getregdetails() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Report.aspx/getregdetails",
                data: "{}",
                datatype: "json", success: function (result) {
                    result = JSON.parse(result.d);
                    //ondate
                    $("#lblonreg").text(result['Table'][0].onlinereg);
                    $("#lbloffreg").text(result['Table'][0].offlinereg);
                    $("#lblregnotfinal").text(result['Table1'][0].incomplete);
                    //Total
                    $("#lbltonline").text(result['Table2'][0].onlinereg);
                    $("#lbltoffline").text(result['Table2'][0].offlinereg);
                    $("#lblticm").text(result['Table3'][0].incomplete);

                },
                error: function (xhr, textStatus, errorThrown) {
                    if (xhr.responseText.indexOf("Session Expired") > 0) {
                        //Session has Expired,redirect to login page
                        window.location.href = "../Default.aspx?Message=Session expired please login again";
                    } else {
                        //Other Exceptions/Errors
                        alert("Error Occured");
                    }
                }
            });
        }
    </script>
</asp:Content>
