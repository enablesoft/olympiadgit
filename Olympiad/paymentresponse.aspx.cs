﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Security.Cryptography;
using DotNetIntegrationKit;
using CCA.Util;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using System.Threading;

public partial class Student_paymentresponse : System.Web.UI.Page
{
    static int Compare1(KeyValuePair<string, string> a, KeyValuePair<string, string> b)
    {
        return a.Key.CompareTo(b.Key);
    }
    static string computeHash(string input)
    {

        byte[] data = null;
        data = HashAlgorithm.Create("SHA512").ComputeHash(Encoding.ASCII.GetBytes(input));
        StringBuilder sBuilder = new StringBuilder();

        for (int i = 0; i < data.Length; i++)
        {
            sBuilder.Append(data[i].ToString("x2"));
        }

        return sBuilder.ToString().ToUpper();

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack) { return; }

        AirPay();


    }


    private void showNoValidTransactionFound()
    {
        String NoValidTrans = "No valid transaction was detected.Please login to make a transaction!Redirecting you to Page in 5 seconds.";
        divMain.InnerHtml = NoValidTrans;
        HtmlMeta meta = new HtmlMeta();
        meta.HttpEquiv = "Refresh";
        meta.Content = "10;url=../Default.aspx"; ;
        this.Page.Controls.Add(meta);
        Response.AddHeader("REDIRECT", "5;URL=Default.aspx");
        return;
    }

    private void AirPay()
    {

        string username = MySession.AirPayUserName;
        string password = MySession.AirPayPassWord;
        string secretKey = MySession.AirPaySecretKey;
        string MID = MySession.AirPayMerchantId;

        string error = "";
        if (Request.Params.Get("TRANSACTIONSTATUS") == null)
        {
            showNoValidTransactionFound();
            return;
        }
        string TRANSACTIONSTATUS = Request.Params.Get("TRANSACTIONSTATUS").Trim();
        string APTRANSACTIONID = Request.Params.Get("APTRANSACTIONID").Trim();
        string MESSAGE = Request.Params.Get("MESSAGE").Trim();
        string TRANSACTIONID = Request.Params.Get("TRANSACTIONID").Trim();
        string AMOUNT = Request.Params.Get("AMOUNT").Trim();
        string ap_SecureHash = Request.Params.Get("ap_SecureHash").Trim();
        string mobile = Request.Params.Get("CUSTOMERPHONE").Trim();
        string custmail = Request.Params.Get("CUSTOMEREMAIL").Trim();
        string CUSTOMER = Request.Params.Get("CUSTOMER").Trim();
        if (TRANSACTIONSTATUS == "" || APTRANSACTIONID == "" || TRANSACTIONID == "" || AMOUNT == "" || ap_SecureHash == "")
        {
            if (TRANSACTIONID == "") { error = "TRANSACTIONID"; }
            if (APTRANSACTIONID == "") { error = "APTRANSACTIONID"; }
            if (AMOUNT == "") { error = "AMOUNT"; }
            if (TRANSACTIONSTATUS == "") { error = "TRANSACTIONSTATUS"; }
            if (ap_SecureHash == "") { error = "ap_SecureHash"; }
        }

        //comparing Secure Hash with Hash sent by Airpay
        string sTemp = TRANSACTIONID + ":" + APTRANSACTIONID + ":" + AMOUNT + ":" + TRANSACTIONSTATUS + ":" + MESSAGE + ":" + MID + ":" + username;
        //string strCRC = CRCCode(sTemp, ap_SecureHash);

        if (error == "")
        {
            if (TRANSACTIONSTATUS == "200")  //Sucessfull status
            {
                //Update Payment Status
                SqlParameter[] param = new SqlParameter[7];
                param[0] = new SqlParameter("@ACKNO", TRANSACTIONID);
                param[1] = new SqlParameter("@TRANSACTIONID", APTRANSACTIONID);
                param[2] = new SqlParameter("@BankAmt", AMOUNT);
                param[3] = new SqlParameter("@REGNO", SqlDbType.BigInt);
                param[3].Direction = ParameterDirection.Output;
                param[4] = new SqlParameter("@Pwd", SqlDbType.VarChar, 30);
                param[4].Direction = ParameterDirection.Output;
                param[5] = new SqlParameter("@Name", SqlDbType.VarChar, 50);
                param[5].Direction = ParameterDirection.Output;
                param[6] = new SqlParameter("@HelpNo", SqlDbType.VarChar, 50);
                param[6].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "PRO_REGISTRATIONFINAL", param);
                //**********************
                Thread email = new Thread(delegate()
                {
                    string varHelpNo = "";
                    string regno = param[3].Value.ToString();
                    string pwd = param[4].Value.ToString();
                    CUSTOMER = param[5].Value.ToString();
                    varHelpNo = param[6].Value.ToString();

                    string MSG = @"Dear " + CUSTOMER + Environment.NewLine + "Congratulations! Registered sucessfully for All India RPS Olympiad 2020 Phase-I. Your Registration No. is " + param[3].Value.ToString() + ", Login id is " + param[3].Value.ToString() + "  and pwd : " + param[4].Value.ToString() + "" + Environment.NewLine + "For any query please contact:" + varHelpNo;
                    SendMail.FireSms(mobile, MSG, varUnicode: "0");
                    string htmlString = @"<html>
                      <body>
                      <p>Dear " + CUSTOMER + @" ,</p>
                      <p>Thank you for Registering for Rps 2020 Olympiad. Your account has now been activated.</p>
                      <br></br>
                      <b>To Login:</b><br/>
                      UserName : " + regno + @"<br/>
                      Password : " + pwd + @"
                      </body>
                      </html>
                     ";
                    if (custmail.ToString() != "")
                    {
                        SendMail.sendmail(pTo: custmail, pSubject: "Registration: Rps 2020 Olympiad", pBody: htmlString, pFromSMTP: "smtp_1", pFromDispName: "", pCC: "", pAttchmntName: "", pBCC: "");
                    }
                });
                email.IsBackground = true;
                email.Start();
                //**********************
                redirectBackToAccounts("<table width='100%'><tr width='100%'><td>Thanku for Registering. Check your mail and message for further details.</td></tr><tr width='100%'><td align='left' width='50%'>Transaction Id</td><td align='left' width='50%' style='color:black;'>" + TRANSACTIONID + "</td></tr><tr width='100%'><td align='left' width='50%'>Airpay Transaction Id</td><td align='left' width='50%' style='color:black;'>" + APTRANSACTIONID + "</td></tr><tr width='100%'><td align='left' width='50%'>Amount</td><td align='left' width='50%' style='color:black;'>" + AMOUNT + "</td></tr><tr width='100%'><td align='left' width='50%'>Transaction Status Code</td><td align='left' width='50%' style='color:black;'>" + TRANSACTIONSTATUS + "</td></tr><tr width='100%'><td align='left' width='50%'>Message</td><td align='left' width='50%' style='color:black;'>" + MESSAGE + "</td></tr><tr width='100%'><td align='left' width='50%'>Status</td><td align='left' width='50%' style='color:green;'>Success</td></tr></table>");
            }
            else
            {
                redirectBackToAccounts("<table width='100%'><tr width='100%'><td align='left' width='50%'>Transaction Id</td><td align='left' width='50%' style='color:black;'>" + TRANSACTIONID + "</td></tr><tr width='100%'><td align='left' width='50%'>Airpay Transaction Id</td><td align='left' width='50%' style='color:black;'>" + APTRANSACTIONID + "</td></tr><tr width='100%'><td align='left' width='50%'>Amount</td><td align='left' width='50%' style='color:black;'>" + AMOUNT + "</td></tr><tr width='100%'><td align='left' width='50%'>Transaction Status Code</td><td align='left' width='50%' style='color:black;'>" + TRANSACTIONSTATUS + "</td></tr><tr width='100%'><td align='left' width='50%'>Message</td><td align='left' width='50%' style='color:black;'>" + MESSAGE + "</td></tr><tr width='100%'><td align='left' width='50%'>Status</td><td align='left' width='50%' style='color:green;'>Failed</td></tr></table>");
            }
        }
        else
        {
            redirectBackToAccounts("<table width='100%'><tr><td align='center'>Variable(s) " + error + " is/are empty.</td></tr></table>");
        }

    }

    public string CRCCode(String ClearString, String key)
    {
        Crc32 crc32 = new Crc32();
        String hash = String.Empty;
        byte[] mybytes = Encoding.UTF8.GetBytes(ClearString);
        foreach (byte b in crc32.ComputeHash(mybytes)) hash += b.ToString("x2");
        UInt32 Output = UInt32.Parse(hash, System.Globalization.NumberStyles.HexNumber);
        UInt32 Output1 = UInt32.Parse(key);
        //  Response.Write(Output);
        //  Response.Write(Output1);
        if (Output1 == Output)
        {
            // Response.Write("Secure Hash match.");
            // return true.ToString();

        }
        else
        {
            Response.Write("Secure Hash mismatch.");
            // return true.ToString();
            // Environment.Exit(0);
        }

        return hash;

    }
    private void redirectBackToAccounts(string Msg)
    {
        HtmlMeta meta = new HtmlMeta();
        meta.HttpEquiv = "Refresh";
        meta.Content = "10;url=Default.aspx"; ;
        this.Page.Controls.Add(meta);
        divMain.InnerHtml = Msg;
        Response.AddHeader("REDIRECT", "10;URL=Default.aspx");
    }

}