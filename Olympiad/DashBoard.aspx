﻿<%@ Page Title="Student Dashboard" Language="C#" AutoEventWireup="true" CodeBehind="DashBoard.aspx.cs"
    Inherits="Olympiad.DashBoard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="UTF-8" />
    <link href="Images/Favicon.png" rel="shortcut icon" type="image/png" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="theme-color" content="#103157">
    <script src="Style/Bootstrap/jquery/jquery.min.js" type="text/javascript"></script>
    <link href="Style/Bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Style/Bootstrap/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="Style/Style.css" rel="stylesheet" type="text/css" />
    <style>
        .errorCode
        {
            color: Red;
        }
        .rbtbutton
        {
            width: 100%;
            border: 1px solid silver;
        }
        .InputText
        {
            width: 100%;
            display: inline-block;
            padding: 5px;
            height: 31px;
        }
        .Registration_div
        {
            font-family: 'Roboto';
            display: inline-block;
            padding: 5px;
            display: inline-block;
            background-color: #fff;
            -webkit-box-shadow: 0 1px 6px rgba(57,73,76,0.35);
            box-shadow: 0 1px 6px rgba(57,73,76,0.35);
            min-height: 400px;
            width: 100%;
            margin-top: 10px;
        }
        .Registration_div p
        {
            margin: 0px 0px 5px 0px;
            color: #7e7e7e;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Header">
        <div class="Layer1">
            <div class="container">
                <div class="col-sm-6 col-xs-12">
                    <a href="#" class="Header_social_Links"><i class="fa fa-envelope"></i>&nbsp;rpsolympiad@gmail.com</a>
                    <%--<a href="#" class="Header_social_Links"><i class="fa fa-phone"></i>&nbsp;+91-9416150201</a>--%>
                </div>
                <div class="col-sm-6 col-xs-12 text-right">
                    <a class="SignUp"><i class=" fa fa-user"></i>&nbsp;<asp:Label ID="lbluser" runat="server"
                        Text="UserName" /></a>
                    <asp:LinkButton runat="server" CssClass="SignIn" OnClick="onClickLogOut"><i class=" fa fa-sign-in"></i>&nbsp;logOut</asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="layer2">
            <div class="container">
                <div class="col-sm-3 col-xs-12 col-md-3">
                    <div class="Logo">
                        <a href="#">
                            <img src="Images/RPSGOS.PNG" alt="RPSGOS" />
                        </a>
                    </div>
                </div>
                <div class="col-sm-6  col-md-6">
                </div>
                <div class="col-sm-3 col-xs-12 col-md-3">
                    <div class="Logo">
                        <a href="#">
                            <img src="Images/Logo-rpsolympiad.png" alt="RPSGOS" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="PageContent">
        <div class="container">
            <div class="col-lg-12">
                <a href="#" class="btn btn-sm" style="font-size: 30px; color: #777" onclick="getregstudetails()">
                    <span class="glyphicon glyphicon-refresh"></span>Refresh </a>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="Box">
                    <h3>
                        Profile</h3>
                    <table class="table table-striped">
                        <tr>
                            <td>
                                <b>Class </b>
                            </td>
                            <td>
                                <span id="s_class"></span>
                                <asp:HiddenField ID="hn_classid" runat="server" Value="0" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Registration No </b>
                            </td>
                            <td>
                                <span id="s_regno"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Name of Candidate</b>
                            </td>
                            <td>
                                <span id="s_name"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Father's Name</b>
                            </td>
                            <td>
                                <span id="s_fname"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>D.O.B</b>
                            </td>
                            <td>
                                <span id="s_dob"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Email</b>
                            </td>
                            <td>
                                <span id="s_email"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Contact No</b>
                            </td>
                            <td>
                                <span id="s_contactno"></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"
                                    onclick="getdataforupdate()" style="display: none">
                                    Edit Details(This options works only once.)</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="Box">
                    <h3>
                        Latest News</h3>
                    <table class="table table-striped">
                        <tr style="display: none">
                            <td style="width: 20%">
                                <b>Admit Card</b>
                            </td>
                            <td>
                                <a onclick="ClickShowPopupAdmitCard()"><i class=" fa fa-download"></i>&nbsp;Print Admit
                                    Card</a>
                            </td>
                        </tr>
                        <tr id="trresult" style="display: none">
                            <td style="width: 20%" id="tdresult">
                                <b>Result Phase-I</b>
                            </td>
                            <td>
                                <span id="s_result1"></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:Button ID="btnsamletest" Text="START OLYMPIAD TEST" runat="server" CssClass="btn btn-success"
                    OnClick="btnsamletest_Click" Width="100%" Visible="false" />
                <asp:HiddenField ID="hn_testid" runat="server" Value="2" />
            </div>
            <div class="clearfix">
            </div>
            <div class="col-sm-6 col-xs-12" id="divmarks1" style="display: none">
                <div class="Box">
                    <h3>
                        Marks Detail(Phase-I)</h3>
                    <table class="table table-striped" id="tblmarks">
                        <th>
                            Subject
                        </th>
                        <th>
                            Marks
                        </th>
                    </table>
                </div>
            </div>
        </div>
        <%--<div class="col-sm-12 col-xs-12 text-center">
            <asp:Button Text="Downlaod Admit Card" ID="btnadmitcard" 
                CssClass="btn btn-success" runat="server" onclick="btnadmitcard_Click" />
        </div>--%>
    </div>
    <div class="ModelPop collapse" id="PopUpAdmitCard">
        <div class="div_admitcard">
            <div class="col-sm-12">
                <div id="PrintDiv" style="width: 700px; border: 1px solid gray; padding: 10px; display: block;
                    margin: 0px auto">
                    <h2 style="text-align: center">
                        ALL INDIA RPS OLYMPIAD -2020</h2>
                    <h4 style="text-align: center">
                        ADMIT CARD</h4>
                    <div style="width: 100%">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 33%" class="style1">
                                    <b>Class </b>
                                </td>
                                <td style="width: 33%" class="style1">
                                    <span id="A_class"></span>
                                </td>
                                <td colspan="2" rowspan="5" style="width: 33%" class="style1">
                                    <div style="width: 140px; height: 140px; padding: 10px; border: 1px solid gray; margin: 0;">
                                        Please affix recent coloured passport size photo
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Registration No </b>
                                </td>
                                <td>
                                    <span id="A_regno"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Name of Candidate</b>
                                </td>
                                <td>
                                    <span id="A_name"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Father’s name</b>
                                </td>
                                <td>
                                    <span id="A_fname"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>D.O.B. </b>
                                </td>
                                <td>
                                    <span id="A_dob"></span>
                                </td>
                                <td colspan="2" style="width: 33%" class="style1">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Contact No.</b>
                                </td>
                                <td>
                                    <span id="A_contactno"></span>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                        <hr />
                        <h3 style="text-align: center">
                            Details of Examination Centre</h3>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 30%">
                                    <b>Name of Exam Centre </b>
                                </td>
                                <td style="width: 70%">
                                    <span id="A_centre"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Address </b>
                                </td>
                                <td>
                                    <span id="A_centreadd"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Date of Examination </b>
                                </td>
                                <td>
                                    02nd February, 2020
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Entry Time </b>
                                </td>
                                <td>
                                    11:00 AM onwards
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Time of examination </b>
                                </td>
                                <td>
                                    12:00 Noon
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td style="text-align: right">
                                    <img alt="Sign" class="style2" src="Images/BLYadav.jpg" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <p style="text-align: right">
                                        <b>Signature of exam controller</b></p>
                                </td>
                            </tr>
                        </table>
                        <h3>
                            GENERAL INSTRUCTION</h3>
                        <ol style="text-align: justify;">
                            <li>For offline mode of examination, no student shall be allowed to appear in the exam
                                without admit card. </li>
                            <li>The candidate is required to paste a recent coloured passport size photograph in
                                the space given in the admit card. </li>
                            <li>The candidate should report the examination centre 60 minutes before, after the
                                commencement of the offline exam, no candidate will be allowed to enter the exam.
                                centre under any circumstance. </li>
                            <li>Candidate should bring his/her own black/blue ball point pen for filling OMR sheet
                                as no pen shall be provided to the candidate from exam centre. </li>
                            <li>Without admit card and school Identity Card (clearly showing the present session
                                and class) / bonafied certificate from the school clearly showing class and session
                                with attested Photograph on school letter head, no student shall be allowed to appear
                                in the exam. under any circumstance. </li>
                            <li>Candidate has to use black/blue ball point pen to write/fill his particulars on
                                the Question Booklet and OMR sheet. Use of white fluid and over writing/cutting
                                on OMR sheet is STRICTLY prohibited. </li>
                            <li>Candidate should ensure that he/she has darken the proper circles to fill the information
                                or responses on OMR sheet. </li>
                            <li>After completion of test, the candidate must handover the OMR sheet to the invigilator
                                and the question booklet can be taken away by him. </li>
                            <li>No electric gadget of any type is allowed in the exam. Sufficient space for rough
                                work will be given on test booklet. </li>
                        </ol>
                    </div>
                </div>
                <div style="width: 700px; border: 1px solid gray; padding: 10px; display: block;
                    margin: 0px auto; text-align: right">
                    <input type="button" class="btn btn-danger" value="Close" id="btnClose" />
                    <input type="button" class="btn btn-success" onclick="PrintPanel()" value="Print Admit Card" />
                </div>
            </div>
        </div>
    </div>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &times;</button>
                    <h4 class="modal-title">
                        Update Details</h4>
                </div>
                <div class="modal-body">
                    <div class="Registration_div">
                        <div class="col-sm-12 col-xs-12 col-md-12">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <p>
                                            Class Studying in (2019-20)
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                                                ControlToValidate="ddlClass" InitialValue="-1" CssClass="errorCode" ValidationGroup="R">
                                            </asp:RequiredFieldValidator></p>
                                        <asp:DropDownList ID="ddlClass" runat="server" CssClass="InputText">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <p>
                                            Gender</p>
                                        <asp:RadioButtonList ID="rbtGender" runat="server" CssClass="rbtbutton" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Male" Value="Male" Selected="True" />
                                            <asp:ListItem Text="Female" Value="Female" />
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12 col-md-12">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <p>
                                            Name of the Candidate
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ControlToValidate="txtName"
                                                ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                                        <asp:TextBox ID="txtName" runat="server" CssClass="InputText" Style="text-transform: uppercase"
                                            MaxLength="50">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <p>
                                            Name of the Father
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="*" ControlToValidate="txtFatherName"
                                                ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                                        <asp:TextBox ID="txtFatherName" runat="server" CssClass="InputText" Style="text-transform: uppercase"
                                            MaxLength="50">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12 col-md-12">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <p>
                                            Name of the Mother
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="*" ControlToValidate="txtMother"
                                                ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                                        <asp:TextBox ID="txtMother" runat="server" CssClass="InputText" Style="text-transform: uppercase"
                                            MaxLength="50">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <p>
                                            Date of Birth(dd/MM/YYYY)
                                            <asp:RegularExpressionValidator ID="revDate1" ErrorMessage="*" CssClass="errorCode"
                                                ValidationGroup="R" ControlToValidate="txtDOB" runat="server" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/]\d{4}" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="*" ControlToValidate="txtDOB"
                                                ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                                        <asp:TextBox ID="txtDOB" runat="server" CssClass="InputText" placeholder="dd/MM/YYYY">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12 col-md-12">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <i class="icon-bar"></i><b>Complete Address</b></div>
                                        <div class="panel-body" style="padding-left: 0px; padding-right: 0px">
                                            <div class="col-sm-12 col-xs-12 col-md-12">
                                                <div class="form-group">
                                                    <p>
                                                        Address
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="*" ControlToValidate="txtAddress"
                                                            ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                                                    <asp:TextBox ID="txtAddress" runat="server" CssClass="InputText" MaxLength="100">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-md-12">
                                                <div class="form-group">
                                                    <p>
                                                        City
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="*" ControlToValidate="txtcity"
                                                            ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                                                    <asp:TextBox ID="txtcity" runat="server" CssClass="InputText" MaxLength="50">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-md-12">
                                                <div class="form-group">
                                                    <p>
                                                        Pin Code
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator10" ErrorMessage="*" ControlToValidate="txtpincode"
                                            ValidationGroup="R" CssClass="errorCode" runat="server" />--%>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtpincode"
                                                            ErrorMessage="Invalid" ValidationExpression="[0-9]{6}" ValidationGroup="R" CssClass="errorCode"></asp:RegularExpressionValidator></p>
                                                    <asp:TextBox ID="txtpincode" runat="server" CssClass="InputText" MaxLength="6" placeholder="(Max 6 characters)">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-md-12">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <p>
                                                            State</p>
                                                        <asp:DropDownList runat="server" ID="ddlstate_comp" CssClass="InputText">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <i class="icon-bar"></i><b>Present School Detail</b></div>
                                        <div class="panel-body" style="padding-left: 0px; padding-right: 0px">
                                            <div class="col-sm-12 col-xs-12 col-md-12">
                                                <div class="form-group">
                                                    <p>
                                                        School Name
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ErrorMessage="*" ControlToValidate="txtschoolname"
                                                            ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                                                    <asp:TextBox ID="txtschoolname" runat="server" CssClass="InputText" MaxLength="100">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-md-12">
                                                <div class="form-group">
                                                    <p>
                                                        Address
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="*" ControlToValidate="txtsch_add"
                                                            ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                                                    <asp:TextBox ID="txtsch_add" runat="server" CssClass="InputText" MaxLength="100">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-md-12">
                                                <div class="form-group">
                                                    <p>
                                                        City
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="*" ControlToValidate="txtsch_city"
                                                            ValidationGroup="R" CssClass="errorCode" runat="server" /></p>
                                                    <asp:TextBox ID="txtsch_city" runat="server" CssClass="InputText" MaxLength="50">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-md-12">
                                                <div class="form-group">
                                                    <p>
                                                        Pin Code
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtsch_pincode"
                                                            ErrorMessage="Invalid" ValidationExpression="[0-9]{6}" ValidationGroup="R" CssClass="errorCode"></asp:RegularExpressionValidator></p>
                                                    <asp:TextBox ID="txtsch_pincode" runat="server" CssClass="InputText" MaxLength="6"
                                                        placeholder="(Max 6 characters)">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-md-12">
                                                <div class="form-group">
                                                    <p>
                                                        State</p>
                                                    <asp:DropDownList runat="server" ID="ddlstate_schadd" CssClass="InputText">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-md-12">
                                                <div class="form-group">
                                                    <p>
                                                        Your School is Affiliated to</p>
                                                    <asp:TextBox ID="txtSchoolAffiliatedTo" runat="server" CssClass="InputText" Style="text-transform: uppercase"
                                                        MaxLength="20">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="Update" class="btn btn-success" runat="server" ValidationGroup="R"
                                OnClientClick="return updatedetails()" />
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close</button>
                            <input type="hidden" id="hnregamt" />
                        </div>
                    </div>
                </div>
            </div>
            <div id="wait" style="display: none; width: 69px; height: 89px; border: 1px solid black;
                position: absolute; top: 50%; left: 50%; padding: 2px;">
                <img src="SampleImages/demo_wait.gif" width="64" height="64" /><br>
                Loading..
            </div>
            <asp:HiddenField ID="hn_regno" runat="server" />
            <script src="Style/Bootstrap/jquery/jquery.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $(document).ajaxStart(function () {
                        $("#wait").css("display", "block");
                    });
                    $(document).ajaxComplete(function () {
                        $("#wait").css("display", "none");
                    });
                    getregstudetails();
                });

                function ClickShowPopupAdmitCard() {
                    //            var margin = $(window).width() - $(".div_admitcard").width() - 20;
                    //            $(".div_admitcard").css('margin-left', margin / 2 + 'px');
                    //            $(".div_admitcard").css('margin-right', margin / 2 + 'px');
                    //            $("#PopUpAdmitCard").show("slow");
                    PrintPanel();
                }
                function PrintPanel() {
                    var panel = document.getElementById("PrintDiv");
                    var printWindow = window.open('', '', 'height=500,width=800');
                    printWindow.document.write('<html><head><title>Admit-Card</title>');
                    printWindow.document.write('</head><body >');
                    printWindow.document.write(panel.innerHTML);
                    printWindow.document.write('</body></html>');
                    printWindow.document.close();
                    setTimeout(function () {
                        printWindow.print();
                    }, 500);
                    return false;
                }
                function getregstudetails() {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Dashboard.aspx/getregisteredstudata",
                        data: "{}",
                        datatype: "json", success: function (result) {
                            result = JSON.parse(result.d);
                            var tbl1 = result['Table'];
                            //profile
                            $("#lbluser").text(tbl1[0].Name);
                            $("#s_class").text(tbl1[0].class);
                            $("#s_regno").text(tbl1[0].RegNo);
                            $("#s_name").text(tbl1[0].Name);
                            $("#s_fname").text(tbl1[0].FName);
                            $("#s_dob").text(tbl1[0].dob);
                            $("#s_contactno").text(tbl1[0].ContactNo);
                            $("#s_email").text(tbl1[0].Email);
                            $("#hn_classid").val(tbl1[0].classid);
                            //*****Admit card
                            $("#A_class").text(tbl1[0].class);
                            $("#A_regno").text(tbl1[0].RegNo);
                            $("#A_name").text(tbl1[0].Name);
                            $("#A_fname").text(tbl1[0].FName);
                            $("#A_dob").text(tbl1[0].dob);
                            $("#A_contactno").text(tbl1[0].ContactNo);
                            $("#A_centre").text(tbl1[0].centre);
                            $("#A_centreadd").text(tbl1[0].centreadd);
                            //*****Marks Details
                            var tblmark = result['Table1'];
                            var tblresult = result['Table2'];

                            var total = 0;
                            var msg = "";
                            $("#divmarks1").hide();
                            $("#s_result1").text("");
                            $("#trresult").hide();
                            if (tblresult.length > 0) {
                                $("#trresult").show();
                                var showmarks = tblresult[0].showmarks.toLowerCase();
                                var qualified = tblresult[0].qualified.toLowerCase();
                                if (showmarks == "yes") {
                                    if (tblmark.length > 0) {
                                        $("#tblmarks").find("tr:gt(0)").remove();
                                        for (var i = 0; i < tblmark.length; i++) {
                                            total += parseFloat(tblmark[i].Marks);
                                            $("#tblmarks").append('<tr><td>' + tblmark[i].SubjectName + '</td><td>' + tblmark[i].Marks + '</td></tr>');
                                        }
                                        $("#tblmarks").append('<tr><td><b>Total</b></td><td><b>' + total + '</b></td></tr>')
                                        $("#divmarks1").show();
                                    }
                                }
                                if (qualified == "yes") {
                                    msg = "Congratulations!\n";
                                    msg += "You have QUALIFIED for All India RPS Olympiad 2020 Phase-II Examination to be held on Sunday, 09-Feb-2020 at 12:00 Noon sharp in OFFLINE MODE ONLY.\n";
                                    msg += "TEAM RPS OLYMPIAD 2020";
                                    $("#s_result1").text(msg);
                                }
                                else {
                                    msg = "You could not qualify for  All India RPS Olympiad 2020 Phase-II Examination TEAM RPS OLYMPIAD 2020";
                                    $("#s_result1").text(msg);
                                }
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            if (xhr.responseText.indexOf("Session Expired") > 0) {
                                //Session has Expired,redirect to login page
                                window.location.href = "../Default.aspx?Message=please login again";
                            } else {

                            }
                        }
                    });
                }
                function getdataforupdate() {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json;charset=utf-8",
                        url: "Dashboard.aspx/getdataforupdate",
                        data: "{}",
                        datatype: "json",
                        success: function (result) {
                            result = JSON.parse(result.d);
                            $("#ddlClass").val(result[0].classid);
                            $("#txtName").val(result[0].Name);
                            $("#txtFatherName").val(result[0].FName);
                            $("#txtMother").val(result[0].MName);
                            $("#txtDOB").val(result[0].dob);
                            $("#txtAddress").val(result[0].Address);
                            $("#txtcity").val(result[0].City);
                            $("#txtpincode").val(result[0].Pincode);
                            $("#ddlstate_comp").val(result[0].Stateid);
                            $("#txtschoolname").val(result[0].schoolname);
                            $("#txtsch_add").val(result[0].Sch_address);
                            $("#txtsch_city").val(result[0].Sch_City);
                            $("#txtsch_pincode").val(result[0].Sch_pincode);
                            $("#ddlstate_schadd").val(result[0].sch_stateid);
                            $("#txtSchoolAffiliatedTo").val(result[0].Affiliated);
                            $('#rbtGender').find("input[value='" + result[0].gender + "']").prop("checked", true);
                            $("#hnregamt").text(result[0].RegAmt);
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            if (xhr.responseText.indexOf("Session Expired") > 0) {
                                window.location.href = "../Default.aspx?Message=pleaselogin again";
                            } else {
                                alert("Error Occured");
                            }
                        }
                    });
                }
                function updatedetails() {
                    if (Page_ClientValidate('R')) {
                        if (!confirm('Are you sure?')) {
                            return false;
                        }

                        var gender = $('#rbtGender input:checked').val();
                        var data = "{regno:'" + $("#hn_regno").val() + "',classid:" + $("#ddlClass").val() + ",gender:'" + gender + "',name:'" + $("#txtName").val() + "',fname:'" + $("#txtFatherName").val() + "',mname:'" + $('#txtMother').val() + "',dob:'" + $('#txtDOB').val() + "',address:'" + $('#txtAddress').val() + "',city:'" + $('#txtcity').val() + "',pincode:'" + $('#txtpincode').val() + "',stateid:" + $("#ddlstate_comp").val() + ",schoolname:'" + $('#txtschoolname').val() + "',sch_address:'" + $('#txtsch_add').val() + "',sch_city:'" + $('#txtsch_city').val() + "',sch_pincode:'" + $('#txtsch_pincode').val() + "',sch_stateid:" + $("#ddlstate_schadd").val() + ",affiliated:'" + $('#txtSchoolAffiliatedTo').val() + "',regamt:" + $("#hnregamt").text() + "}";
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "Dashboard.aspx/updatedetails",
                            data: data,
                            datatype: "json",
                            success: function (result) {
                                alert(result.d);
                            },
                            error: function (xhr, textStatus, errorThrown) {
                                if (xhr.responseText.indexOf("Session Expired") > 0) {
                                    window.location.href = "../Default.aspx?Message=please login again";
                                } else {
                                    alert("Error Occured");
                                }
                            }
                        });
                    }
                    return false;
                }
            </script>
    </form>
</body>
</html>
