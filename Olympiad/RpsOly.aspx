﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RpsOly.aspx.cs" Inherits="Olympiad.RpsOly" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Style/Bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Style/Bootstrap/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="Style/Style.css?v=1.1" rel="stylesheet" type="text/css" />
    <link href="Style/animate.css" rel="stylesheet" type="text/css" />
    <meta name="google-site-verification" content="IShOe6zOVfla-fiLGcN-87PDE-KyTyaaPmJfgZzo52s" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="ModelPop" id="zoomOutIn">
        <div class="LoginBox">
            <a class="Model_closebtn">X</a>
            <div class="heading">
                <h2>
                    Log in</h2>
            </div>
            <div class="Login_Desc">
                <asp:Label ID="lblmsg" CssClass="errorcode" Text="" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Enter UserId"
                    CssClass="errorcode" ValidationGroup="a" ControlToValidate="txtUserid" runat="server" />
                <asp:TextBox runat="server" ID="txtUserid" placeholder="User Id" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Enter Password"
                    ControlToValidate="txtPass" ValidationGroup="a" CssClass="errorCode" runat="server" />
                <asp:TextBox ID="txtPass" runat="server" CssClass="InputText" placeholder="Password"
                    TextMode="Password">
                </asp:TextBox>
                <div class="col-sm-12 col-xs-12 text-center">
                    <label id="lblalertcaptch" class="errorcode">
                    </label>
                    <div class="g-recaptcha" data-sitekey="6Leh9ssUAAAAAH9jaDBJGt48XgbMsVNx41CEy5_G">
                    </div>
                </div>
                <div style="padding: 5px; display: inline-block; width: 100%">
                </div>
                <asp:Button class="vldform__button" Text="Log In" ID="btnLogin" OnClientClick="return checkCaptcha()"
                    runat="server" ValidationGroup="a" onclick="btnLogin_Click" />
                <div class="col-sm-6 col-xs-12" style="display: none">
                    <p class="vldform__signup">
                        Don't have account? <a class="vldform__signuplink" id="log_signUp">Sign up</a>
                    </p>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <a href="#" onclick="forgotpwd()">Forgot Password ?</a>
                </div>
                <script src="https://www.google.com/recaptcha/api.js" async defer></script>
            </div>
        </div>
        <script src="Style/Bootstrap/jquery/jquery.min.js" type="text/javascript"></script>
        <script src="Style/Bootstrap/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            function forgotpwd() {
                alert('Contact to service provider.')
            }
            function checkCaptcha() {
                var UID = document.getElementById('<%=txtUserid.ClientID%>').value;
                if (UID == "") {
                    alert("Enter UserId");
                    document.getElementById('<%=txtUserid.ClientID%>').focus();
                    return false;
                }
                var Upass = document.getElementById('<%=txtPass.ClientID%>').value;
                if (Upass == "") {
                    alert("Enter Password");
                    document.getElementById('<%=txtPass.ClientID%>').focus();
                    return false;
                }
                var v = grecaptcha.getResponse();
                if (v.length == 0) {
                    document.getElementById('lblalertcaptch').innerHTML = "You can't leave Captcha Code empty";
                    return false;
                }
                else {
                    document.getElementById('lblalertcaptch').innerHTML = "Captcha completed";
                    return true;
                }
            }
        </script>
    </form>
</body>
</html>
