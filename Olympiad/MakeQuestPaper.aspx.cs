﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ManageTests : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadClass();
            ddlNegMarking_SelectedIndexChanged(sender, e);
        }
        LoadSubjects();
        showQuestionPaper();
    }
    protected void ddlOnlineTests_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadTestDetails();
    }


    private void LoadSubjects()
    {
        DataTable dt = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, @"select b.Id SubjectId,b.Name SubjectName,0 TotalQuests from OnlineTestClasswiseTestSubj  a
left join OnlineTestSubjectMaster b on a.SubjectId=b.Id  where b.IsActive=1 and ClassId=" + ddlClass.SelectedValue);
        DataTable dtDiffLevels = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, @"select Id,Name,0 as TotalQuests 
        from OnlineTestQuestDiffcultyLevels where IsActive=1");
        ViewState["DiffLevels"] = dtDiffLevels;
        tblQuestManifest.Rows.Clear();
        int TotalQuestsOfTest = Convert.ToInt16(txtTotalNoOfQuests.Text.Trim());
        TableRow th = new TableRow();
        foreach (DataColumn dc in dt.Columns)
        {
            TableHeaderCell td = new TableHeaderCell();

            switch (dc.ColumnName)
            {
                case "SubjectName":
                    Label lbl = new Label();
                    lbl.Text = "Subject";
                    td.Controls.Add(lbl);
                    th.Cells.Add(td);
                    break;
                case "TotalQuests":
                    Label txtBox = new Label();
                    txtBox.Text = "Total Quests";
                    td.Controls.Add(txtBox);
                    th.Cells.Add(td);
                    break;
            }

        }

        foreach (DataRow dr1 in dtDiffLevels.Rows)
        {
            TableCell td = new TableCell();
            Label txtBox = new Label();
            txtBox.Text = dr1["Name"].ToString();
            td.Controls.Add(txtBox);
            th.Cells.Add(td);
        }
        tblQuestManifest.Rows.Add(th);
        int RowCount = dt.Rows.Count;
        foreach (DataRow dr in dt.Rows)
        {
            int totalOfThisSubject = Convert.ToInt16(Math.Round(Convert.ToDouble(TotalQuestsOfTest / RowCount)));
            TableRow tr = new TableRow();
            foreach (DataColumn dc in dt.Columns)
            {
                TableCell td = new TableCell();
                switch (dc.ColumnName)
                {
                    case "SubjectName":
                        Label lbl = new Label();
                        lbl.Text = dr[dc.ColumnName].ToString();
                        td.Controls.Add(lbl);
                        tr.Cells.Add(td);
                        break;
                    case "TotalQuests":
                        TextBox txtBox = new TextBox();
                        txtBox.ID = "SubjectId_" + dr["SubjectId"].ToString();
                        TotalQuestsOfTest -= totalOfThisSubject;
                        txtBox.Text = totalOfThisSubject.ToString();
                        RowCount--;
                        td.Controls.Add(txtBox);
                        tr.Cells.Add(td);
                        break;
                }

            }
            int RowCountDiffWise = dtDiffLevels.Rows.Count;
            foreach (DataRow dr1 in dtDiffLevels.Rows)
            {
                int totalOfThisDiff = Convert.ToInt16(Math.Round(Convert.ToDouble(totalOfThisSubject / RowCountDiffWise)));
                RowCountDiffWise--;
                totalOfThisSubject -= totalOfThisDiff;
                TableCell td = new TableCell();
                TextBox txtBox = new TextBox();
                txtBox.ID = dr["SubjectId"].ToString() + "DiffLevel_" + dr1["Id"].ToString();
                txtBox.Text = totalOfThisDiff.ToString();
                td.Controls.Add(txtBox);
                tr.Cells.Add(td);
            }
            tblQuestManifest.Rows.Add(tr);
        }

    }





    private void LoadQuestions()
    {
        DataTable dtQuestPlaceHolder = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, "select top 0 Id,Name,ClassId,SubjectId,MarksOfQuestion,DifficultyLevelId,ImageUrl from OnlineTestQuestions");
        int TotalQuests = Convert.ToInt16(txtTotalNoOfQuests.Text);
        DataTable dtDiffLevels = ViewState["DiffLevels"] as DataTable;
        if (dtDiffLevels == null)
            return;
        DataSet dsQuests = new DataSet();
        int TotalQuestsOfThisTest = 0;
        for (int i = 1; i < tblQuestManifest.Rows.Count; i++)
        {
            TableRow tr = tblQuestManifest.Rows[i];
            TextBox txtBoxTotalQuests = tr.Cells[1].Controls[0] as TextBox;
            String SubjectId = txtBoxTotalQuests.ID.Split('_')[1];
            String SubjectName = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, "Select Name from OnlineTestSubjectMaster where Id=" + SubjectId, SqlHelper.QueryType.Scaler);
            int TotalQuestsSubjectWise = Convert.ToInt16(txtBoxTotalQuests.Text.Trim());
            int TotalQuestsDiffLevelWiseOfThisSubject = 0;
            foreach (DataRow dr1 in dtDiffLevels.Rows)
            {
                String DiffLevelId = dr1["Id"].ToString();
                TextBox txtBoxDiffLevelQuest = tr.FindControl(SubjectId + "DiffLevel_" + DiffLevelId) as TextBox;
                int DiffLevelQuests = Convert.ToInt16(txtBoxDiffLevelQuest.Text.Trim());
                TotalQuestsDiffLevelWiseOfThisSubject = TotalQuestsDiffLevelWiseOfThisSubject + DiffLevelQuests;
                string sQuery = @"select top " + DiffLevelQuests + " Id,Name,ClassId,SubjectId,MarksOfQuestion,DifficultyLevelId,ImageUrl " +
                @"from OnlineTestQuestions where ClassId=" + ddlClass.SelectedValue + " and DifficultyLevelId=" + DiffLevelId + " and SubjectId=" + SubjectId + " ORDER BY NEWID()";
                DataTable dtDiffLevelQuests = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, sQuery);
                if (dtDiffLevelQuests.Rows.Count < DiffLevelQuests)
                {
                    showAlert("Only " + dtDiffLevelQuests.Rows.Count + " questions found for Subject :" + SubjectName + " Diffculty Level :" + dr1["Name"]);
                    return;
                }
                else
                {
                    foreach (DataRow drQuests in dtDiffLevelQuests.Rows)
                    {
                        dtQuestPlaceHolder.Rows.Add(drQuests.ItemArray);
                    }
                }

            }

            if (TotalQuestsDiffLevelWiseOfThisSubject != TotalQuestsSubjectWise)
            {
                showAlert("Make sure max quests of this subjects is equal to difficulty level wise total.");
                return;
            }
            TotalQuestsOfThisTest = TotalQuestsOfThisTest + TotalQuestsDiffLevelWiseOfThisSubject;
        }
        if (TotalQuestsOfThisTest != Convert.ToInt16(txtTotalNoOfQuests.Text.Trim()))
        {
            showAlert("Make sure no of quests of this test is equal to that of total of subjectwise total.");
            return;
        }
        ViewState["PreparedQuestions"] = dtQuestPlaceHolder;
        showQuestionPaper();
    }



    protected void buttonShuffle_Click(object sender, EventArgs e)
    {
        Button button = sender as Button;
        int index = Convert.ToInt16(button.ID);
        DataTable dtQuestPlaceHolder = ViewState["PreparedQuestions"] as DataTable;
        if (ViewState["PreparedQuestions"] == null)
            return;
        String DiffId = dtQuestPlaceHolder.Rows[index]["DifficultyLevelId"].ToString();
        String SubjectId = dtQuestPlaceHolder.Rows[index]["SubjectId"].ToString();
        string CSVQuestIds = "";
        foreach (DataRow dr in dtQuestPlaceHolder.Rows)
        {
            CSVQuestIds += dr["Id"].ToString()+",";
        }
        CSVQuestIds=CSVQuestIds.TrimEnd(',');

        string IdNotInPhrase = CSVQuestIds.Trim()==""?"": "and Id not in (" + CSVQuestIds + ")";
        string sQuery = @"select top 1 Id,Name,ClassId,SubjectId,MarksOfQuestion,DifficultyLevelId,ImageUrl " +
                @"from OnlineTestQuestions where ClassId=" + ddlClass.SelectedValue + " and DifficultyLevelId=" + DiffId + " and SubjectId=" + SubjectId + " "+IdNotInPhrase+" ORDER BY NEWID()";
        DataTable dtDiffLevelQuest = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, sQuery);
        if (dtDiffLevelQuest.Rows.Count > 0)
        {
            dtQuestPlaceHolder.Rows[index].ItemArray = dtDiffLevelQuest.Rows[0].ItemArray;
            ViewState["PreparedQuestions"] = dtQuestPlaceHolder;
            showQuestionPaper();
        }
        else
        { 
        showAlert("No more question of this subject and difficulty level found.");
        }
    }

    private void showQuestionPaper()
    {
        DataTable dtQuestPlaceHolder=ViewState["PreparedQuestions"] as DataTable;
        if (ViewState["PreparedQuestions"] == null)
            return;
        tblQuestionPaper.Rows.Clear();
        foreach (DataRow dr in dtQuestPlaceHolder.Rows)
        {
            TableRow tr = new TableRow();
            TableCell cellSrNo = new TableCell();
            Label lblQuestionSrNo = new Label();
            lblQuestionSrNo.Text = (dtQuestPlaceHolder.Rows.IndexOf(dr) + 1).ToString() + ". ";
            cellSrNo.Controls.Add(lblQuestionSrNo);
            tr.Cells.Add(cellSrNo);

            string QuestId = dr["Id"].ToString();
            string ClassId = dr["ClassId"].ToString();
            string SubjectId = dr["SubjectId"].ToString();
            TableCell cellQuestion = new TableCell();
            Label lblQuestion = new Label();
            string Question = dr["Name"].ToString();
            cellQuestion.Controls.Add(lblQuestion);
            lblQuestion.Text = Question;
            tr.Cells.Add(cellQuestion);

            TableCell cellImage = new TableCell();
            if (dr["ImageUrl"].ToString() != "")
            {
                Image imgQuestion = new Image();
                string ImageUrl = ("~/QuestImages/") + dr["ImageUrl"].ToString();
                imgQuestion.ImageUrl = ImageUrl;
                imgQuestion.Width = 100;
                //imgQuestion.Height = 100;
                cellImage.Controls.Add(imgQuestion);
            }
            tr.Cells.Add(cellImage);
            TableCell cellEmptyBeforMarks = new TableCell();
            tr.Cells.Add(cellEmptyBeforMarks);
            TableCell cellMarks = new TableCell();
            Label lblMarks = new Label();
            string MarksOfQuestion = DoFormat(Convert.ToDouble(dr["MarksOfQuestion"]));
            lblMarks.Text = MarksOfQuestion;
            cellMarks.Controls.Add(lblMarks);
            tr.Cells.Add(cellMarks);

            //TableCell cellEmptyBeforDiff = new TableCell();
            //tr.Cells.Add(cellEmptyBeforDiff);

            TableCell cellDifficulty = new TableCell();
            Label lblDifficulty = new Label();
            string diff = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, "Select Name from OnlineTestQuestDiffcultyLevels where Id=" + dr["DifficultyLevelId"].ToString(), SqlHelper.QueryType.Scaler);
            lblDifficulty.Text = diff;
            cellDifficulty.Controls.Add(lblDifficulty);
            tr.Cells.Add(cellDifficulty);

            TableCell cellReSuffleQuest = new TableCell();
            Button btnReSuffleQuest = new Button();
            btnReSuffleQuest.ID = dtQuestPlaceHolder.Rows.IndexOf(dr).ToString();
            btnReSuffleQuest.Text = "Shuffle";
            btnReSuffleQuest.CssClass = "btn btn-danger";
            btnReSuffleQuest.Click += new EventHandler(buttonShuffle_Click);
            cellReSuffleQuest.Controls.Add(btnReSuffleQuest);
            tr.Cells.Add(cellReSuffleQuest);

            tblQuestionPaper.Rows.Add(tr);

            string sQueryOptions = @"select Id,Name,Text,ImageUrl,IsCorrectAnswer from OnlineTestQuestionOptions
                                    where ClassId=" + ClassId + " and QuestionId=" + QuestId + " and IsActive=1";
            DataTable dtOptions = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, sQueryOptions);

            foreach (DataRow drOptions in dtOptions.Rows)
            {

                TableRow trOption = new TableRow();
                TableCell cellEmpty = new TableCell();
                trOption.Cells.Add(cellEmpty);

                TableCell cellOptionTitle = new TableCell();
                Label lblOptionTitle = new Label();
                lblOptionTitle.Text = drOptions["Name"].ToString() + ". ";
                cellOptionTitle.Controls.Add(lblOptionTitle);
                trOption.Cells.Add(cellOptionTitle);


                //TableCell cellOptionText = new TableCell();
                Label lblOptionText = new Label();
                lblOptionText.Text = drOptions["Text"].ToString() + ". ";
                cellOptionTitle.Controls.Add(lblOptionText);
                trOption.Cells.Add(cellOptionTitle);

                TableCell cellOptionImage = new TableCell();
                if (drOptions["ImageUrl"].ToString() != "")
                {

                    Image imgOptionImage = new Image();
                    //imgOptionImage.Width = 100;
                    imgOptionImage.Height = 100;
                    imgOptionImage.ImageUrl = ("~/OptImages/" + drOptions["ImageUrl"].ToString()).ToString();
                    cellOptionImage.Controls.Add(imgOptionImage);

                }
                trOption.Cells.Add(cellOptionImage);

                tblQuestionPaper.Rows.Add(trOption);
            }
        }

    }

    public string DoFormat(double myNumber)
    {
        var s = string.Format("{0:0.00}", myNumber);
        if (s.EndsWith("00"))
        {
            return ((int)myNumber).ToString();
        }
        else { return s; }
    }

    protected void Unnamed1_Click(object sender, EventArgs e)
    {
        String Name = txtName.Text;
        String NegativeMarking = ddlNegMarking.SelectedValue;
        String TotalNoOfQuests = txtTotalNoOfQuests.Text;
        String MaxMarks = txtMaxMarks.Text.Trim();
        String ConductDate = txtConductDate.Text;
        String PassingMarks = txtPassingMarks.Text;
        String StartTime = txtStartTime.Text;
        String EndTime = txtEndTime.Text;
        String IsActive = chkIsActive.Checked ? "1" : "0";

        string sQuery = "Update OnlineTestDetail set Name='" + Name + "'";
        sQuery += ",NegativeMarking=" + NegativeMarking;
        sQuery += ",TotalNoOfQuests=" + TotalNoOfQuests;
        sQuery += ",TotalMarks=" + MaxMarks;
        sQuery += ",ConductDate='" + ConductDate + "'";
        sQuery += ",PassingMarks=" + PassingMarks;
        sQuery += ",StartTime='" + StartTime + "'";
        sQuery += ",EndTime='" + EndTime + "'";
        sQuery += ",IsActive=" + IsActive;
        sQuery += " where Id=" + ddlOnlineTests.SelectedValue;
        SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, sQuery);
        LoadTestDetails();

    }
    private void LoadClass()
    {
        string squery = @"Select ClassId, Class from  tblclass";
        DataTable dt = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, squery);
        ddlClass.DataSource = dt;
        ddlClass.DataTextField = "Class";
        ddlClass.DataValueField = "ClassId";
        ddlClass.DataBind();
        LoadTests();
    }

    private void LoadTests()
    {
        string squery = @"Select Id,Name+convert(varchar,ConductDate,106)+ '	('+CONVERT(VARCHAR(5), StartTime, 108) +' To '+ CONVERT(VARCHAR(5),EndTime, 108)+')' Name
from  OnlineTestDetail where ClassId=" + ddlClass.SelectedValue;
        DataTable dt = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, squery);
        ddlOnlineTests.DataSource = dt;
        ddlOnlineTests.DataTextField = "Name";
        ddlOnlineTests.DataValueField = "Id";
        ddlOnlineTests.DataBind();
        LoadTestDetails();

    }
    protected void ddlClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadTests();
    }

    public void LoadTestDetails()
    {
        string squery = @"Select Name,NegativeMarking,TotalNoOfQuests,TotalMarks,convert(varchar,ConductDate,106) ConductDate,PassingMarks,convert(varchar(5),StartTime,108)StartTime,convert(varchar(5),EndTime,108)EndTime,IsActive  from OnlineTestDetail where Id=" + ddlOnlineTests.SelectedValue;
        DataTable dt = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, squery);

        String Name = dt.Rows[0]["Name"].ToString();
        String NegativeMarking = dt.Rows[0]["NegativeMarking"].ToString();
        String TotalNoOfQuests = dt.Rows[0]["TotalNoOfQuests"].ToString();
        String ConductDate = dt.Rows[0]["ConductDate"].ToString();
        String PassingMarks = dt.Rows[0]["PassingMarks"].ToString();
        String StartTime = dt.Rows[0]["StartTime"].ToString();
        String EndTime = dt.Rows[0]["EndTime"].ToString();
        String IsActive = dt.Rows[0]["IsActive"].ToString();
        String MaxMarks = dt.Rows[0]["TotalMarks"].ToString();

        txtName.Text = Name;
        ddlNegMarking.SelectedValue = NegativeMarking;
        txtTotalNoOfQuests.Text = TotalNoOfQuests;
        txtMaxMarks.Text = MaxMarks;
        txtConductDate.Text = ConductDate;
        txtPassingMarks.Text = PassingMarks;
        txtStartTime.Text = StartTime;
        txtEndTime.Text = EndTime;
        chkIsActive.Checked = IsActive == "1";


    }
    protected void ddlNegMarking_SelectedIndexChanged(object sender, EventArgs e)
    {
        divDeductMarks.Visible = ddlNegMarking.SelectedValue == "1";
        txtDeductMarks.Text = "1";
    }
    void showAlert(string msg)
    {
        lblAlertMessage.Text = msg;
        ClientScript.RegisterClientScriptBlock(GetType(), "alert", "alert('" + msg + "')", true);


    }
    protected void btnPrepareQuestionPaper_Click(object sender, EventArgs e)
    {
        LoadQuestions();
    }
}