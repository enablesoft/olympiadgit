﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json;
using System.Runtime.Remoting.Contexts;

namespace Olympiad
{
    /// <summary>
    /// Summary description for api
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class api : System.Web.Services.WebService
    {

        [WebMethod]
        public void LogIn(string UserId, string Password)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@loginid", UserId);
            param[1] = new SqlParameter("@pwd", Password);
            DataTable dt = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "pro_validatelogin", param).Tables[0];
            Context.Response.Write((dt.Rows.Count > 0) ? dt.Rows[0]["Regno"].ToString() : "Invalid UserId or Password");
        }

        [WebMethod]
        public void getRegData(string RegNo)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@RegNo", RegNo);
            DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "pro_getregisteredstudata", param);
            Context.Response.Write(JsonConvert.SerializeObject(ds.Tables[0]));
        }

        [WebMethod]
        public void getTestStartDetail(string TestId, string ClassId, string RegNo)
        {
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@Testid", TestId);
            param[1] = new SqlParameter("@Classid", ClassId);
            param[2] = new SqlParameter("@msg", SqlDbType.VarChar, 100);
            param[2].Direction = ParameterDirection.Output;
            param[3] = new SqlParameter("@RegNo", RegNo);

            DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "pro_getstarttestdetail", param);
            if (param[2].Value.ToString() != "")
            {
                Context.Response.Write(param[2].Value.ToString());
            }
            else
            {
                Context.Response.Write(JsonConvert.SerializeObject(ds.Tables[0]));
            }
        }

        [WebMethod]
        public void GetTestData(string Regno, string TestId)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@regNo", Regno);
            param[1] = new SqlParameter("@testid", TestId);
            DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "Pro_getstutestdata", param);

            DataTable dtQuestions = ds.Tables[2];
            ds.Tables[2].Columns.Add("OptionsJson", typeof(string));

            foreach (DataRow dr in ds.Tables[2].Rows)
            {
                string QId = dr["Quesid"].ToString();
                DataTable dtOptionsOfThisQuestion = new DataTable();
                dtOptionsOfThisQuestion.Columns.Add("Ans", typeof(string));
                dtOptionsOfThisQuestion.Columns.Add("Aimg", typeof(string));
                dtOptionsOfThisQuestion.Columns.Add("QOId", typeof(string));
                dtOptionsOfThisQuestion.Columns.Add("OptionTitle", typeof(string));


                foreach (DataRow drOptions in ds.Tables[3].Rows)
                {

                    if (drOptions["Quesid"].ToString().Equals(QId))
                    {
                        DataRow drOpt = dtOptionsOfThisQuestion.NewRow();
                        drOpt["Ans"] = drOptions["Ans"];
                        drOpt["Aimg"] = drOptions["Aimg"];
                        drOpt["QOId"] = drOptions["QOId"];
                        drOpt["OptionTitle"] = drOptions["OptionTitle"];
                        dtOptionsOfThisQuestion.Rows.Add(drOpt);
                    }
                }
                string Json = JsonConvert.SerializeObject(dtOptionsOfThisQuestion);
                dr["OptionsJson"] = Json;
            }

            Context.Response.Write(JsonConvert.SerializeObject(ds));
        }
        [WebMethod]
        public void SubmitTestData(string RegNo, int TestId, int ClassId, int t_hr, int t_min, int t_sec, string ansdata)
        {
            try
            {

                DataTable dt = new DataTable();
                dt.Columns.Add("Quesid");
                dt.Columns.Add("Ans");
                dt.Columns.Add("AnsId");
                dynamic jsonArray = JsonConvert.DeserializeObject(ansdata);
                foreach (var item in jsonArray)
                {
                    DataRow dr = dt.NewRow();
                    dr["Quesid"] = item.Quesid;
                    dr["Ans"] = item.Ans;
                    dr["AnsId"] = item.AnsId;
                    dt.Rows.Add(dr);
                }

                SqlParameter[] param = new SqlParameter[7];
                param[0] = new SqlParameter("@Regno", RegNo);
                param[1] = new SqlParameter("@testid", TestId);
                param[2] = new SqlParameter("@classid", ClassId);
                param[3] = new SqlParameter("@t_hour", t_hr);
                param[4] = new SqlParameter("@t_min", t_min);
                param[5] = new SqlParameter("@t_sec", t_sec);
                param[6] = new SqlParameter("@ansdata", dt);
                SqlHelper.ExecuteNonQuery(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "Pro_SubmittestAnsdetails", param);
                Context.Response.Write("Submit test Successfully.");
            }
            catch (Exception ex)
            {
                Context.Response.Write("Error Occured.");
            }
        }
    }
}

