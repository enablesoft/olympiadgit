﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SampleTest.aspx.cs" Inherits="Olympiad.SampleTest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <link href="Images/Favicon.png" rel="shortcut icon" type="image/png" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="theme-color" content="#103157">
    <link href="Style/TestStyle/TestBootstrap.min.css?v=1.2" rel="stylesheet" type="text/css" />
    <link href="Style/Bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet"
        type="text/css" />
    <link href="Style/TestStyle/TestStyle.css?v=1.3" rel="stylesheet" type="text/css" />
    <style>
        .previous
        {
            text-decoration: none;
            display: inline-block;
            padding: 2px 16px;
            background-color: #f1f1f1;
            color: black;
        }
        
        .next
        {
            text-decoration: none;
            display: inline-block;
            padding: 2px 16px; /*
            background-color: #4CAF50;
            */
            color: white;
        }
        
        .round
        {
            border-radius: 50%;
        }
        .top-part
        {
            margin-top: 5px;
        }
        
        
        .Header_Img
        {
            width: 100%;
            display: inline-block;
        }
        .Header_Img > img
        {
            width: auto;
            max-width: 100%;
            height: 60px;
        }
        .carousel-control
        {
            top: 200px;
        }
        .carousel-control.left
        {
            right: 48px;
            left: auto;
        }
        .Slider_Icons
        {
            display: inline-block;
            padding: 0px;
            width: 100%;
            position: ABSOLUTE;
            bottom: 50PX;
            text-align: right;
        }
        .Slider_Icons .btnIcon
        {
            display: inline-block;
            padding: 10px;
            background-color: #7a7979;
            margin: 5px;
            border-radius: 50%;
            color: #fff;
        }
        .Slider_Icons .btnIcon:hover
        {
            background-color: #5cb85c;
        }
        body
        {
            overscroll-behavior: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="theme-wrapper">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 ">
            <div class="Header_Img">
                <img src="Images/Logo-rpsolympiad.png" alt="RPS Olympiad" />
            </div>
        </div>
        <div class="col-lg-6  text-center col-sm-4 col-md-4 col-xs-6 removepaddingMobile">
            <span id="tclass" class="Header_classname"></span>
            <br />
            <span style="font-size: 15px; color: #ff8f00; letter-spacing: 1px;">(Please do not Refresh
                and Press Back button)</span>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  removepaddingMobile" style="margin-top: 5px;">
            <div class="TimerDiv" style="">
                <strong>Total Time</strong><br>
                <span class="numbers" id="thour">0</span> <span class="numbers">&nbsp;:&nbsp;</span>
                <span class="numbers" id="tmin">0</span> <span class="numbers">&nbsp;:&nbsp;</span>
                <span class="numbers" id="tsec">00</span>
            </div>
            <div class="TimerDiv">
                <strong>Elapsed Time</strong><br />
                <span class="numbers" id="dday">0</span> <span class="numbers">&nbsp;:&nbsp;</span>
                <span class="numbers" id="dhour">0</span> <span class="numbers">&nbsp;:&nbsp;</span>
                <span class="numbers" id="dmin">18</span> <span class="numbers">&nbsp;:&nbsp;</span>
                <span class="numbers" id="dsec">40</span>
            </div>
        </div>
        <h1 class="navbar hide" id="header-navbar" style="background-color: #2eafb0; margin: 0px;
            padding: 5px; color: #fff">
            RPS Olympiad 2020
        </h1>
    </div>
    <div id="page-wrapper" class="container-fluid" style="margin-top: 0px; background-color: #fff">
        <div class="row">
            <div id="content-wrapper">
                <div class="col-lg-2 col-xs-12 col-md-2 col-sm-12 ">
                    <div class="div_section">
                        <h6>
                            Sections :</h6>
                        <div class="pull-left" id="divsection">
                        </div>
                        <input type="hidden" id="sectionListId" name="sectionListId" value="[NCO, NSO, IMO, IEO, Reasoning]">
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-xs-9 col-md-7 col-sm-8 ">
                    <div class="main-box" style="margin-bottom: 5px; font-family: times new roman">
                        <header class="main-box-header clearfix" style="min-height: 36px; border-bottom: 1px solid rgb(243, 245, 246);
                            padding: 0px 20px 4px; margin-bottom: 5px;">
                            <h2>
                                <span class="pull-left" style="color: #212121;"><strong>Q No:</strong> <span style="color: #212121;
                                    margin-right: 0px; padding-right: 0px;" id="qNumber" name="qNumber">1</span> /<span
                                        id="qt" style="color: #000; padding-left: 2px">50</span> </span><span class="pull-right"
                                            style="color: #212121;"><strong>Marks:</strong><span id="tempMarks" name="tempMarks"
                                                style="color: #212121;">1</span> </span>
                            </h2>
                        </header>
                        <div style="padding: 0px 0 5px 0; height: 300px; overflow: auto;">
                            <div class="container" style="width: 100%">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
                                    <div class="carousel-inner" id="divques">
                                    </div>
                                    <%-- <a id="previousButtonDivId" name="previousButtonDivId" class="left carousel-control"
                                        href="#myCarousel" data-slide="prev" style="display: block;"><span class="fa fa-arrow-left">
                                        </span><span class="sr-only">Previous</span> </a><a id="nextButtonDivId" name="nextButtonDivId"
                                            class="right carousel-control" href="#myCarousel" data-slide="next" style="background-image: none;
                                            display: block;"><span class="fa fa-arrow-right"></span><span class="sr-only">Next</span>
                                        </a>--%>
                                </div>
                            </div>
                        </div>
                        <div class="Slider_Icons">
                            <a href="#myCarousel" data-slide="prev" id="previousButtonDivId" class="btnIcon"><span
                                class="fa fa-arrow-left"></span><span class="sr-only">Previous</span> </a><a href="#myCarousel"
                                    data-slide="next" id="nextButtonDivId" class="btnIcon"><span class="fa fa-arrow-right">
                                    </span><span class="sr-only">Previous</span> </a>
                        </div>
                        <div id="errorSpan11" style="display: none; color: red; text-align: right; position: absolute;
                            font-size: 14px; line-height: 40px; right: -2px; bottom: -100px" class="errors">
                            * Please Select Option
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-xs-3 col-md-3 col-sm-4 removepaddingMobile ">
                    <div class="main-box clearfix" style="margin-bottom: 5px; background-color: #e7ebee;">
                        <div class="main-box-header clearfix" style="">
                            <div class="form-group">
                                <h6 class="SubHeading">
                                    answer :</h6>
                                <div class="clearfix">
                                </div>
                                <div id="ajaxRadioDiv">
                                    <style>
                                        .radio-inline + .radio-inline
                                        {
                                            margin-top: 0;
                                            margin-left: -20px;
                                        }
                                    </style>
                                    <!--<link rel="stylesheet" href="/resources/newTestScreen/css/style.css">-->
                                    <input type="hidden" id="noofoptions" name="noOfOptions" value="4" style="margin-left: 54px;">
                                    <div class=" col-lg-12 col-md-12 col-xs-12 removepaddingMobile" style="margin-top: 0;">
                                        <input id="A" type="radio" name="radio1" value="A" class="css-checkbox2"><label for="A"
                                            class="css-label2">&nbsp; A</label>
                                        <div class="check">
                                            <div class="inside">
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" col-lg-12 col-md-12 col-xs-12 removepaddingMobile" style="margin-top: 0;">
                                        <input id="B" type="radio" name="radio1" value="B" class="css-checkbox2"><label for="B"
                                            class="css-label2">&nbsp; B</label>
                                        <div class="check">
                                            <div class="inside">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-xs-12 removepaddingMobile" style="margin-top: 0;">
                                        <input id="C" type="radio" name="radio1" value="C" class="css-checkbox2"><label for="C"
                                            class="css-label2">&nbsp; C</label>
                                        <div class="check">
                                            <div class="inside">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-xs-12 removepaddingMobile" style="margin-top: 0;">
                                        <input id="D" type="radio" name="radio1" value="D" class="css-checkbox2"><label for="D"
                                            class="css-label2">&nbsp; D</label>
                                        <div class="check">
                                            <div class="inside">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 removepaddingMobile">
                                    <br />
                                    <a class="btn btn-danger btn-sm" type="button" style="width: 100%;" onclick="clearanswer();">
                                        Clear Answer</a><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-10 col-xs-12 col-md-10 col-sm-8 ">
                    <div class="main-box">
                        <div class="header">
                            <div class="col-lg-6 col-xs-12 col-md-4 col-sm-6 ">
                                <h2>
                                    <span class="pull-left" style="color: #212121;"><strong>Total Question: </strong><span
                                        id="tques" style="color: #000">50</span></span>
                                </h2>
                            </div>
                            <div class="col-lg-6 col-xs-12 col-md-8 col-sm-6 ">
                                <span class="infographic-box4 "><i class="fa label-primary"></i>Non Visited </span>
                                <span class="infographic-box4 "><i class="fa red-bg"></i>Skipped </span><span class="infographic-box4 ">
                                    <i class="fa green-bg"></i>Answered </span>
                            </div>
                        </div>
                        <div class="conversation-wrapper" style="padding: 0px;">
                            <div class="conversation-content">
                                <div class="slimScrollDiv">
                                    <div id="divqbtn" class="conversation-inner1 infographic-box1">
                                    </div>
                                    <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute;
                                        top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px;
                                        height: 141.61px;">
                                    </div>
                                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute;
                                        top: 0px; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90;
                                        right: 1px; display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix" style="margin-bottom: 5px;">
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-xs-12 col-md-2 col-sm-4">
                    <button class="btn btn-success btn-lg" type="button" style="margin-bottom: 7px; width: 100%;"
                        onclick="submittest(true);">
                        Submit Test</button>
                    <!--<input type="button" class="submit_textmrk" value="End Test" onclick="endTestWithoutLogin('viewQuestionformId', '/endTestSubmit');"/>-->
                </div>
            </div>
        </div>
    </div>
    <a id="endtest" href="Default.aspx"></a>
    <input type="hidden" id="quesSelectid" />
    <input type="hidden" id="rbSelectvalue" />
    <asp:HiddenField ID="hn_starttime" Value="0" runat="server" />
    <asp:HiddenField ID="hn_currenttime" Value="0" runat="server" />
    <asp:HiddenField ID="hn_testid" runat="server" />
    <asp:HiddenField ID="hn_classid" runat="server" />
    <asp:HiddenField ID="hn_thour" runat="server" />
    <asp:HiddenField ID="hn_tmin" runat="server" />
    <asp:HiddenField ID="hn_testtype" runat="server" />
    <asp:HiddenField ID="hn_regno" runat="server" />
    <asp:HiddenField ID="hn_submiturl" runat="server" />
    <asp:HiddenField ID="hn_getdataurl" runat="server" />
    </form>
    <script src="Style/Bootstrap/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"> </script>
    <script type="text/javascript">
        var tques = 0,
	        tblsection, tblques, tblopt, tblquesans = [], testtype;
        $(document).ready(function () {
            testtype = $("#hn_testtype").val();
            $("#tmin").text($("#hn_tmin").val());
            $("#thour").text($("#hn_thour").val());
            var today;
            if (testtype == "1") {
                today = new Date();
            }
            else {
                today = new Date($("#hn_starttime").val());
            }
            countdown(today.getTime());
            getsampletestdata();
        });
        function countdown(edate) {
            var today;
            //sample
            if (testtype == "1") {
                today = new Date();
            }
            else {
                today = new Date($("#hn_currenttime").val());
            }
            var dd = today.getTime() - edate;

            var dday = Math.floor(dd / (60 * 60 * 1000 * 24) * 1);
            var dhour = Math.floor((dd % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
            var dmin = Math.floor(((dd % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
            var dsec = Math.floor((((dd % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);

            if (parseInt($("#tmin").text()) > 59) {
                if (dmin == 0 && dsec == 0 && dhour > 0) {
                    dmin = 60;
                }
            }

            if (dhour >= $("#thour").text() && dmin >= $("#tmin").text()) {
                alert("Your test time completed.Ok to submit test !!!");
                submittest(false);
                document.getElementById('dsec').style.display = "none";
                document.getElementById('dmin').style.display = "none";
            }
            else if (dmin < 0 || dhour < 0 || dhour > $("#thour").text() || dday > 0) {
                alert('Invalid Test Start Time.');
                window.location.href = document.getElementById('endtest').href + '?Invalid Start Time.';
                return;
            }
            else {
                document.getElementById('dday').innerHTML = dday;

                document.getElementById('dhour').innerHTML = dhour;

                document.getElementById('dmin').innerHTML = dmin;

                document.getElementById('dsec').innerHTML = dsec;

                //add one second in server time
                var dt = new Date($("#hn_currenttime").val());
                dt.setSeconds(dt.getSeconds() + 1);
                $("#hn_currenttime").val(dt);
                //**********
                setTimeout("countdown('" + edate + "')", 1000);
            }
        }
        function submittest(confirmpopup) {
            if (confirmpopup == true) {
                if (!confirm('Do you want to submit test ?')) {
                    return false;
                }
            }
            var lastans = "";
            if ($('input[name=radio1]:checked').length) {
                lastans = $('input[name=radio1]:checked').val();
            }
            Useranswer($("#quesSelectid").val(), lastans, 0);

            if (tblquesans.length > 0) {
                var data = "{regno:'" + $('#hn_regno').val() + "',tstid:" + $('#hn_testid').val() + ",classid:" + $('#hn_classid').val() + ",t_hr:" + $('#dhour').text() + ",t_min:" + $('#dmin').text() + ",t_sec:" + $('#dsec').text() + ",ansdata:'" + JSON.stringify(tblquesans) + "'}";

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: $("#hn_submiturl").val(),
                    data: data,
                    datatype: "json",
                    success: function (result) {
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        if (xhr.responseText.indexOf("Session Expired") > 0) {
                            //Session has Expired,redirect to login page
                            window.location.href = "../Default.aspx?Message=Session expired please login again";
                        } else {
                            //Other Exceptions/Errors
                            alert("Error Occured");
                        }
                    }
                });
            }
            window.location.href = document.getElementById('endtest').href;
            return;
        }
        function getsampletestdata() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: $("#hn_getdataurl").val(),
                data: "{regno:'" + $('#hn_regno').val() + "',testid:'" + $('#hn_testid').val() + "'}",
                datatype: "json",
                success: function (result) {
                    result = JSON.parse(result.d);
                    //total time
                    var tmaster = result['Table'];
                    $("#tmin").text(tmaster[0].TotalMinutes);
                    $("#tclass").text("Test class : " + tmaster[0].class);
                    tques = tmaster[0].TotalNoOfQuests;
                    $("#tques").text(tques);
                    $("#qt").text(tques);
                    //add Sections Dynamically
                    tblsection = result['Table1'];
                    addsections();
                    //add Questions btn
                    quesbtn();
                    //questions
                    tblques = result['Table2'];
                    //OPtions
                    tblopt = result['Table3'];
                    ques_ans();

                },
                error: function (xhr, textStatus, errorThrown) {
                    if (xhr.responseText.indexOf("Session Expired") > 0) {
                        //Session has Expired,redirect to login page
                        window.location.href = "../Default.aspx?Message=Session expired please login again";
                    } else {
                        //Other Exceptions/Errors
                        alert("Error Occured");
                    }
                }
            });
        }
        function addsections() {
            if (tblsection.length) {
                for (var i = 0; i < tblsection.length; i++) {
                    var a;
                    if (i == 0) {
                        a = '<a data-target="#myCarousel"  data-slide-to="' + tblsection[i].Dataitemindex + '"><span class="label label-primary label-primary1" id="sectionid' + i + '">' + tblsection[i].ShortName + '</span> </a>';
                    } else {
                        a = '<a data-target="#myCarousel"   data-slide-to="' + tblsection[i].Dataitemindex + '"><span class="label label-primary label-default1" id="sectionid' + i + '">' + tblsection[i].ShortName + '</span> </a>';
                    }
                    $("#divsection").append(a);
                }
            }
        }

        function quesbtn() {
            if (tques > 0) {
                for (var i = 0; i < tques; i++) {
                    var a = '<a data-target="#myCarousel" href="#" data-slide-to=' + i + '><i id="qbtn' + (i + 1) + '" class="fa label-primary">' + (i + 1) + '</i> </a>';
                    $("#divqbtn").append(a);
                }
            }
        }
        $('#myCarousel').on('slid.bs.carousel', function () {
            var currentItem = $("#myCarousel .item.active");
            var currentIndex = $('#myCarousel .item').index(currentItem) + 1;
            $("#qNumber").text(currentIndex);
            $("#quesSelectid").val($("#questionselectedid" + currentIndex + "").val());
            Useranswer($("#quesSelectid").val(), "", 1);
        });
        $('#myCarousel').on('slide.bs.carousel', function () {
            var currentItem = $("#myCarousel .item.active");
            var currentIndex = $('#myCarousel .item').index(currentItem) + 1;
            $("#quesSelectid").val($("#questionselectedid" + currentIndex + "").val());

            //**Answer
            if ($('input[name=radio1]:checked').length) {
                $("#qbtn" + currentIndex + "").removeClass().addClass("fa green-bg");
                $("#rbSelectvalue").val($('input[name=radio1]:checked').val());
            } else {
                $("#qbtn" + currentIndex + "").removeClass().addClass("fa red-bg");
                $("#rbSelectvalue").val("");
            }
            if ($('input[name=radio1]:checked').length) {
                document.querySelector('input[name="radio1"]:checked').checked = false;
            }
            Useranswer($("#quesSelectid").val(), $("#rbSelectvalue").val(), 0);
        });

        function Useranswer(q, a, curslide) {
            var index;
            //last slide response
            if (curslide == 0) {
                if (tblquesans.length > 0) {
                    index = getindex(tblquesans, "quesid", q);
                }
                if (index != null) {
                    //delete existing record
                    tblquesans.splice(index, 1);
                }
                if (a != "") {
                    tblquesans.push({
                        quesid: q,
                        ans: a,
                        AnsId: 0
                    });
                }
            }
            //cur slide response
            else {
                if (tblquesans.length > 0) {
                    index = getindex(tblquesans, "quesid", q);
                }
                if (index != null) {
                    $("input[name=radio1][value='" + tblquesans[index].ans + "']").prop("checked", true);
                }
            }
        }

        function getindex(arrsrch, key, valuesearch) {
            for (var i = 0; i < arrsrch.length; i++) {
                if (arrsrch[i][key] === valuesearch) {
                    return i;
                }
            }
            return null;
        }

        function ques_ans() {
            var opt, A, opttext;
            for (var i = 0; i < tblques.length; i++) {
                var isactive = 'active';
                if (i > 0) {
                    isactive = '';
                }
                var q = '<div id="questionSelected' + (i + 1) + '" name="questionSelected" class="item ' + isactive + '">\n' +
				'<style type="text/css">\n' +
				'{list-style:none;font-size:14px;line-height:45px;}\n' +
				'{line-height:30px;}\n' +
				'\n' +
				'span.optA{background:#130303;}\n' +
				'span.optB{background:#d041ff;}\n' +
				'span.optC{background:#9cad3d;}\n' +
				'span.optD{background:#d04b23;}\n' +
				'span.optE{background:#c04e2c;}\n' +
				'</style>\n' +
				'<p>' + tblques[i].Ques + '';
                if (tblques[i].Qimg != "") {
                    q += '<br> <img src="' + tblques[i].Qimg + '">';
                }
                q += '<br><br>';

                //Options
                opt = tblopt.filter(function (obj) {
                    return obj.Quesid == tblques[i].Quesid;
                });
                A = '';
                for (var j = 0; j < opt.length; j++) {
                    opttext = getopttext(j);
                    A += '<span class="opt' + opttext + '">' + opttext + '</span> ' + opt[j].Ans;
                    if (opt[j].Aimg != "") {
                        A += '<img src="' + opt[j].Aimg + '" middle">';
                    }
                    A += '<br>';
                }
                //end Options
                if (i == 0) {
                    $("#quesSelectid").val(tblques[i].Quesid);
                }
                var ques_ans = q + A + '</p><input type="hidden" value="' + tblques[i].Quesid + '" id="questionselectedid' + (i + 1) + '"></div>';

                $("#divques").append(ques_ans);
            }
        }

        function getopttext(i) {
            var txt;
            switch (i) {
                case 0:
                    txt = 'A';
                    break;
                case 1:
                    txt = 'B';
                    break;
                case 2:
                    txt = 'C';
                    break;
                case 3:
                    txt = 'D';
                    break;
                case 4:
                    txt = 'E';
                    break;
                default:

            }
            return txt;
        }
        function clearanswer() {
            var curindex = $("#qNumber").text();
            $("#qbtn" + curindex + "").removeClass().addClass("fa red-bg");
            Useranswer($("#quesSelectid").val(), "", 0);
            if ($('input[name=radio1]:checked').length) {
                document.querySelector('input[name="radio1"]:checked').checked = false;
            }
        }
        $('input[name="radio1"]:radio').on('change', function (e) {
            var selection = $(this).val();
            tempanssave($("#quesSelectid").val(), selection);
        });
        function tempanssave(quesid, ans) {
            var data = "{testid:" + $('#hn_testid').val() + ",classid:" + $('#hn_classid').val() + ",regno:'" + $('#hn_regno').val() + "',quesid:" + quesid + ",ans:'" + ans + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "SampleTest.aspx/tempanssave",
                data: data,
                datatype: "json",
                success: function (result) {
                },
                error: function (xhr, textStatus, errorThrown) {
                    tempanssave();
                }
            });
        }
        //Disable mouse right click
        //        $("body").on("contextmenu", function (e) {
        //            return false;
        //        });
        //        $('body').bind('cut copy paste keydown keypress keyup ctrl alt ', function (e) {
        //            e.preventDefault();
        //        });

        $('body').bind('cut copy paste keypress ctrl alt ', function (e) {
            e.preventDefault();
        });
       
    </script>
</body>
</html>
