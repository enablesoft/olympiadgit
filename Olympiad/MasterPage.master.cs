﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Web.Services;
public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void btncontregs_Click(object sender, EventArgs e)
    {
        if (checkConditions.Checked)
        {
            Session["acceptcond"] = "1";
            Response.Redirect("Registration.aspx");
        }
        else
        {
            checkConditions.Focus();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopupInstruction();", true);
            Session["acceptcond"] = null;
        }
    }
    protected void OnClick_Login(object sender, EventArgs e)
    {
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@loginid", txtUserid.Text);
        param[1] = new SqlParameter("@pwd", txtPass.Text);
        DataTable dt = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "pro_validatelogin", param).Tables[0];
        if (dt.Rows.Count > 0)
        {
            lblmsg.Text = "";
            Session["RegsNo"] = dt.Rows[0]["Regno"].ToString();
            Response.Redirect("~/DashBoard.aspx");
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup();", true);
            lblmsg.Text = "Invalid Userid or Password.";
            return;
        }
    }
   
}
