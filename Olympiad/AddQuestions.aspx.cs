﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Data.SqlClient;

public partial class AdminOnlineTestConfig : System.Web.UI.Page
{
    static string TestId = "2";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            LoadClass();
            LoadSubjects();
            LoadDiffLevels();
            initiateOptionGrid();

        }
        SavePreviousData();
    }

    private void LoadClass()
    {
        string squery = @"Select ClassId, Class from  tblclass";
        DataTable dt = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, squery);
        ddlClass.DataSource = dt;
        ddlClass.DataTextField = "Class";
        ddlClass.DataValueField = "ClassId";
        ddlClass.DataBind();
        LoadQuests();
    }





    protected void ddlClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadSubjects();
        LoadQuests();
        clearQuestAndOptions();

    }

    public void LoadQuests()
    {
        string squery = @"select a.Name as Question,b.Class,c.Name Subject,e.Name DifficultyLevel,a.Id QuestionId,b.ClassId ClassId,SubjectId,
                        MarksOfQuestion,DifficultyLevelId from OnlineTestQuestions  a
                        left join tblclass b on a.ClassId=b.Classid
                        left join OnlineTestSubjectMaster c on a.SubjectId=c.Id
                        left join OnlineTestQuestDiffcultyLevels e on a.DifficultyLevelId=e.Id
                        where  a.IsActive=1 and c.IsActive=1  and e.IsActive=1 
                        and a.ClassId=" + ddlClass.SelectedValue;
        DataTable dt = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, squery);
        gridQuestions.DataSource = dt;
        gridQuestions.DataBind();
    }


    public void LoadSubjects()
    {
        string squery = @"Select b.Id,b.Name from  OnlineTestClasswiseTestSubj a   left join   OnlineTestSubjectMaster b 
on a.Subjectid=b.Id 
where IsActive=1 and a.Classid=" + ddlClass.SelectedValue;
        DataTable dt = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, squery);
        ddlQuestSubject.DataSource = dt;
        ddlQuestSubject.DataTextField = "Name";
        ddlQuestSubject.DataValueField = "Id";
        ddlQuestSubject.DataBind();
        ddlQuestSubject.Items.Insert(0, new ListItem("Select", ""));
    }

    public void LoadDiffLevels()
    {
        string squery = @"Select Id,Name from OnlineTestQuestDiffcultyLevels where IsActive=1";
        DataTable dt = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, squery);
        ddlQuestDiffLevel.DataSource = dt;
        ddlQuestDiffLevel.DataTextField = "Name";
        ddlQuestDiffLevel.DataValueField = "Id";
        ddlQuestDiffLevel.DataBind();
        ddlQuestDiffLevel.Items.Insert(0, new ListItem("Select", ""));
    }


    private void initiateOptionGrid()
    {

        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        dt.Columns.Add(new DataColumn("OptionTitle", typeof(string)));
        dt.Columns.Add(new DataColumn("OptionText", typeof(string)));
        dt.Columns.Add(new DataColumn("IsCorrectAnswer", typeof(string)));
        dt.Columns.Add(new DataColumn("OptionImage", typeof(byte[])));

        dr = dt.NewRow();
        dr["RowNumber"] = 1;
        dr["OptionTitle"] = string.Empty;
        dr["OptionText"] = string.Empty;
        dr["IsCorrectAnswer"] = "0";
        dr["OptionImage"] = null;
        dt.Rows.Add(dr);

        ViewState["OptionTable"] = dt;
        gridViewOptions.DataSource = dt;
        gridViewOptions.DataBind();

    }

    private void removeLastRowToOptionsGrid()
    {
        if (ViewState["OptionTable"] != null)
        {
            DataTable dtCurrentTable = (DataTable)ViewState["OptionTable"];
            if (dtCurrentTable.Rows.Count > 1)
                dtCurrentTable.Rows.RemoveAt(dtCurrentTable.Rows.Count - 1);

            ViewState["OptionTable"] = dtCurrentTable;
            gridViewOptions.DataSource = dtCurrentTable;
            gridViewOptions.DataBind();
        }

        SetPreviousData(false);

    }

    private void addNewRowToOptionsGrid()
    {

        int rowIndex = 0;
        if (ViewState["OptionTable"] != null)
        {
            DataTable dtCurrentTable = (DataTable)ViewState["OptionTable"];
            DataRow drCurrentRow = null;
            if (dtCurrentTable.Rows.Count > 0)
            {
                for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                {
                    TextBox box1 = (TextBox)gridViewOptions.Rows[rowIndex].Cells[1].FindControl("txtOptionTitle");
                    TextBox box2 = (TextBox)gridViewOptions.Rows[rowIndex].Cells[2].FindControl("txtOptionText");
                    FileUpload fuImage = (FileUpload)gridViewOptions.Rows[rowIndex].Cells[3].FindControl("fuOptionImage");
                    CheckBox chkIsCorrectAnswer = (CheckBox)gridViewOptions.Rows[rowIndex].Cells[4].FindControl("chkIsCorrectAnswer");
                    System.Web.UI.WebControls.Image imgOptionImage = (System.Web.UI.WebControls.Image)gridViewOptions.Rows[rowIndex].Cells[3].FindControl("imgOptionImage");
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["RowNumber"] = i + 2;
                    dtCurrentTable.Rows[i]["OptionTitle"] = box1.Text;
                    dtCurrentTable.Rows[i]["OptionText"] = box2.Text;
                    dtCurrentTable.Rows[i]["IsCorrectAnswer"] = chkIsCorrectAnswer.Checked ? "1" : "0";

                    byte[] bytes;
                    if (fuImage.HasFile)
                        bytes = resizeImage(fuImage);
                    else
                        bytes = dtCurrentTable.Rows[i]["OptionImage"] as byte[];
                    dtCurrentTable.Rows[i]["OptionImage"] = bytes;

                    rowIndex++;
                }
                dtCurrentTable.Rows.Add(drCurrentRow);
                ViewState["OptionTable"] = dtCurrentTable;
                gridViewOptions.DataSource = dtCurrentTable;
                gridViewOptions.DataBind();
            }
        }

        SetPreviousData(false);

    }

    private void SavePreviousData()
    {

        byte[] bytesQuestImage = ViewState["QuestImage"] as byte[];
        if (fuQuestImage.HasFile)
            bytesQuestImage = resizeImage(fuQuestImage);
        if (bytesQuestImage != null)
            imgQuestImage.ImageUrl = "data:image;base64," + Convert.ToBase64String(bytesQuestImage);
        ViewState["QuestImage"] = bytesQuestImage;

        int rowIndex = 0;
        if (ViewState["OptionTable"] != null)
        {
            DataTable dt = (DataTable)ViewState["OptionTable"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox txtOptionTitle = (TextBox)gridViewOptions.Rows[rowIndex].Cells[1].FindControl("txtOptionTitle");
                    TextBox txtOptionText = (TextBox)gridViewOptions.Rows[rowIndex].Cells[2].FindControl("txtOptionText");
                    FileUpload fuImage = (FileUpload)gridViewOptions.Rows[rowIndex].Cells[3].FindControl("fuOptionImage");
                    CheckBox chkIsCorrectAnswer = (CheckBox)gridViewOptions.Rows[rowIndex].Cells[4].FindControl("chkIsCorrectAnswer");
                    System.Web.UI.WebControls.Image imgOptionImage = (System.Web.UI.WebControls.Image)gridViewOptions.Rows[rowIndex].Cells[3].FindControl("imgOptionImage");

                    dt.Rows[i]["OptionTitle"] = txtOptionTitle.Text;
                    dt.Rows[i]["OptionText"] = txtOptionText.Text;
                    dt.Rows[i]["IsCorrectAnswer"] = chkIsCorrectAnswer.Checked ? ("1") : "0";
                    byte[] bytes = null;
                    if (fuImage.HasFile)
                        bytes = resizeImage(fuImage);
                    else if (dt.Rows[i]["OptionImage"] != null)
                        bytes = dt.Rows[i]["OptionImage"] as byte[];
                    dt.Rows[i]["OptionImage"] = bytes;
                    if (bytes != null)
                        imgOptionImage.ImageUrl = "data:image;base64," + Convert.ToBase64String(bytes);
                    rowIndex++;
                }
            }

        }

    }

    private void SetPreviousData(bool fromEdit)
    {
        int rowIndex = 0;
        if (ViewState["OptionTable"] != null)
        {
            DataTable dt = (DataTable)ViewState["OptionTable"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox txtOptionTitle = (TextBox)gridViewOptions.Rows[rowIndex].Cells[1].FindControl("txtOptionTitle");
                    TextBox txtOptionText = (TextBox)gridViewOptions.Rows[rowIndex].Cells[2].FindControl("txtOptionText");
                    FileUpload fuImage = (FileUpload)gridViewOptions.Rows[rowIndex].Cells[3].FindControl("fuOptionImage");
                    CheckBox chkIsCorrectAnswer = (CheckBox)gridViewOptions.Rows[rowIndex].Cells[4].FindControl("chkIsCorrectAnswer");
                    System.Web.UI.WebControls.Image imgOptionImage = (System.Web.UI.WebControls.Image)gridViewOptions.Rows[rowIndex].Cells[3].FindControl("imgOptionImage");

                    txtOptionTitle.Text = dt.Rows[i]["OptionTitle"].ToString();
                    txtOptionText.Text = dt.Rows[i]["OptionText"].ToString();
                    chkIsCorrectAnswer.Checked = dt.Rows[i]["IsCorrectAnswer"].ToString().Equals("1");
                    byte[] bytes;
                    if (fuImage.HasFile && !fromEdit)
                        bytes = resizeImage(fuImage);
                    else
                        bytes = dt.Rows[i]["OptionImage"] as byte[];

                    if (bytes != null)
                        imgOptionImage.ImageUrl = "data:image;base64," + Convert.ToBase64String(bytes);
                    rowIndex++;
                }
            }

        }

    }

    byte[] resizeImage(FileUpload FileUpload1)
    {
        if (!FileUpload1.HasFile)
            return null;
        int width = 200;
        int height = 200;
        try
        {
            Stream stream = FileUpload1.PostedFile.InputStream;
            Bitmap image = new Bitmap(stream);
            Bitmap target = new Bitmap(width, height);
            Graphics graphic = Graphics.FromImage(target);
            graphic.DrawImage(image, 0, 0, width, height);

            ImageConverter converter = new ImageConverter(); return (byte[])converter.ConvertTo(image, typeof(byte[]));

        }
        catch (Exception)
        {
            return null;
        }

    }

    protected void AddNewRowToOptionsGrid(object sender, EventArgs e)
    {
        addNewRowToOptionsGrid();

    }

    protected void RemoveLastRowFromOptionsGrid(object sender, EventArgs e)
    {
        removeLastRowToOptionsGrid();

    }


    protected void btnSaveQuestion_Click(object sender, EventArgs e)
    {
        SavePreviousData();
        String Question = txtQuestName.Text;
        String SubjectId = ddlQuestSubject.SelectedValue;
        String DiffLevelId = ddlQuestDiffLevel.SelectedValue;
        String Marks = txtQuestMarks.Text;
        String IsActive = (chkQuestIsActive.Checked ? "1" : "0");

        if (Question == "")
        {
            showAlert("Question can not be empty!");
            return;
        }

        if (SubjectId == "")
        {
            showAlert("Please select a subject!");
            return;
        }

        if (DiffLevelId == "")
        {
            showAlert("Please provide difficulty level!");
            return;
        }

        if (Marks == "")
        {
            showAlert("Please provide marks for this question!");
            return;
        }

        if (IsActive == "0")
        {
            showAlert("Please check  Active check box because not checking it will hide this question from final list!");
            return;
        }
        if (ViewState["OptionTable"] == null)
        {
            showAlert("Please provide options!");
            return;
        }
        DataTable dtCurrentTable = (DataTable)ViewState["OptionTable"];
        if (dtCurrentTable.Rows.Count < 4)
        {
            showAlert("Please provide atleast 4 options for this question!");
            return;
        }
        int CorrectAnswers = 0;

        for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
        {
            if (dtCurrentTable.Rows[i]["OptionTitle"].ToString().Trim() == "")
            {
                showAlert("option Title must be given example A B C D");
                return;
            }

            if (dtCurrentTable.Rows[i]["OptionText"].ToString().Trim() == "")
            {
                showAlert("Option Text must be given.");
                return;
            }
            if (dtCurrentTable.Rows[i]["IsCorrectAnswer"].ToString() == "")
            {
                showAlert("There must be a correct answer!");
                return;
            }
            if (dtCurrentTable.Rows[i]["IsCorrectAnswer"].ToString() == "1")
                CorrectAnswers++;
        }
        if (CorrectAnswers != 1)
        {
            showAlert("Correct answer must be only one.");
            return;
        }
        string fileName = "";
        string savefilename = "";


        if (ViewState["QuestImage"] != null)
        {
            byte[] bytesQuestImage = ViewState["QuestImage"] as byte[];
            fileName = Guid.NewGuid().ToString() + ".jpg";
            Directory.CreateDirectory(Server.MapPath("~/QuestImages/" + TestId + "/" + ddlClass.SelectedValue + "/"));
            File.WriteAllBytes(Server.MapPath("~/QuestImages/" + TestId + "/" + ddlClass.SelectedValue + "/" + fileName), bytesQuestImage);
            savefilename = "QuestImages/" + TestId + "/" + ddlClass.SelectedValue + "/" + fileName;
        }
        String Id = "";
        if (ViewState["QuestionId"] != null)
        {
            Id = ViewState["QuestionId"].ToString();
            string sQuery = @"Update OnlineTestQuestions  
                        set Name=@Name,ClassId=@ClassId,SubjectId=@SubjectId,MarksOfQuestion=@MarksOfQuestion,DifficultyLevelId=@DifficultyLevelId,ImageUrl=@ImageUrl,IsActive=@IsActive
                        where Id=" + Id;
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@ClassId", ddlClass.SelectedValue);
            param[1] = new SqlParameter("@SubjectId", ddlQuestSubject.SelectedValue);
            param[2] = new SqlParameter("@MarksOfQuestion", Marks);
            param[3] = new SqlParameter("@DifficultyLevelId", DiffLevelId);
            // param[4] = new SqlParameter("@ImageUrl", (fileName == "" ? DBNull.Value : fileName as object));
            param[4] = new SqlParameter("@ImageUrl", (savefilename == "" ? DBNull.Value : savefilename as object));
            param[5] = new SqlParameter("@IsActive", IsActive);
            param[6] = new SqlParameter("@Name", Question);
            SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, sQuery, param);
        }
        else
        {
            string sQuery = @"Insert into OnlineTestQuestions  
                        (Name,ClassId,SubjectId,MarksOfQuestion,DifficultyLevelId,ImageUrl,IsActive)
                        values (@Name,@ClassId,@SubjectId,@MarksOfQuestion,@DifficultyLevelId,@ImageUrl,@IsActive) Select Scope_Identity() as Id";
            sQuery = string.Format(sQuery, Question, ddlClass.SelectedValue, ddlQuestSubject.SelectedValue, Marks, DiffLevelId, (fileName == "" ? "null" : "'" + fileName + "'"), IsActive);
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@Name", Question);
            param[1] = new SqlParameter("@ClassId", ddlClass.SelectedValue);
            param[2] = new SqlParameter("@SubjectId", ddlQuestSubject.SelectedValue);
            param[3] = new SqlParameter("@MarksOfQuestion", Marks);
            param[4] = new SqlParameter("@DifficultyLevelId", DiffLevelId);
            //param[5] = new SqlParameter("@ImageUrl", (fileName == "" ? DBNull.Value : fileName as object));
            param[5] = new SqlParameter("@ImageUrl", (savefilename == "" ? DBNull.Value : savefilename as object));
            param[6] = new SqlParameter("@IsActive", IsActive);

            Id = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, sQuery, param).Tables[0].Rows[0]["Id"].ToString();
        }

        string squeryDeleteOptions = "delete from OnlineTestQuestionOptions where QuestionId=" + Id + " and ClassId=" + ddlClass.SelectedValue;
        SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, squeryDeleteOptions);
        for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
        {
            string OptionTitle = dtCurrentTable.Rows[i]["OptionTitle"].ToString().Trim();
            string OptionText = dtCurrentTable.Rows[i]["OptionText"].ToString().Trim();
            String isCorrectAnswer = dtCurrentTable.Rows[i]["IsCorrectAnswer"].ToString();
            byte[] bytes = dtCurrentTable.Rows[i]["OptionImage"] as byte[];
            string FileUrl = "";
            string FileurlSave = "";
            if (bytes != null)
            {
                FileUrl = Guid.NewGuid().ToString() + ".jpg";
                Directory.CreateDirectory(Server.MapPath("~/OptImages/" + TestId + "/" + ddlClass.SelectedValue + "/"));
                File.WriteAllBytes(Server.MapPath("~/OptImages/" + TestId + "/" + ddlClass.SelectedValue + "/" + FileUrl), bytes);
                FileurlSave = "OptImages/" + TestId + "/" + ddlClass.SelectedValue + "/" + FileUrl;
            }
            string sOptionsQuery = @"Insert into OnlineTestQuestionOptions (Id,Name,Text,ImageUrl,ClassId,QuestionId,IsCorrectAnswer,IsActive) values
((Select isnull(max(Id),0)+1 from OnlineTestQuestionOptions),@OptionTitle,@OptionText,@ImageUrl,@ClassId,@QuestionId,@IsCorrectAnswer,1)";
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@OptionTitle", OptionTitle);
            param[1] = new SqlParameter("@OptionText", OptionText);
            // param[2] = new SqlParameter("@ImageUrl", (FileUrl == "" ? DBNull.Value : FileUrl as object));
            param[2] = new SqlParameter("@ImageUrl", (FileurlSave == "" ? DBNull.Value : FileurlSave as object));
            param[3] = new SqlParameter("@ClassId", ddlClass.SelectedValue);
            param[4] = new SqlParameter("@QuestionId", Id);
            param[5] = new SqlParameter("@IsCorrectAnswer", isCorrectAnswer);

            SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.Text, sOptionsQuery, param);
            // sbuilder.Append(sOptionsQuery);

        }



        clearQuestAndOptions();
        showAlert("Question has been saved.");
        LoadQuests();

    }

    private void clearQuestAndOptions()
    {
        txtQuestMarks.Text = "";
        txtQuestName.Text = "";
        ddlQuestSubject.SelectedValue = "";
        ddlQuestSubject.SelectedValue = "";
        ViewState["QuestImage"] = null;
        ViewState["QuestionId"] = null;
        imgQuestImage.ImageUrl = null;
        chkQuestIsActive.Checked = true;
        initiateOptionGrid();

    }



    protected void btnEditQuestOnClick(object sender, EventArgs e)
    {
        Button btn = sender as Button;
        GridViewRow gv = btn.NamingContainer as GridViewRow;

        string QuestId = (gridQuestions.DataKeys[gv.RowIndex].Values["QuestionId"].ToString());
        string ClassId = (gridQuestions.DataKeys[gv.RowIndex].Values["ClassId"].ToString());
        string SubjectId = (gridQuestions.DataKeys[gv.RowIndex].Values["SubjectId"].ToString());
        string MarksOfQuestion = (gridQuestions.DataKeys[gv.RowIndex].Values["MarksOfQuestion"].ToString());
        string DifficultyLevelId = (gridQuestions.DataKeys[gv.RowIndex].Values["DifficultyLevelId"].ToString());



        DataTable dt = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, "Select Id,Name,ImageUrl from OnlineTestQuestions where Id=" + QuestId + " and SubjectId=" + SubjectId);
        String QuestionText = dt.Rows[0]["Name"].ToString();
        String QuestImageUrl = dt.Rows[0]["ImageUrl"].ToString();

        String ImageUrl = dt.Rows[0]["ImageUrl"].ToString();

        if (ImageUrl != "")
        {
            //byte[] bytes = File.ReadAllBytes(Server.MapPath("~/QuestImages/"+TestId+"/"+ddlClass.SelectedValue+"/" + ImageUrl));
            byte[] bytes = File.ReadAllBytes(Server.MapPath("~/" + ImageUrl));
            imgQuestImage.ImageUrl = "data:image;base64," + Convert.ToBase64String(bytes);
            ViewState["QuestImage"] = bytes;
        }
        else
        {
            imgQuestImage.ImageUrl = null;
        }
        ViewState["QuestionId"] = QuestId;
        txtQuestName.Text = QuestionText;
        txtQuestMarks.Text = MarksOfQuestion;
        ddlQuestDiffLevel.SelectedValue = DifficultyLevelId;
        ddlQuestSubject.SelectedValue = SubjectId;

        string sQueryOptions = @"select Row_Number() over (Order by Id) RowNumber,Name OptionTitle,Text OptionText,IsCorrectAnswer,ImageUrl OptionImage from OnlineTestQuestionOptions where QuestionId=" + QuestId + " and ClassId=" + ClassId;
        DataTable dtOptions = SqlHelper.ExecuteQuery(ErpGlobal.DBCONNECTIONSTRING, sQueryOptions);


        DataTable dtCloned = new DataTable();
        dtCloned.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        dtCloned.Columns.Add(new DataColumn("OptionTitle", typeof(string)));
        dtCloned.Columns.Add(new DataColumn("OptionText", typeof(string)));
        dtCloned.Columns.Add(new DataColumn("IsCorrectAnswer", typeof(string)));
        dtCloned.Columns.Add(new DataColumn("OptionImage", typeof(byte[])));

        foreach (DataRow row in dtOptions.Rows)
        {
            DataRow dr = dtCloned.NewRow();
            dr["RowNumber"] = row["RowNumber"];
            dr["OptionTitle"] = row["OptionTitle"];
            dr["OptionText"] = row["OptionText"];
            dr["IsCorrectAnswer"] = row["IsCorrectAnswer"];

            byte[] bytes = null;
            if (row["OptionImage"].ToString() != "")
            {
                // bytes = File.ReadAllBytes(Server.MapPath("~/OptImages/" + TestId + "/" + ddlClass.SelectedValue + "/" + row["OptionImage"].ToString()));
                bytes = File.ReadAllBytes(Server.MapPath("~/" + row["OptionImage"].ToString()));
            }
            dr["OptionImage"] = bytes;
            dtCloned.Rows.Add(dr);
        }

        ViewState["OptionTable"] = dtCloned;
        gridViewOptions.DataSource = dtCloned;
        gridViewOptions.DataBind();
        SetPreviousData(true);
    }

    void showAlert(string msg)
    {
        lblAlertMessage.Text = msg;
        ClientScript.RegisterClientScriptBlock(GetType(), "alert", "alert('" + msg + "')", true);


    }
}
