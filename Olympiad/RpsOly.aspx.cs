﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
namespace Olympiad
{
    public partial class RpsOly : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["adminuserid"] = null;
        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@userid", txtUserid.Text);
                param[1] = new SqlParameter("@pwd", PasswordEncrypt_Decrypt.Encrypt(txtPass.Text));
                DataSet ds = SqlHelper.ExecuteDataset(ErpGlobal.DBCONNECTIONSTRING, CommandType.StoredProcedure, "Pro_validatestafflogin", param);
                if (ds.Tables[0].Rows.Count>0)
                {
                    Session["adminuserid"]=ds.Tables[0].Rows[0]["userid"].ToString();
                    Response.Redirect("~/Report.aspx");
                }
                else
                {
                    UtilityModule.showMessage(Page,"Invalid Userid or Password.Try Again");
                    return;
                }
            }
            catch (Exception ex)
            {
                UtilityModule.showMessage(Page, ex.Message.Replace("'", ""));
            }
        }
    }
}