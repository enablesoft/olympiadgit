﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Student_paymentresponse"
    CodeBehind="paymentresponse.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="img/fevi_campus.ico" type="image/x-icon" />
    <link href="../Style/bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../Style/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../Style/Login.css?v=2" rel="stylesheet" type="text/css" />
    <title>Payment response</title>
    <style>
    .container2 {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translateX(-50%) translateY(-50%);
}

.loader {
  border: 5px solid #f3f3f3; /* Light grey */
  border-top: 5px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 45px;
  height: 45px;
  animation: spin 2s linear infinite;
  left: 50%;
  z-index: 1;
  border-radius: 50%;
  -webkit-animation: spin 2s linear infinite;
  
  
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
    </style>
</head>
<body style="font-size: 15px;">
    <div class='Header_top'>
        <div class='container'>
            <div class='col-sm-3'>
            </div>
            <div class='col-sm-3 loader'>
            </div>
            <h2>
                Please Wait....</h2>
        </div>
    </div>
    <form id="form1" runat="server">
    <div id="divMain" class="container2" runat="server">
    </div>
    </form>
</body>
</html>
